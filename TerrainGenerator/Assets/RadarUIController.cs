﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RadarUIController : MonoBehaviour {

	public float minFrontalDetection= 500;
	public RadarMarker radarMarker;
	public int maxTargetsOnDisplay = 20;
	public List<RadarMarker> usedMarkers = new List<RadarMarker>();
	public List<RadarMarker> unusedMarkers = new List<RadarMarker>();
	public List<RadarTarget> tempRadarTargetList = new List<RadarTarget>();

	private static RadarUIController instance;
	public static RadarUIController Instance
	{
		get{
			if(instance==null)
			{
				instance = FindObjectOfType<RadarUIController>();
			}
			return instance;
		}
	}
	
	private void Awake()
	{
		if(instance!=null && instance!=this)
		{
			Destroy(instance.gameObject);
			return;
		}
		instance = this;
		NotificationCenter.AddListener(HandleRegister,NotificationType.RegisterRadarTarget);
		NotificationCenter.AddListener(HandleUnregister,NotificationType.UnRegisterRadarTarget);
	}

	private bool initialized = false;
	private void Start()
	{
		unusedMarkers.Add(radarMarker);
		for(int i=0;i<maxTargetsOnDisplay;i++)
		{
			RadarMarker r  =Instantiate<RadarMarker>(radarMarker);
			RectTransform tTransform = r.GetComponent<RectTransform>();
			tTransform.SetParent(this.transform);
			tTransform.localScale = Vector3.one;
			tTransform.anchoredPosition = Vector2.zero;
			unusedMarkers.Add(r);
		}

		foreach(RadarMarker r in unusedMarkers)
		{
			r.gameObject.SetActive(false);
		}
		initialized = true;
		foreach(RadarTarget r in tempRadarTargetList)
		{
			CreateMarker(r);
		}
	}

	void HandleRegister (Notification note)
	{
		if(!initialized)
		{
			tempRadarTargetList.Add((RadarTarget)note.data);
			return;
		}
		CreateMarker((RadarTarget)note.data);
	}

	void HandleUnregister (Notification note)
	{
		if(!initialized)
		{
			tempRadarTargetList.Remove((RadarTarget)note.data);
			return;
		}
		RemoveMarker((RadarTarget)note.data);
	}	

	public void CreateMarker(RadarTarget rTarget)
	{
		RadarMarker m = unusedMarkers[0];
		m.radarTarget = rTarget;
		unusedMarkers.RemoveAt(0);
		usedMarkers.Add(m);
		m.gameObject.SetActive(true);
	}

	public void RemoveMarker(RadarTarget rTarget)
	{
		RadarMarker markerToRemove = usedMarkers.Find(a=>a.radarTarget == rTarget);
		if(markerToRemove!=null)
		{
			usedMarkers.Remove(markerToRemove);
			unusedMarkers.Add(markerToRemove);
			markerToRemove.gameObject.SetActive(false);
		}
	}
}
