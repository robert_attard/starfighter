﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Holoville.HOTween;

public class HitDecal : MonoBehaviour {

	private Image decalImage;
	private Color startColor;
	private Tweener anim;

	void Awake()
	{
		decalImage = GetComponentInChildren<Image>();
		startColor = decalImage.color;
		decalImage.color = new Color(startColor.r,startColor.g,startColor.b,0);
		anim = HOTween.To(decalImage,0.2f,new TweenParms().Prop("color", startColor,false).AutoKill(false).Ease(EaseType.EaseInOutQuad).Loops(4,LoopType.Yoyo).OnComplete(()=>{anim.Rewind();}));
		anim.Pause();
	}

	public void Show()
	{
		if(anim.isPaused)
		{
			anim.Play();
		}
	}
}
