﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HitDecalUIManager : MonoBehaviour {

	private HitDecal[] hitDecals;
	private Dictionary<float,HitDecal> hitDecalDictionary = new Dictionary<float, HitDecal>();

	// Use this for initialization
	void Awake () {
		hitDecals = GetComponentsInChildren<HitDecal>();
		foreach(HitDecal h in hitDecals)
		{
			float angle = (Mathf.Round(h.transform.localRotation.eulerAngles.z));
			angle = (angle!=0)?(360f-angle):angle;
			hitDecalDictionary.Add(angle,h);
		}
		NotificationCenter.AddListener(HandlePlayerHit,NotificationType.UIUpdate);
	}

	void OnDestroy()
	{
		NotificationCenter.AddListener(HandlePlayerHit,NotificationType.UIUpdate);
	}

	void HandlePlayerHit (Notification note)
	{
		UIUpdateInfo info = (UIUpdateInfo)note.data;
		if(info.uiUpdateType == UIUpdateType.PlayerUIUpdate)
		{
			PlayerUIUpdateInfo pInfo = (PlayerUIUpdateInfo)info.data;
			float dot = Vector3.Dot(pInfo.playerForwardTransform.forward,(pInfo.inflictor.position - pInfo.playerForwardTransform.position).normalized);
			float flank = Mathf.Sign( Vector3.Dot(pInfo.playerForwardTransform.right,(pInfo.inflictor.position - pInfo.playerForwardTransform.position).normalized));

			float angle = Mathf.Acos(dot) * Mathf.Rad2Deg * flank;
			float fixedAngle  = (angle<0)?((180f+angle)+180f):angle;

			float f = Mathf.Round((fixedAngle/360f)*8f)/8f;
			f*=360f;
			f = (f==360f)?0:f;
			hitDecalDictionary[f].Show();
		}
	}
}
