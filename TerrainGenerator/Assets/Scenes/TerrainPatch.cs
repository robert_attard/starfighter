﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ThreadPooling;


public class TerrainPatch : MonoBehaviour {

	private Vector3 position;
	public Vector3 Position
	{
		internal set{
			position = value;
		}
		get{
			return position;
		}
	}

	private Mesh mesh;
	private MeshPatchTemplate mPatchTemplate;
	private MeshThreadState state;
	private float patchWidth;
	public float timeStamp;

	public MeshRenderer mr;
	public MeshFilter mf;
	MeshCollider  mc;

	public MeshThreadState State
	{
		get{
			return state;
		}
		internal set{
			state = value;
		}
	}

	public static TerrainPatch Create(Material terrainMaterial, int vertexcolumns,float cellWidth,MapBitMap mBitMap,float patchWidth, int meshColliderDivisor)
	{
		GameObject g = new GameObject("TerrainPatch");
		g.layer =9;
		TerrainPatch tPatch = g.AddComponent<TerrainPatch>();
		tPatch.mr = g.AddComponent<MeshRenderer>();
		tPatch.mr.material = terrainMaterial;
		tPatch.mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		tPatch.mr.receiveShadows =true;
		tPatch.mf = g.AddComponent<MeshFilter>();
		tPatch.mf.mesh = new Mesh();
//		tPatch.mc= g.AddComponent<MeshCollider>();
//		tPatch.mc.enabled = false;
//		tPatch.mc.sharedMesh = new Mesh();
		tPatch.Init(terrainMaterial,vertexcolumns,cellWidth,mBitMap,patchWidth,meshColliderDivisor);
		return tPatch;
	}
	
	public void Init(Material terrainMaterial, int vertexcolumns,float cellWidth,MapBitMap mBitMap,float patchWidth, int meshColliderDivisor)
	{
		mPatchTemplate = new MeshPatchTemplate(vertexcolumns,cellWidth,mBitMap,meshColliderDivisor);
		State = MeshThreadState.Idle;
	}

	public TerrainPatch SubmitForProcessing(Vector3 position)
	{
//		mc.enabled =false;
		mr.enabled = false;
		gameObject.SetActive(true);
		this.Position = position;
		State = MeshThreadState.ProcessingDoNotDisturb;
		StartCoroutine(CheckRenderJobCoroutine());
		ThreadPool.QueueUserWorkItem(ThreadProcess,this);
		return this;
	}

	IEnumerator CheckRenderJobCoroutine()
	{
		while(State != MeshThreadState.ReadyForRendering)
		{
			yield return null;
		}
		RenderJob();
	}

	public void RenderJob()
	{
		transform.position = Position;
		mf.mesh = new Mesh();
		mf.mesh.vertices = mPatchTemplate.hVertices;
		mf.mesh.triangles = mPatchTemplate.triangleIndices;
		mf.mesh.colors = mPatchTemplate.colors;
		mf.mesh.normals = mPatchTemplate.normals;
		mf.mesh.RecalculateBounds();

//		mc.enabled = false;
//		mc.sharedMesh = new Mesh();
//		mc.sharedMesh.vertices = mPatchTemplate.meshColliderMeshVertices;
//		mc.sharedMesh.triangles = mPatchTemplate.meshColliderTriangleIndices;
//		mc.sharedMesh.RecalculateBounds();
//		mc.sharedMesh.RecalculateNormals();
//		mc.enabled = true;
		mr.enabled = true;
		State = MeshThreadState.Rendering;
	}
	
	private void ThreadProcess(object meshJob)
	{
		TerrainPatch mt = (TerrainPatch)meshJob;
		mt.mPatchTemplate.UpdateMeshProperties(Position);
		State = MeshThreadState.ReadyForRendering;
	}

}
