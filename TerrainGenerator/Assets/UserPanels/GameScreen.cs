using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameScreen : UIPanel
{

	public UIBarMask healthBar;

	public override PanelId panelId
	{
		get
		{
			return PanelId.GameScreen;
		}
	}

	void Awake()
	{
		NotificationCenter.AddListener(HandlePlayerHealthChange,NotificationType.UIUpdate);
	}

	void OnDestroy()
	{
		NotificationCenter.RemoveListener(HandlePlayerHealthChange,NotificationType.UIUpdate);
	}

	public void OnClickSettings()
	{
		UIController.ChangePanel(PanelId.OptionScreen);
	}

	void HandlePlayerHealthChange (Notification note)
	{
		UIUpdateInfo info = (UIUpdateInfo)note.data;
		if(info.uiUpdateType == UIUpdateType.PlayerUIUpdate)
		{
			PlayerUIUpdateInfo pInfo = (PlayerUIUpdateInfo)info.data;
			healthBar.SetBarValue(pInfo.HealthPercentage);
		}
	}
}
