using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OptionScreen : UIPanel
{

	public override PanelId panelId
	{
		get
		{
			return PanelId.OptionScreen;
		}
	}

	public Text sliderSmoothValue;
	public Slider sliderSmooth;

	public Text sliderScaleValue;
	public Slider sliderScale;

	public Text sliderJoysticRadiusValue;
	public Slider sliderJoysticRadius;

	public Text sliderYawValue;
	public Slider sliderYaw;

	public Text sliderPitchValue;
	public Slider sliderPitch;

	public Text sliderRollValue;
	public Slider sliderRoll;


	public VehicleController vMouseController;
	public InputTouchController iTouchController;

	public void ReloadScene()
	{
		Application.LoadLevel(0);
	}

	public override void Open ()
	{
		sliderSmooth.onValueChanged.AddListener(HandleValueChange);
		sliderScale.onValueChanged.AddListener(HandleScaleValueChange);
		sliderJoysticRadius.onValueChanged.AddListener(HandleJoysticRadiusValueChange);
		sliderYaw.onValueChanged.AddListener(HandleYawValueChange);
		sliderPitch.onValueChanged.AddListener(HandlePitchValueChange);
		sliderRoll.onValueChanged.AddListener(HandleRollValueChange);
		base.Open ();
	}

	protected override void OnOpenComplete ()
	{
		base.OnOpenComplete ();
		sliderSmooth.value = iTouchController.smoothStep;
		sliderScale.value = iTouchController.scaleOffset;
		sliderJoysticRadius.value = iTouchController.movementRadius;
		sliderPitch.value = vMouseController.pitchSpeed;
		sliderYaw.value = vMouseController.yawSpeed;
		sliderRoll.value = vMouseController.rollSmoothTime;
	}

	public override void Close ()
	{
		sliderSmooth.onValueChanged.RemoveListener(HandleValueChange);
		sliderScale.onValueChanged.RemoveListener(HandleScaleValueChange);
		sliderJoysticRadius.onValueChanged.RemoveListener(HandleJoysticRadiusValueChange);
		sliderYaw.onValueChanged.RemoveListener(HandleYawValueChange);
		sliderPitch.onValueChanged.RemoveListener(HandlePitchValueChange);
		sliderRoll.onValueChanged.RemoveListener(HandleRollValueChange);
		base.Close ();
	}

	public void OnClickClose()
	{
		UIController.ChangePanel(PanelId.GameScreen);
	}
	
	void HandleValueChange (float arg0)
	{
		iTouchController.smoothStep = arg0;
		arg0 = Mathf.Round(arg0*1000f)/1000f;
		sliderSmoothValue.text = arg0.ToString();
	}

	void HandleScaleValueChange (float arg0)
	{
		iTouchController.scaleOffset = arg0;
		arg0 = Mathf.Round(arg0*1000f)/1000f;
		sliderScaleValue.text = arg0.ToString();
	}

	void HandleJoysticRadiusValueChange (float arg0)
	{
		iTouchController.movementRadius = arg0;
		arg0 = Mathf.Round(arg0*1000f)/1000f;
		sliderJoysticRadiusValue.text = arg0.ToString();
	}

	void HandleYawValueChange (float arg0)
	{
		vMouseController.yawSpeed = arg0;
		arg0 = Mathf.Round(arg0*1000f)/1000f;
		sliderYawValue.text = arg0.ToString();
	}

	void HandlePitchValueChange (float arg0)
	{
		vMouseController.pitchSpeed = arg0;
		arg0 = Mathf.Round(arg0*1000f)/1000f;
		sliderPitchValue.text = arg0.ToString();
	}

	void HandleRollValueChange (float arg0)
	{
		vMouseController.rollSmoothTime = arg0;
		arg0 = Mathf.Round(arg0*1000f)/1000f;
		sliderRollValue.text = arg0.ToString();
	}
}
