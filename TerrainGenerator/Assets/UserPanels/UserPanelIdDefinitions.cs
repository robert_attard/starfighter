using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class PanelId {

public static readonly PanelId GameScreen = new PanelId(0,"GameScreen");
public static readonly PanelId OptionScreen = new PanelId(1,"OptionScreen");
public static readonly PanelId DebugScreen = new PanelId(2,"DebugScreen");

}
