using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugScreen : UIPanel
{

	public override PanelId panelId
	{
		get
		{
			return PanelId.DebugScreen;
		}
	}
}
