﻿using UnityEngine;
using System.Collections;

public class ActiveTerrainPlacement : MonoBehaviour {

	public float yOffset = 0;
	private MeshRenderer[] mr;
	public MonoBehaviour mainController;
	// Use this for initialization
	void Awake()
	{
		mr= GetComponentsInChildren<MeshRenderer>();
		ToggleVisibility(false);
	}

	private void ToggleVisibility(bool enabled)
	{
		mainController.enabled = enabled;
		foreach(MeshRenderer m in mr)
		{
			m.enabled = enabled;
		}
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		if(Physics.Raycast(transform.position + (Vector3.up * 1000),Vector3.down,out hit,Mathf.Infinity,1<<9))
		{
			transform.position = hit.point + (Vector3.up * yOffset);
			this.enabled = false;
			ToggleVisibility(true);
		}
	}
}
