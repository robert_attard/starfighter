﻿using UnityEngine;
using System.Collections;

public class DamageInfo
{
	public float damage;
	public GameObject inflictor;

	public DamageInfo(float damage, GameObject inflictor)
	{
		this.damage = damage;
		this.inflictor = inflictor;
	}
}

public class WeaponBase : MonoBehaviour {

	public float damage;
}
