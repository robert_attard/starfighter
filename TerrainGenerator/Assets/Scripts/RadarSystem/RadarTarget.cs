﻿using UnityEngine;
using System.Collections;

public enum RadarTargetType
{
	enemy,
	objective
}

public class RadarTarget : MonoBehaviour {

	public RadarTargetType targetType;

	private void Start()
	{
		NotificationCenter.Post(NotificationType.RegisterRadarTarget,this);
	}

	private void OnDestroy()
	{
		NotificationCenter.Post(NotificationType.UnRegisterRadarTarget,this);
	}
}
