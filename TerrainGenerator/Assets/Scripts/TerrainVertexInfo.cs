﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class VTColorInfo
{
	public TerrainColorMatch color;
	public float distanceFromVertex;
	public float weighting;
	public Vector2 vertexPosition;
	public Vector2 quadrantCenter;
	
}


public class TerrainVertexInfo : MonoBehaviour {
	public List<VTColorInfo> vt = new List<VTColorInfo>();
	public List<VTColorInfo> all = new List<VTColorInfo>();
	
}
