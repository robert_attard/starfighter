﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



//[System.Serializable]
public class MeshPatchTemplate2 {

	public static int triangleCount;
	public static int vertexCount;
	public static float cellWidth;
	public static float patchWidth;
	public static float maxQuadrantDistance;

	public static int vertexColumns=0;
	public PatchVertex[] pVertices;

	public MapBitMap mBitMap;

	public static void Init(int vColumns, float cwidth)
	{
		vertexColumns = vColumns;
		cellWidth = cwidth;
		triangleCount =  (int)Mathf.Pow(vertexColumns-1,2)*2;
		patchWidth = cellWidth * (vertexColumns-1);
		vertexCount = (int)Mathf.Pow(vertexColumns,2);
		maxQuadrantDistance = Mathf.Sqrt(2* Mathf.Pow(patchWidth,2f));
	}

	public MeshPatchTemplate2(MapBitMap mBitMap)
	{
		CodeSpeedTester.LogTimeStart("i");
		this.mBitMap = mBitMap;
		pVertices = new PatchVertex[vertexCount];
		CodeSpeedTester.LogElapsedTime("i");
		InitPoints();
	}

	private void InitPoints()
	{
		//Set Up base positions;
		for(int i=0;i<vertexCount;i++)
		{
			float x = ((i%vertexColumns) * cellWidth);
			float z = ((Mathf.Floor(i/vertexColumns)) * cellWidth);
			PatchVertex  pv = new PatchVertex(x,0,z);
			pVertices[i] = pv;
		}

		//setNeighbours
		int[] neighbourIndexOffsets = new int[]{-vertexColumns-1, -vertexColumns,+1,vertexColumns+1,vertexColumns,-1};
		for(int i=0;i<vertexCount;i++)
		{
			int childCount =0;
			for(int p=0;p<neighbourIndexOffsets.Length;p++)
			{
				int targetIndex = GetNeighbour(i,neighbourIndexOffsets[p]);
				if(targetIndex!=-1)
				{
					pVertices[i].neighbours[childCount] = pVertices[targetIndex];
					childCount++;
				}
				pVertices[i].neighbourCount = childCount;
			}
			if(i>(vertexColumns-1)*vertexColumns)
			{
				SetLastNeighbourToFirst(pVertices[i]);
			}
		}
	}

	private void SetLastNeighbourToFirst(PatchVertex vertex)
	{
		PatchVertex[] n = new PatchVertex[6];
		PatchVertex lastN = vertex.neighbours[vertex.neighbourCount-1];
		for(int i=0;i<vertex.neighbourCount-1;i++)
		{
			n[i+1] = vertex.neighbours[i];
		}
		n[0]=lastN;
		vertex.neighbours = n;
	}

	private int GetNeighbour(int startIndex, int offset)
	{
		if((startIndex + offset)<0 || (startIndex + offset) >= vertexCount)
		{
			return -1;
		}

		int mod = startIndex%vertexColumns;

		if(mod == 0)
		{
			if(offset == -1  || offset == -vertexColumns-1 || offset == vertexColumns-1)
			{
				return -1;
			}
		}

		if(mod == (vertexColumns-1))
		{
			if(offset == -vertexColumns+1  || offset == 1 || offset == vertexColumns+1)
			{
				return -1;
			}
		}
		return startIndex + offset;
	}

	private class TerrainQuadrant
	{
		public TerrainCharacteristic tc;
		public float distance;
		public static float patchWidth;
		public static MapBitMap mBitMap;
		public static float squaredRadius;
		public float _x;
		public float _y;
		public VTColorInfo vtInfo;
		private float maxDistance;

		public TerrainQuadrant(float x, float y)
		{
			this._x = x + (patchWidth/2f);
			this._y = y + (patchWidth/2f);
			tc = mBitMap.GetHeightMapInfo(x,y);
			maxDistance = patchWidth*2f;
		}

		public bool ContainsPoint(Vector2 point)
		{
			return Mathf.Pow(point.x - _x,2) + Mathf.Pow(point.y - _y,2f) < squaredRadius;
		}
	}

	private static float SDistance(Vector2 a,Vector2 b)
	{
		return maxQuadrantDistance - Vector3.Distance(a,b);
	}

	public void UpdateMeshProperties(Vector3 offset)
	{
		TerrainQuadrant.patchWidth = patchWidth;
		TerrainQuadrant.mBitMap = mBitMap;
		TerrainQuadrant.squaredRadius = Mathf.Pow(patchWidth,2f);
		TerrainQuadrant[] quadrants = new TerrainQuadrant[9];
		
		bool isSimilar= false;
		
		quadrants[0] = new TerrainQuadrant(offset.x-patchWidth ,offset.z-patchWidth);
		quadrants[1] = new TerrainQuadrant(offset.x ,offset.z-patchWidth);
		quadrants[2] = new TerrainQuadrant(offset.x+patchWidth ,offset.z-patchWidth);
		quadrants[3] = new TerrainQuadrant(offset.x-patchWidth,offset.z);
		quadrants[4] = new TerrainQuadrant(offset.x ,offset.z);
		quadrants[5] = new TerrainQuadrant(offset.x+patchWidth,offset.z);
		quadrants[6] = new TerrainQuadrant(offset.x-patchWidth,offset.z+patchWidth);
		quadrants[7] = new TerrainQuadrant(offset.x,offset.z+patchWidth);
		quadrants[8] = new TerrainQuadrant(offset.x+patchWidth,offset.z+patchWidth);
		
		if(quadrants[0].tc == quadrants[1].tc)
			if(quadrants[1].tc == quadrants[2].tc)
				if(quadrants[2].tc == quadrants[3].tc)
					if(quadrants[3].tc == quadrants[4].tc)
						if(quadrants[4].tc == quadrants[5].tc)
							if(quadrants[5].tc == quadrants[6].tc)
								if(quadrants[6].tc == quadrants[7].tc)
									if(quadrants[7].tc == quadrants[8].tc)
								{
									isSimilar = true;
								}
		
		
		
		float totalDistance =0;
		if(!isSimilar)
		{
			for(int i=0;i<pVertices.Length;i++)
			{
				List<VTColorInfo> allVtInfoList= new List<VTColorInfo>();
				TerrainQuadrant[] tQuadrantArray = new TerrainQuadrant[System.Enum.GetValues(typeof( TerrainColorMatch)).Length];
				PatchVertex p = pVertices[i];
				
				for(int q=0;q<quadrants.Length;q++)
				{
					Vector2 ptPos = new Vector2(offset.x+p.x,offset.z+p.z);
					if(quadrants[q].ContainsPoint(ptPos))
					{
						TerrainQuadrant tq = quadrants[q];
						
						float fDistance = Mathf.Pow( SDistance(new Vector2(tq._x,tq._y),ptPos),3f);
						allVtInfoList.Add(new VTColorInfo(){ color = tq.tc.color, distanceFromVertex = fDistance,weighting =-1,vertexPosition = ptPos,quadrantCenter = new Vector2(tq._x,tq._y)});
						if(tQuadrantArray[(int)tq.tc.color]==null)
						{
							tQuadrantArray[(int)tq.tc.color] = tq;
							tq.distance =fDistance;
						}
						else
						{
							tQuadrantArray[(int)tq.tc.color].distance += fDistance;
						}
						totalDistance += fDistance;
					}
				}
				
				totalDistance =0;
				List<TerrainQuadrant> tQuadrants = new List<TerrainQuadrant>();
				foreach(TerrainQuadrant kv in tQuadrantArray)
				{
					if(kv!=null)
					{
						totalDistance +=kv.distance;
						tQuadrants.Add(kv);
					}
				}
				
				p.y =0;
				if(tQuadrants.Count==1)
				{
					p.y += tQuadrants[0].tc.Generate(WorldWrap( offset.x+p.x), WorldWrap(offset.z + p.z));
					float f = (p.y +15f)/30f;
					p.color = quadrants[4].tc.gradient.Evaluate(f);
				}
				else
				{
					float gX1 = WorldWrap(offset.x+p.x);
					float gZ1 = WorldWrap(offset.z + p.z);
					
					
					p.y = Mathf.Lerp(tQuadrants[0].tc.Generate(gX1,gZ1 ),tQuadrants[1].tc.Generate(gX1,gZ1),tQuadrants[1].distance/totalDistance);
					float f0 = (p.y)/tQuadrants[0].tc.heightScale;
					float f1 = (p.y)/tQuadrants[1].tc.heightScale;
					p.color = Color.Lerp(tQuadrants[0].tc.gradient.Evaluate(f0),tQuadrants[1].tc.gradient.Evaluate(f1),tQuadrants[1].distance/totalDistance);
				}
			}
		}
		else
		{
			TerrainQuadrant tq = quadrants[4];
			for(int i=0;i<pVertices.Length;i++)
			{
				PatchVertex p = pVertices[i];
				p.y = tq.tc.Generate(WorldWrap( offset.x+p.x), WorldWrap(offset.z + p.z));
				float f = (p.y +15f)/30f;
				p.color = tq.tc.gradient.Evaluate(f);
			}
		}

		for(int i=0;i<pVertices.Length;i++)
		{
			pVertices[i].AddAmbientOcclusion();
			int  u = PatchVertexToIndex(pVertices[i],offset);
			TerrainPregenerator.max = Mathf.Max(TerrainPregenerator.max,u);
		}

	}

	private int PatchVertexToIndex(PatchVertex pv,Vector3 offset)
	{
		float widthNorm = (mBitMap.worldLength/cellWidth) +1;

		float xNorm = (pv.Vector.x + offset.x) /cellWidth;
		float zNorm = (pv.Vector.z + offset.z) /cellWidth;

		float xIndex =  xNorm % widthNorm;
		float yIncrement = zNorm % widthNorm;
		float index = xIndex + (widthNorm * yIncrement);

		return (int)index; ///CHANGE!!!!
	}

	private float WorldWrap(float v)
	{
		return v % mBitMap.worldLength;
	}

	public static Vector3 GetNormal(Vector3 a, Vector3 b, Vector3 c)
	{
		return Vector3.Cross(b-a,c-a).normalized;
	}
	
	public static Vector3 MakePlanar ( Vector3 v0, Vector3 v2, Vector3 v3 , Vector3 v1 )
	{
		Vector3 normal = GetNormal(v0,v2,v3);
		float d = -(normal.x * v0.x)-(normal.y * v0.y)-(normal.z * v0.z);
		float y = ( - d - (normal.x*v1.x) - (normal.z*v1.z))/ normal.y;
		return new Vector3(v1.x,y,v1.z);
	}
}



