﻿using UnityEngine;
using System.Collections;
using MonoExtensions;

public class AAGunController : WeaponBase {

	public Transform pivot;
	public Transform guns;
	private Transform playerTransform;
	public ParticleSystem gunPS;
	public float bulletSpeed = 1000f;
	private Vector3 previousPlayerPos; 

	private UIUpdateInfo updateInfo;
	private EnemyDecalInfo enemyDecalInfo;

	private float health =100f;

	public GameObject explosionPrefab;

	void Awake()
	{
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		CreateUIUpdateInfo();
	}

	private void CreateUIUpdateInfo()
	{
		updateInfo = new UIUpdateInfo();
		updateInfo.uiUpdateType = UIUpdateType.DecalUpdate;
		enemyDecalInfo = new EnemyDecalInfo();
		enemyDecalInfo.decalEntity = transform;
		enemyDecalInfo.heightOffset = 60f;
		updateInfo.data = enemyDecalInfo;
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(FireBulletCoroutine());
		previousPlayerPos= playerTransform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(!isPlayerInRange())
		{
			return;
		}

		Vector3 playerVelocity = (playerTransform.position-previousPlayerPos)/Time.deltaTime;

		float distanceFromGun = Vector3.Distance(playerTransform.position,guns.position);
		float bulletTravelTime = distanceFromGun/bulletSpeed;

		Vector3 aimSpot = playerTransform.position +  (playerVelocity * bulletTravelTime);

		Vector3 pivotLookPos = new Vector3(aimSpot.x,pivot.transform.position.y,aimSpot.z);
		Vector3 pivotLookDir = pivotLookPos - pivot.position;
		Quaternion pivotLookRot = Quaternion.LookRotation(pivotLookDir);

		pivot.rotation = Quaternion.Lerp(pivot.rotation, pivotLookRot,20f * Time.deltaTime);


		Vector3 basePlayerPos = new Vector3(aimSpot.x,guns.transform.position.y,aimSpot.z);
		Vector3 fromDir = basePlayerPos - guns.position;
		Vector3 toDir = aimSpot - guns.position;

		Debug.DrawLine(guns.position,guns.position+fromDir*100f, Color.green);
		Debug.DrawLine(guns.position,guns.position+toDir*100f, Color.green);

		float angle = fromDir.AngleDir(toDir,guns.right);
		guns.localRotation = Quaternion.Lerp(guns.localRotation, Quaternion.Euler(angle,0,0),20f * Time.deltaTime);
		previousPlayerPos= playerTransform.position;

		enemyDecalInfo.healthPercentage =health/100f;
		NotificationCenter.Post(NotificationType.UIUpdate,updateInfo);
	}

	private float range = 900f;

	private bool isPlayerInRange()
	{
		float distance = Vector3.Distance(transform.position,new Vector3(playerTransform.position.x,transform.position.y,playerTransform.position.z));
		return distance<range;
	}

	IEnumerator FireBulletCoroutine()
	{
		while(true)
		{
			if(isPlayerInRange())
			{
				gunPS.Emit(gunPS.transform.position,gunPS.transform.forward*bulletSpeed,gunPS.startSize,gunPS.startLifetime,gunPS.startColor);
			}
			yield return new WaitForSeconds(1f);
		}
	}

	bool isDead = false;
	public void ApplyDamage(DamageInfo damageInfo)
	{
		if(health>0 && !isDead)
		{
			health-=damageInfo.damage;
		}
		else
		{
			isDead = true;
			Instantiate(explosionPrefab,transform.position,Quaternion.identity);
			Destroy(gameObject);
		}
	}
}
