﻿using System;
using System.Text;
using System.IO;


/// <summary>
/// Summary description for Class1.
/// </summary>
class Uncommenter
{
	public enum status {SINGLE_LINE = 1, MUL_LINE = 2}
	/// <summary>
	/// The main entry point for the application.
	/// </summary>
	public static string doUncomment(string file)
	{
		StringReader reader = new StringReader(file);
		string line = null;
		StringBuilder temp = new StringBuilder();
		StringBuilder buffer = new StringBuilder();
		status type = (status) 0;
		int linenbr = 0;

		
		while((line = reader.ReadLine()) != null)
		{
			linenbr++;
			
			int spos = line.IndexOf("/*");
			int mpos = line.IndexOf("//");

			if(type != status.MUL_LINE)
			{
				if(spos != -1)
				{
					type = status.MUL_LINE;
					
					if(mpos != -1)
						if(mpos < spos)
							type = status.SINGLE_LINE;
					
					if(type == status.MUL_LINE)
					{
						if(!checkInStr(line, spos))
						{
							if(line.Length > 1)
								buffer.Append(line.Substring(0, spos));
							
						}
						else
						{
							buffer.Append(line + "\n");
							type = 0;
						}
					}
				}
				else if(mpos != -1)
					type = status.SINGLE_LINE;
				else
					buffer.Append(line + "\n");
				
			}
			
			if(type == status.SINGLE_LINE)
			{
				if(!checkInStr(line, mpos))
				{
					if(line.Length > 1)
						buffer.Append(line.Substring(0, mpos));
					type = 0;
				}
				else
				{
					buffer.Append(line + "\n");
				}
				
			}
			
			if(type == status.MUL_LINE)
			{
				int pos = line.IndexOf("*/");
				
				if(pos != -1)
				{
					pos += 2;
					if(pos > line.Length)
						buffer.Append(line.Substring(pos, line.Length - 1));
					
					type = 0;
				}
			}
			
		}
		reader.Close();
		return buffer.ToString();
	}
	
	static bool checkInStr(string lineb, int compos)
	{
		bool sflag = false;
		
		for(int i = 0; i < lineb.Length; i++)
		{
			if(lineb[i] == '\x22')
				sflag = !sflag;
			
			if((sflag == false) && i == compos)
				return false;
		}
		
		return true;
	}
}
