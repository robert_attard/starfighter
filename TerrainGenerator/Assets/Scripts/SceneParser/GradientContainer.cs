using UnityEngine;
using System.Collections;

[System.Serializable]
public struct GradientInfo{
	public string id;
	public Gradient gradient;
}

public class GradientContainer : ScriptableObject {

	public GradientInfo[] gradientColors;
}


