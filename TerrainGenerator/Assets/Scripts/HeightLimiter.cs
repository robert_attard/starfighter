﻿using UnityEngine;
using System.Collections;

public class HeightLimiter : MonoBehaviour {

	public VehicleController playerVehicle;

	// Update is called once per frame
	void LateUpdate () {
		transform.position = new Vector3(playerVehicle.transform.position.x,transform.position.y,playerVehicle.transform.position.z);
	}
}
