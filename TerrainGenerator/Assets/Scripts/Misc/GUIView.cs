﻿using UnityEngine;
using System.Collections;

public class GUIView : MonoBehaviour {

	public delegate void GUIViewHandler();
	public static GUIViewHandler gui;

#if DEBUGVIEW
	void OnGUI()
	{
		if(gui!=null)
		{
			gui();
		}
	}
#endif
}
