﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapBitMap : MonoBehaviour {

	public Texture2D bitmap;
	private TerrainCharacteristic[] terrainCharacteristics;
	private Color32[] colours;
	private TerrainColorMatch[] cMatchList;
	private Dictionary<TerrainColorMatch,TerrainCharacteristic> terrainCDictionary = new Dictionary<TerrainColorMatch, TerrainCharacteristic>();

	private float scale;
	public float worldLength = 40000f;
	private int width;
	
	public GameObject planePrefab;

	bool CompareColor(Color32 a,Color32 b)
	{
		bool result = true;
		result &= a.r==b.r;
		result &= a.g==b.g;
		result &= a.b==b.b;
		return result;
	}

	private float cellWidth;
	public void Init(float cellWidth)
	{
		this.cellWidth = cellWidth;
		colours = bitmap.GetPixels32();
		width = bitmap.width;
		cMatchList = new TerrainColorMatch[colours.Length];
		worldLength =  Mathf.Floor(worldLength/cellWidth)  * cellWidth;
		scale = worldLength/(bitmap.width * cellWidth);


		for (int i = 0; i < colours.Length; i++) {
			Color32 c = colours [i];
			if (CompareColor(c,new Color32 (0, 0, 0, 255))) {
				cMatchList[i] = TerrainColorMatch.Black;
			}

			if (CompareColor(c,new Color32 (255, 255, 255, 255))) {
				cMatchList[i] = TerrainColorMatch.White;
			}

			if (CompareColor(c,new Color32 (255, 0, 0, 255))) {
				cMatchList[i] = TerrainColorMatch.Red;
			}

			if (CompareColor(c,new Color32 (0, 255, 0, 255))) {
				cMatchList[i] = TerrainColorMatch.Green;
			}

			if (CompareColor(c,new Color32 (0, 0, 255, 255))) {
				cMatchList[i] = TerrainColorMatch.Blue;
			}

			if (CompareColor(c,new Color32 (255, 255, 0, 255))) {
				cMatchList[i] = TerrainColorMatch.Yellow;
			}

			if (CompareColor(c,new Color32 (255, 0, 255, 255))) {
				cMatchList[i] = TerrainColorMatch.Magenta;
			}

			if (CompareColor(c,new Color32 (0, 255, 255, 255))) {
				cMatchList[i] = TerrainColorMatch.Cyan;
			}
		}

		TerrainCharacteristic[] tc = GetComponents<TerrainCharacteristic>();
		foreach(TerrainCharacteristic t in tc)
		{
			t.Init(cellWidth);
			terrainCDictionary.Add(t.color,t);
		}
	}

	public TerrainCharacteristic GetHeightMapInfo(float x, float y)
	{
		float currentX = x/(scale * cellWidth);
		float currentY = y/(scale * cellWidth);
//		print ( currentX + " % " +width + "     " + currentX % width);
		float modx =   Mathf.Abs(currentX % width);
		float mody = Mathf.Abs(currentY % width);



		int xIndex =  (int)modx;
		int yIncrement = (int)mody;
		int index = xIndex + ((int)width * yIncrement);
		return terrainCDictionary[cMatchList[index]];
	}

}
