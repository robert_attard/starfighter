﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ThreadPooling;

public enum MeshThreadState
{
	Idle,
	ProcessingDoNotDisturb,
	ReadyForRendering,
	Rendering
}

public class MeshThreadedJob
{
	private Vector3 position;
	private Mesh mesh;
	private MeshPatchTemplate mPatchTemplate;
	private Material terrainMat;
	private MeshThreadState state;
	private float patchWidth;
	public MeshCollider meshCollider;
	public GameObject terrainObject;
	public float timeStamp;
	public MeshThreadState State
	{
		get{
			return state;
		}
		internal set{
			state = value;
		}
	}
	
	public MeshThreadedJob(Material terrainMaterial, int vertexcolumns,float cellWidth,MapBitMap mBitMap,float patchWidth, int meshColliderDivisor)
	{
		this.patchWidth = patchWidth;
		this.terrainMat = terrainMaterial;
		mPatchTemplate = new MeshPatchTemplate(vertexcolumns,cellWidth,mBitMap,meshColliderDivisor);
		mesh = new Mesh();
		State = MeshThreadState.Idle;
	}

	public MeshThreadedJob SubmitForProcessing(Vector3 position)
	{
		this.position = position;
		State = MeshThreadState.ProcessingDoNotDisturb;
		ThreadPool.QueueUserWorkItem(ThreadProcess,this);
		return this;
	}
	
	public bool CheckRenderJob()
	{
		switch(State)
		{
		case MeshThreadState.ReadyForRendering:
			timeStamp = Time.time;
			terrainObject = new GameObject("TerrainCollider");
			terrainObject.layer = 9;
			terrainObject.transform.position = position;
			MeshRenderer mr = terrainObject.AddComponent<MeshRenderer>();
			mr.material = terrainMat;
			mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			mr.receiveShadows =true;
			MeshFilter mf = terrainObject.AddComponent<MeshFilter>();
			mf.mesh = new Mesh();
			mf.mesh.vertices = mPatchTemplate.hVertices;
			mf.mesh.triangles = mPatchTemplate.triangleIndices;
			mf.mesh.colors = mPatchTemplate.colors;
			mf.mesh.normals = mPatchTemplate.normals;
			mf.mesh.RecalculateBounds();

			MeshCollider  mc= terrainObject.AddComponent<MeshCollider>();
			mc.enabled = false;
			mc.sharedMesh = new Mesh();
			mc.sharedMesh.vertices = mPatchTemplate.meshColliderMeshVertices;
			mc.sharedMesh.triangles = mPatchTemplate.meshColliderTriangleIndices;
			mc.sharedMesh.RecalculateBounds();
			mc.sharedMesh.RecalculateNormals();
			mc.enabled = true;
			State = MeshThreadState.Rendering;
			break;
		case MeshThreadState.Rendering:
			return false;
			break;
		}
		return true;
	}
	
	private void ThreadProcess(object meshJob)
	{
		MeshThreadedJob mt = (MeshThreadedJob)meshJob;
		mt.mPatchTemplate.UpdateMeshProperties(position);
		State = MeshThreadState.ReadyForRendering;
	}
}