﻿using UnityEngine;
using System.Collections;

public class ImageBasedGenerator : Fractal {

	public Texture2D texture;
	private Color[] textureColors;
	private float[,] map2DValues;
	
	private int width;
	public float textureWorldScale =1f;
	public float heightScale=1;

	public void Process()
	{
		ValidateTexture();
		textureColors = texture.GetPixels ();
		width = texture.width;
		sqrMagnitudes = new float[4];
		neighbourValues = new float[4];
		map2DValues = new float[width,width];
		for (int i=0;i<textureColors.Length;i++)
		{
			Color c = textureColors[i];
			float average = (c.r + c.g + c.b)/3f;
			map2DValues[i%width, (int)Mathf.Floor(i/width)] = average;
		}
	}

	float[] sqrMagnitudes;
	float[] neighbourValues;
	float nx;
	float ny;
	float _x;
	float _y;
	int xMin;
	int xMax;
	int yMin;
	int yMax;	
	int pixels;

	public override double Generate(float x,float y)
	{
		 nx = x/textureWorldScale - Mathf.Floor(x/textureWorldScale);
		 ny = y/textureWorldScale - Mathf.Floor(y/textureWorldScale);
		_x = nx * (width-1);
		_y = ny * (width-1);
		xMin = (int)Mathf.Floor(_x);
		xMax = (int)Mathf.Min(Mathf.Ceil(_x),width);
		yMin = (int)Mathf.Floor(_y);
		yMax = (int)Mathf.Min(Mathf.Ceil(_y),width);	
		pixels= (((xMax -xMin)+1) * ((yMax - yMin)+1));

		float mSum =0;
		int counter =0;

		for(int v = yMin;v<=yMax;v++)
		{
			for(int u = xMin;u<=xMax;u++)
			{
				float diffx = Mathf.Abs(_x-u);
				float diffy = Mathf.Abs(_y-v);
				float magnitude =  (new Vector2(diffx,diffy)).magnitude;
				
				mSum+=magnitude;
				sqrMagnitudes[counter] = magnitude;
				neighbourValues[counter] = map2DValues[u,v];
				counter++;
			}
		}

		float s=0;
		for(int p=0;p<pixels;p++)
		{
			float sq = sqrMagnitudes[p];
			float v = 1-( (sq==0?0:(sq/mSum)) );
			s+=v;
			sqrMagnitudes[p] = v;
		}
		float result =0;

		for(int p=0;p<pixels;p++)
		{
			result += neighbourValues[p] * (sqrMagnitudes[p] /=s);
		}
		
		return (double)(result * heightScale);
	}


	public void ValidateTexture()
	{
		if(texture.width!=texture.height && texture.mipmapCount>0)
		{
			Debug.LogError("Invalid Texture");
		}
	}

}
