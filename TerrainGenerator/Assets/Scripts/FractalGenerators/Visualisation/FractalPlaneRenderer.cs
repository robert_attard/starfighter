﻿using UnityEngine;
using System.Collections;

public class FractalPlaneRenderer : MonoBehaviour {
	public int textureWidth = 200;
	private Color[] colors;
	private Texture2D texture;
	private Fractal fractal;

	public float xOffset =0;


	// Use this for initialization
	void Start () {
		fractal = GetComponent<Fractal>();
		texture =  new Texture2D(textureWidth,textureWidth);
		colors = new Color[textureWidth*textureWidth];
	}
	
	// Update is called once per frame
	void Update () {

		for(int i=0;i<colors.Length;i++)
		{
			float x = xOffset +(i)%textureWidth;
			float y = (i)/textureWidth;
//			float noise = pn.FractalNoise2D ((x)*scale,y*scale,octaves,frq,amp);
			float noise = (float)fractal.Generate(x,y);
			colors[i]= new Color(noise,noise,noise);
		}
		texture.SetPixels(colors);
		texture.Apply();
		
		Renderer r =GetComponent<Renderer>();
		r.material.mainTexture = texture;

	}
}
