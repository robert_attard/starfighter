﻿using UnityEngine;
using System.Collections;

public abstract class Fractal : MonoBehaviour{
	public float maxValue;
	public float minValue;
	public abstract double Generate( float x, float y );
}
