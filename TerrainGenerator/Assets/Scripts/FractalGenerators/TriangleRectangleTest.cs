﻿using UnityEngine;
using System.Collections;

public class TriangleRectangleTest : MonoBehaviour {

	private  static float x0;
	private  static float y0;
	private  static float x1;
	private  static float y1;
	private  static float x2;
	private  static float y2;
	private  static float l;
	private  static float r;
	private  static float t;
	private  static float b;
	
	private  static float top_intersection;
	private  static float bottom_intersection;
	private  static float toptrianglepoint;
	private  static float bottomtrianglepoint;
	
	private static float m;
	private static float c;
		// THESE ARE THE TWO FUNCTIONS YOU CAN OPTIMISE.
		// I realise that you could inline the lineRectangleIntersect method, but I'd rather not do that just yet...
		// I'm looking for maths/logic improvements.
		
	public static bool TriangleAABBIntersectionTest(Rect rect,Vector2  vertex0, Vector2 vertex1, Vector2 vertex2)
	{
		// YOU MUST LEAVE THESE DECLARATIONS; they simulate necessary data exchange within PV3D
		l = rect.xMin;
		r = rect.xMax;
		t = rect.yMax;
		b = rect.yMin;
		
		x0 = vertex0.x;
		y0 = vertex0.y;
		x1 = vertex1.x;
		y1 = vertex1.y;
		x2 = vertex2.x;
		y2 = vertex2.y;
		return LineRectangleIntersect(x0,y0,x1,y1) || LineRectangleIntersect(x1,y1,x2,y2) || LineRectangleIntersect(x2,y2,x0,y0);
	}

	public static bool Test(Rect rect, Vector3[] trianglePoints)
	{

		float triangleMinX = float.MaxValue;
		float triangleMinY = float.MaxValue;

		float triangleMaxX = float.MinValue;
		float triangleMaxY = float.MinValue;

		for(int i=0;i<trianglePoints.Length;i++)
		{
			if(trianglePoints[i].x<triangleMinX)
			{
				triangleMinX = trianglePoints[i].x;
			}
			if(trianglePoints[i].x>triangleMaxX)
			{
				triangleMaxX = trianglePoints[i].x;
			}
			if(trianglePoints[i].z<triangleMinY)
			{
				triangleMinY = trianglePoints[i].z;
			}
			if(trianglePoints[i].z>triangleMaxY)
			{
				triangleMaxY = trianglePoints[i].z;
			}
		}

		if (rect.xMax < triangleMinX) return false; // a is left of b
		if (rect.xMin > triangleMaxX) return false; // a is right of b
		if (rect.yMax < triangleMinY) return false; // a is above b
		if (rect.yMin > triangleMaxY) return false; // a is below b

		return true;
	}


	
	public static bool LineRectangleIntersect(float x0,float y0,float x1,float y1)
	{
		// Calculate m and c for the equation for the line (y = mx+c)
		m = (y1-y0) / (x1-x0);
		c = y0 -(m*x0);
		
		// if the line is going up from right to left then the top intersect point is on the left
		if(m>0)
		{
			top_intersection = (m*l  + c);
			bottom_intersection = (m*r  + c);
		}
		// otherwise it's on the right
		else
		{
			top_intersection = (m*r  + c);
			bottom_intersection = (m*l  + c);
		}
		
		// work out the top and bottom extents for the triangle
		if(y0<y1)
		{
			toptrianglepoint = y0;
			bottomtrianglepoint = y1;
		}
		else
		{
			toptrianglepoint = y1;
			bottomtrianglepoint = y0;
		}
		
		float topoverlap;
		float botoverlap;
		
		// and calculate the overlap between those two bounds
		topoverlap = top_intersection>toptrianglepoint ? top_intersection : toptrianglepoint;
		botoverlap = bottom_intersection<bottomtrianglepoint ? bottom_intersection : bottomtrianglepoint;

		return (topoverlap<botoverlap) && (!((botoverlap<t) || (topoverlap>b)));
	}
}
