using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ThreadPooling;


public class TerrainOCRenderer : MonoBehaviour {
	
	public class TerrainPatchUpdateInfo
	{
		public float timeStamp;
		public GameObject gameObject;
		public Vector3 position;
	}
	
	public Dictionary<Vector3,TerrainPatch> drawnPatches = new Dictionary<Vector3,TerrainPatch>();
	
	[HideInInspector]
	public Vector3[] tPts;
	public float fovScale = 1.3f;
	public Material terrainMaterial;
	public int vertexColumns = 10;
	public float patchWidth = 100f;
	public MapBitMap mBitMap;
	public float farClip = 4000f;
	
	private Rasterizer rasterizer;
	private MeshPatchTemplate mPatchTemplate;
	private float CellWidth;
	private static TerrainOCRenderer instance;
	private Edge[] edges;
	
	public int meshColliderDivisor = 3;
//	private void OnValidate()
//	{
//		float remainder = (float)(vertexColumns-1)/(float)meshColliderDivisor;
//		float fraction = remainder - Mathf.Floor(remainder);
//		if(fraction ==0)
//		{
//			return;
//		}
//		if(fraction>=0.5)
//		{
//			vertexColumns++;
//		}
//		if(fraction<0.5f)
//		{
//			vertexColumns--;
//		}
//		
//		float g = vertexColumns/meshColliderDivisor;
//		if(!IsEvenNumber(g))
//		{
//			vertexColumns =(int)( ((g-1)*meshColliderDivisor)+1);
//		}
//	}
	
	private bool IsEvenNumber(float number)
	{
		return (number % 2 )==0;
	}
	
	public static TerrainOCRenderer Instance
	{
		get{
			if(instance==null)
			{
				instance = FindObjectOfType<TerrainOCRenderer>();
			}
			return instance;
		}
	}

	private List<TerrainPatch> activePatches = new List<TerrainPatch>();
	private List<TerrainPatch> inactivePatches = new List<TerrainPatch>();
	public const int maxTerrainCacheCount = 700;
	
	private void CreateTerrainPatchPool()
	{
		for(int i=0;i<maxTerrainCacheCount;i++)
		{
			TerrainPatch newPatch = TerrainPatch.Create(terrainMaterial,vertexColumns,CellWidth,mBitMap,patchWidth,meshColliderDivisor);
			newPatch.transform.parent = transform;
			inactivePatches.Add(newPatch);
			newPatch.gameObject.SetActive(false);
		}
	}
	
	void Awake()
	{
		if(instance!=null)
		{
			instance = this;		
		}
		#if !UNITY_EDITOR
		Application.targetFrameRate = 60;
		#endif
		CellWidth = patchWidth/(float)(vertexColumns-1);
		mBitMap.Init(CellWidth);
	}
	
	void Start()
	{
		ThreadPool.InitInstance(20,4);
		CreateTerrainPatchPool();
	}

	public int terrainPatchCount =0;
	
	private void Update()
	{
		tPts = CreateCameraRectangle(Camera.main,fovScale);
		DrawTriangle(tPts[0].x,tPts[0].z,tPts[1].x,tPts[1].z,tPts[2].x,tPts[2].z);
	}
	
	private Vector2 ConvertToVector2(Vector3 v)
	{
		return new Vector2(v.x,v.z);
	}
	
	public Vector3[] CreateCameraRectangle(Camera camera, float scaleOffset)
	{
		float diff = (farClip * scaleOffset - farClip)/2f;
		
		Vector3[] Pts = new Vector3[3];
		Pts[0] = camera.transform.position - (camera.transform.forward * diff);
		
		float farClipLength = farClip * scaleOffset;
		float vFOVrad = Camera.main.fieldOfView * Mathf.Deg2Rad;
		float cameraHeightAt1 = Mathf.Tan(vFOVrad *0.5f);
		float hFOVrad = Mathf.Atan(cameraHeightAt1 * Camera.main.aspect) * 2f;
		float hFOV = (hFOVrad * Mathf.Rad2Deg)/2f;
		float otherAngle = 180f-hFOV-90f;
		
		float a  = (farClipLength/Mathf.Sin(Mathf.Deg2Rad*otherAngle))* Mathf.Sin(Mathf.Deg2Rad*hFOV);
		
		Pts[1] = Pts[0]+ (camera.transform.forward*farClipLength) + (-camera.transform.right * a);
		Pts[2] = Pts[0]+ (camera.transform.forward*farClipLength) + (camera.transform.right * a);
		return Pts;
	}
	
	private void SetTerrainPatch(float x, float y)
	{
		Vector3 pos = new Vector3(x,0,y);
		if(!drawnPatches.ContainsKey(pos))
		{
			drawnPatches.Add(pos,GetTerrainPatch(pos));
		}
		else
		{
			drawnPatches[pos].timeStamp = Time.time;
		}
	}

	private TerrainPatch GetTerrainPatch(Vector3 position)
	{
		if(inactivePatches.Count==0)
		{
//			Debug.Log("Resupplying inactivePatches List");
			activePatches.Sort((a,b)=>{ return a.timeStamp.CompareTo(b.timeStamp);});
			for(int i=0;i<100;i++)
			{
				activePatches[i].gameObject.SetActive(false);
				drawnPatches.Remove (activePatches[i].Position);
			}
			inactivePatches.AddRange(activePatches.GetRange(0,100));

			activePatches.RemoveRange(0,100);
		}

		TerrainPatch t = inactivePatches[0];
		t.timeStamp = Time.time;
		inactivePatches.RemoveAt(0);
		t.SubmitForProcessing(position);
		activePatches.Add(t);
		return t;
	}
	
	public void DrawTriangle(float x1, float y1,float x2, float y2,float x3, float y3)
	{
		y1= ConvertToPixelGrid(y1);
		y2= ConvertToPixelGrid(y2);
		y3= ConvertToPixelGrid(y3);
		
		List<Vector2> vertices = new List<Vector2>();
		vertices.Add(new Vector2(x1,y1));
		vertices.Add(new Vector2(x2,y2));
		vertices.Add(new Vector2(x3,y3));
		
		vertices.Sort((a,b)=>{return a.y.CompareTo(b.y);});
		
		// create edges for the triangle
		edges = new Edge[]{ new Edge(new Vector2(x1,y1),new Vector2(x2,y2)),
			new Edge(new Vector2(x2,y2),new Vector2(x3,y3)),
			new Edge(new Vector2(x3,y3),new Vector2(x1,y1))};
		
		Vector2 middlePt = vertices[1];
		Vector2 slicePt = Vector2.zero;
		
		foreach(Edge e in edges)
		{
			float x =middlePt.x;
			float yResult = e.Intercept;
			
			if(e.Gradient !=0)
			{
				x = (middlePt.y - e.Intercept)/e.Gradient;
				yResult = e.Gradient*x + e.Intercept;
			}
			
			if( !(Mathf.Round(x) == Mathf.Round(middlePt.x)) ||  !(Mathf.Round(yResult) == Mathf.Round(middlePt.y)))
			{
				slicePt = new Vector2(x,ConvertToPixelGrid( middlePt.y));
				break;
			}
		}
		
		if(Mathf.Approximately(middlePt.y,vertices[0].y))
		{
			if(vertices[0].x>=middlePt.x)
			{
				DrawSpansBetweenEdges(new Edge(middlePt,vertices[2]), new Edge(vertices[0],vertices[2]));
				return;
			}
			else
			{
				DrawSpansBetweenEdges(new Edge(vertices[0],vertices[2]), new Edge(middlePt,vertices[2]));
				return;
			}
		}
		
		if(slicePt.x>=middlePt.x)
		{
			DrawSpansBetweenEdges(new Edge(vertices[0],middlePt), new Edge(vertices[0],slicePt));
			DrawSpansBetweenEdges(new Edge(middlePt,vertices[2]), new Edge(slicePt,vertices[2]));
		}
		else
		{
			DrawSpansBetweenEdges(new Edge(vertices[0],slicePt), new Edge(vertices[0],middlePt));
			DrawSpansBetweenEdges(new Edge(slicePt,vertices[2]), new Edge(middlePt,vertices[2]));
		}
		return;
	}
	
	private void DrawSpansBetweenEdges(Edge e1, Edge e2)
	{
		for(float y =  e2.Y1; y <   e2.Y2; y+=patchWidth) {
			float X1 = (float.IsInfinity((y-e1.Intercept)/e1.Gradient)?e1.X1:(y-e1.Intercept)/e1.Gradient);
			float X2 = (float.IsInfinity((y-e2.Intercept)/e2.Gradient)?e2.X1:(y-e2.Intercept)/e2.Gradient);
			
			Span span= new Span( X1 , X2);
			DrawSpan(span, y);
		}
	}
	
	private void DrawSpan(Span span, float y)
	{
		for(float x =ConvertToPixelGrid( span.X1); x <= ConvertToPixelGrid( span.X2); x+=patchWidth) {
			SetTerrainPatch(x,y);
		}
	}
	
	private Vector3 ToZ(Vector2 a)
	{
		return new Vector3(a.x,0,a.y);
	}
	
	public float ConvertToPixelGrid(float coordinate)
	{
		return Mathf.Floor(coordinate/patchWidth)*patchWidth;
	}
	
	private void OnDrawGizmos()
	{
		if(!Application.isPlaying)
		{
			tPts = CreateCameraRectangle(Camera.main,fovScale);
		}
		
		if(tPts!=null && tPts.Length>0)
		{
			Gizmos.color = new Color(1f,0,0,0.4f);
			
			for (int i = 1; i < tPts.Length; i++) {
				Gizmos.DrawLine(tPts [i-1],tPts [i]);
				Gizmos.DrawSphere(tPts [i-1],5f);
			}
			Gizmos.DrawLine(tPts [tPts.Length-1],tPts [0]);
			Gizmos.DrawSphere(tPts [tPts.Length-1],5f);
		}
	}
	
}
