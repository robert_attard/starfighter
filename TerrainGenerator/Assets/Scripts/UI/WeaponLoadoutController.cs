﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

[RequireComponent(typeof(CanvasGroup))]
public class WeaponLoadoutController : MonoBehaviour {
	private RectTransform rectTransform;
	private CanvasGroup cGroup;
	private float width;

	// Use this for initialization
	void Start () {
		cGroup = GetComponent<CanvasGroup>();
		rectTransform = GetComponent<RectTransform>();
		cGroup.alpha =1;
		width = (rectTransform.rect.width);
		rectTransform.anchoredPosition = new Vector2(width,rectTransform.anchoredPosition.y);
		tween = HOTween.To(rectTransform,0.3f,new TweenParms().Prop("anchoredPosition",new Vector2(0,rectTransform.anchoredPosition.y),false)
		                   .Ease(EaseType.EaseOutQuad)
		                   .OnRewinded(()=>{gameObject.SetActive(false);})
		                   .AutoKill(false));
		tween.Pause();
	}

	Tweener tween;
	public void isEnabled(bool isEnabled)
	{
		if(isEnabled)
		{
			gameObject.SetActive(true);
			tween.PlayForward();
		}
		else
		{
			tween.PlayBackwards();
		}
	}

	public void Close()
	{
		Debug.Log("Closing");
		isEnabled(false);
	}
}
