﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class StrafeUIController : MonoBehaviour {

	public StrafeIndicatorItem[] strafeItems;
	public delegate void StrafeUpdateHandler(float strafeDir);
	public StrafeUpdateHandler StrafeUpdate;
	public InputScroller StrafeInputController;
	public CanvasGroup cGroup;
	private float currentStrafe =0;
	private int start;
	private float fadeTimer =float.MinValue;
	private Tweener fadeTween;

	private void OnStrafeUpdate(float strafeUIValue)
	{
		if(StrafeUpdate!=null)
		{
			StrafeUpdate(strafeUIValue);
		}
	}

	void Awake() {
		start = -Mathf.FloorToInt(strafeItems.Length/2f);
		int end = -(start);
		for(int i=start;i<end+1;i++)
		{
			strafeItems[i+(-start)].InitUI(this,i);
		}
	}

	void Start()
	{
		cGroup.alpha=0;
		InputScroller.DragVerticalDelta += HandleVerticalDrag;
		fadeTween = HOTween.To(cGroup,0.2f,new TweenParms().Prop("alpha",1f,false).AutoKill(false).Ease(EaseType.EaseInOutQuad));
		fadeTween.Pause();
	}

	void Update()
	{
		currentStrafe = Mathf.Lerp(currentStrafe,StrafeInputController.output.x*-start,12f * Time.deltaTime);
		OnStrafeUpdate(currentStrafe);
		if(Time.time>fadeTimer && !fadeTween.isReversed)
		{
			fadeTween.PlayBackwards();
		}
	}

	void HandleVerticalDrag (float swipeAxis)
	{
		fadeTimer = Time.time + 0.2f;
		if(!fadeTween.isComplete)
		{
			fadeTween.PlayForward();
		}
	}

}
