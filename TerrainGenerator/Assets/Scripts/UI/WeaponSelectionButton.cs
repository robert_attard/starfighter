﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;
using UnityEngine.EventSystems;

public class WeaponSelectionButton : MonoBehaviour ,IPointerDownHandler,IDragHandler{

	private const float fadeSpeed = 0.2f;
	private const float entrySpeed = 0.2f;
	public Image baseImage;
	public Image highlightImage;
	public WeaponLoadoutController wLoadoutController;

	private float alpha;
	public float Alpha {
		get {
			return alpha;
		}
		set {
			alpha = value;
			baseImage.color = new Color(1f,1f,1f,1f-alpha);
			highlightImage.color = new Color(1f,1f,1f,alpha);
		}
	}

	private Sequence toggleSequence;
	private RectTransform rectTransform;
	private List<WeaponSelectionButton> otherButtons = new List<WeaponSelectionButton>();
	public bool isDefault;

	// Use this for initialization
	void Start () {
		Init();
		if(isDefault)
		{
			OnPointerDown(null);
		}
	}

	private void Init()
	{
		if(isInit)
		{
			return;
		}
		isInit = true;
		Alpha =0;
		WeaponSelectionButton[] weaponBtns =transform.parent.GetComponentsInChildren<WeaponSelectionButton>();
		foreach(WeaponSelectionButton w in weaponBtns)
		{
			if(w!=this)
			{
				otherButtons.Add(w);
			}
		}
		rectTransform = GetComponent<RectTransform>();
		toggleSequence = new Sequence(new SequenceParms().AutoKill(false));
		Vector2 initialPos = rectTransform.anchoredPosition;
		toggleSequence.Insert(0,HOTween.To(this,fadeSpeed,new TweenParms().Prop("Alpha",1f,false).Ease(EaseType.EaseInOutQuad)));
		toggleSequence.Insert(0,HOTween.To(rectTransform,entrySpeed,new TweenParms().Prop("anchoredPosition",new Vector2(-43f,initialPos.y),false).Ease(EaseType.EaseOutBack)));
	}

	private bool isEnabled = false;
	private bool isInit = false;
	public bool IsEnabled {
		get {
			return isEnabled;
		}
		set {
			Init();
			isEnabled = value;
			if(isEnabled)
			{
				toggleSequence.PlayForward();
			}
			else
			{
				toggleSequence.PlayBackwards();
			}
		}
	}

	#region UIHandlers implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		IsEnabled=true;
		foreach(WeaponSelectionButton w in otherButtons)
		{
			w.IsEnabled = false;
		}
	}

	public void OnDrag (PointerEventData eventData)
	{
		
		if(Mathf.Sign(eventData.delta.x)==-1)
		{
			wLoadoutController.isEnabled(true);
		}
	}

	#endregion
}
