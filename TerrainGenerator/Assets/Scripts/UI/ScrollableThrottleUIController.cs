﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;


public class ScrollableThrottleUIController : MonoBehaviour {
	public DialItem dialText;
	public int dialSpan = 7;
	public RectTransform dialBoxRectTransform;
	public RectTransform dialPivotRectTransform;
	public CanvasGroup dialPivotCanvasGroup;

	public AnimationCurve dialAlphaCurve;
	public AnimationCurve dialScaleCurve;
	public AnimationCurve dialDistanceCurve;

	[HideInInspector]
	public const int dialValueMultiplier = 5;
	public int minimumValue = -30;
	public delegate void OnUpdateDialHandler(float delta);
	public static OnUpdateDialHandler UpdateDelta;

	private float dialItemHeight;
	private float dialBoxHeight;
	[HideInInspector]
	public int halfSpan;

	private DialItem bottomDial;
	private DialItem topDial;
	private DialItem zeroDial;
	private Tweener dialPivotCanvasGroupTweener;
	private float visibilityTimer =float.MinValue;
	private bool isReversingVisibility = false;

	public delegate void OnThrottleUpdateHandler(float throttle);
	public static OnThrottleUpdateHandler Throttle;

	public void OnThrottleUpdate(float throttle)
	{
		if(Throttle!=null)
		{
			Throttle(throttle);
		}
	}

	void OnValidate()
	{
		if(dialSpan%2==0)
		{
			dialSpan++;
		}
	}

	// Use this for initialization
	void Start () {
		InputScroller.DragVerticalDelta += HandleDragVerticalDelta;
		dialBoxHeight = dialBoxRectTransform.rect.height;
		dialItemHeight = dialText.rectTransform.rect.height;

		halfSpan = Mathf.CeilToInt( dialSpan/2f);

		int start = minimumValue/dialValueMultiplier;
		int end= (100/dialValueMultiplier)+1;
		for(int i=start;i<end;i++)
		{
			DialItem newDial = Instantiate<DialItem>(dialText);
			newDial.rectTransform.SetParent(dialPivotRectTransform.transform);
			newDial.SetStartIndex((float)i,i,this,dialBoxHeight,halfSpan);

			if(i==start)
			{
				bottomDial = newDial;
			}

			if(i==end-1)
			{
				topDial = newDial;
			}

			if(i==0)
			{
				zeroDial = newDial;
			}
		}
		dialText.gameObject.SetActive(false);
		dialPivotCanvasGroup.alpha =0;
		dialPivotCanvasGroupTweener = HOTween.To(dialPivotCanvasGroup,0.2f,new TweenParms().Prop("alpha",1,false).AutoKill(false).Ease(EaseType.EaseInOutQuad));
		dialPivotCanvasGroupTweener.Pause();
	}

	void OnDestroy()
	{
		InputScroller.DragVerticalDelta -= HandleDragVerticalDelta;
	}

	public float dragSensitivity = 10f;
	void HandleDragVerticalDelta (float swipeAxis)
	{
		OnUpdateDial(-swipeAxis * dragSensitivity);
	}


	void Update()
	{
		if(Time.time>visibilityTimer && !isReversingVisibility && !dialPivotCanvasGroupTweener.isReversed)
		{
			isReversingVisibility =true;
			dialPivotCanvasGroupTweener.PlayBackwards();
		}
	}


	private void OnUpdateDial(float delta)
	{
		StopAllCoroutines();
		isReseting=false;
		if(topDial.index +delta<0)
		{
			return;
		}
		if(bottomDial.index +delta>0)
		{
			return;
		}
		if(dialPivotCanvasGroupTweener.isPaused)
		{
			isReversingVisibility = false;
			dialPivotCanvasGroupTweener.PlayForward();
		}
		if(UpdateDelta!=null)
		{
			visibilityTimer = Time.time +0.1f;
			UpdateDelta(delta);
		}
	}

	bool isReseting = false;
	public void ResetThrottle()
	{
		if(!isReseting)
		{
			StartCoroutine(ResetThrottleCoroutine());
			isReseting = true;
		}
	}

	IEnumerator ResetThrottleCoroutine()
	{
		float dec = (zeroDial.index>=0)?Mathf.Floor(zeroDial.index):Mathf.Ceil(zeroDial.index);
		dec = (zeroDial.index - dec);
		UpdateDelta(-dec);
		if(zeroDial.index==0)
		{
			yield break;
			isReseting = false;
		}
		float dir = (zeroDial.index<0)?1:-1;
		if(dialPivotCanvasGroupTweener.isPaused)
		{
			isReversingVisibility = false;
			dialPivotCanvasGroupTweener.PlayForward();
		}
		while(zeroDial.index!=0)
		{
			visibilityTimer = Time.time +0.1f;
			yield return new WaitForSeconds(0.03f);
			UpdateDelta(dir);
		}
		isReseting = false;
	}
}
