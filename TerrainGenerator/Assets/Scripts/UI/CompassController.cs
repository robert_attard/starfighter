﻿using UnityEngine;
using System.Collections;

public class CompassController : MonoBehaviour {

	public VehicleController playerVehicle;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.localRotation = Quaternion.Euler(0,0,playerVehicle.yawPivot.localRotation.eulerAngles.y);
	}
}
