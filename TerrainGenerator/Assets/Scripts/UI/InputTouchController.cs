using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputTouchController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
	
	public Image cursor;
	
	private Vector3 startPosition;
	public Vector3 cursorStartPosition;
	private Vector3 dir = Vector3.zero;
	private RectTransform rTransform;
	private Vector3 currentPosition;
	public float smoothStep = 0.2f;
	private Vector3 vel;
	private Vector3 targetPos;
	public float scaleOffset =1;
	private Vector3 previousDirection;
	public Canvas canvas;
	private bool isDragging=false;
	private int currentPointerId=int.MaxValue;
	public float movementRadius =1;
	private float maxMagnitude;
	public Vector2 output;

	private void Awake()
	{
		rTransform = cursor.GetComponent<RectTransform>();
		cursorStartPosition = rTransform.anchoredPosition;
	}

	public void OnPointerDown (PointerEventData eventData)
	{
		if(isDragging)
		{
			return;
		}
		
		isDragging = true;
		currentPointerId = eventData.pointerId;
		startPosition = eventData.position;
		currentPosition = eventData.position;
		targetPos = eventData.position;
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		if(eventData.pointerId ==currentPointerId)
		{
			dir = Vector2.zero;
			currentPointerId =int.MaxValue;
			isDragging = false;
		}
	}

	public void OnDrag (PointerEventData eventData)
	{
		if(eventData.pointerId ==currentPointerId)
		{

			dir = (Vector3)eventData.position - startPosition;
		}
	}

	public Vector3 cursorScreenPos;

	private void Update()
	{
		RectTransform canvasRecTransform = canvas.GetComponent<RectTransform>();
		float cScaledHeight = canvasRecTransform.rect.height;// / canvasRecTransform.localScale.y;
		
		currentPosition = Vector3.SmoothDamp(currentPosition,startPosition + dir,ref vel,smoothStep);
		Vector3 canvasScale = canvas.GetComponent<RectTransform>().localScale * scaleOffset;

		Vector3 convertedDir = new Vector3(dir.x/canvasScale.x,dir.y/canvasScale.y,0);
		maxMagnitude = (cScaledHeight/2f)* movementRadius;
		convertedDir = Vector3.ClampMagnitude(convertedDir,maxMagnitude);
		rTransform.anchoredPosition = cursorStartPosition + convertedDir;
		cursorScreenPos = rTransform.position;


		output.x = (rTransform.anchoredPosition.x - cursorStartPosition.x)/maxMagnitude;
		output.y = (rTransform.anchoredPosition.y - cursorStartPosition.y)/maxMagnitude;

	}
}
