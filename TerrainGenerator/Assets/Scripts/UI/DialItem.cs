﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasGroup))]


public class DialItem : MonoBehaviour {

	[HideInInspector]
	public Text textItem;
	[HideInInspector]
	public RectTransform rectTransform;


	private CanvasGroup canvasGroup;
	public Text minusText;

	void Awake()
	{
		textItem = GetComponent<Text>();
		rectTransform = GetComponent<RectTransform>();
		canvasGroup = GetComponent<CanvasGroup>();
	}

	private ScrollableThrottleUIController sController;


	private float dialBoxHeight;
	public float index;
	private int halfSpan;
	private float preferredWidth;
	public int dialValue;


	public void SetStartIndex(float index, int dialValue,ScrollableThrottleUIController sController, float dialBoxHeight, int halfSpan)
	{
		this.sController = sController;
		this.dialBoxHeight =dialBoxHeight;
		this.halfSpan = halfSpan;
		this.index = (float)index;
		ScrollableThrottleUIController.UpdateDelta += HandleUpdateDelta;
		this.dialValue =dialValue;
		textItem.text = Mathf.Abs(dialValue*ScrollableThrottleUIController.dialValueMultiplier).ToString();

		UpdateUI();
		if(index>=0)
		{
			minusText.enabled=false;
			return;
		}

		TextGenerationSettings t = new TextGenerationSettings();
		t.fontSize = textItem.fontSize;
		t.font = textItem.font;
		t.fontStyle = textItem.fontStyle;
		t.horizontalOverflow = textItem.horizontalOverflow;
		t.lineSpacing = textItem.lineSpacing;
		t.verticalOverflow = textItem.verticalOverflow;
		t.textAnchor = TextAnchor.MiddleCenter;
		preferredWidth = textItem.cachedTextGenerator.GetPreferredWidth(textItem.text,t);
		minusText.rectTransform.anchoredPosition = new Vector2((-preferredWidth/2f) - minusText.rectTransform.rect.width,0);
	}

	private void OnDestroy()
	{
		ScrollableThrottleUIController.UpdateDelta -= HandleUpdateDelta;
	}

	private void UpdateUI()
	{
		float clampedIndex = Mathf.Clamp(index,-(sController.halfSpan+1),sController.halfSpan-1);
		float percY = (float)Mathf.Abs(Mathf.Round(clampedIndex))/(float)(halfSpan-1);

		float y = Mathf.Sign(clampedIndex) * (percY) * dialBoxHeight/2f;;
		rectTransform.anchoredPosition = new Vector2(0,y * sController.dialDistanceCurve.Evaluate(percY));
		transform.localScale = Vector3.one * sController.dialScaleCurve.Evaluate(percY);

		textItem.color = (y==0)?Color.white:Color.black;
		minusText.color = (y==0)?Color.white:Color.black;
		
		canvasGroup.alpha = (1f-sController.dialAlphaCurve.Evaluate(percY));
		rectTransform.SetParent(((rectTransform.anchoredPosition.y ==0)?sController.dialBoxRectTransform.transform:sController.dialPivotRectTransform.transform));

		if(rectTransform.anchoredPosition.y ==0)
		{
			sController.OnThrottleUpdate((dialValue*ScrollableThrottleUIController.dialValueMultiplier)/100f);
		}
	}

	void HandleUpdateDelta (float delta)
	{
		index+=delta;
		gameObject.SetActive((Mathf.Abs(index)<sController.halfSpan));
		UpdateUI();
	}
}
