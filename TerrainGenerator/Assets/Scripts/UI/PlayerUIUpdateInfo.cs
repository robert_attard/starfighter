﻿using UnityEngine;
using System.Collections;

public class PlayerUIUpdateInfo {

	public Transform playerForwardTransform;

	private float healthPercentage;
	public float HealthPercentage
	{
		get{
			return healthPercentage;
		}
		set{
			healthPercentage = Mathf.Clamp(value,0,1f);
		}
	}

	public Transform inflictor;
}
