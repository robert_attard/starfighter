﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputScroller : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	public Image cursor;
	
	private Vector3 startPosition;
	public Vector3 cursorStartPosition;
	private Vector3 dir = Vector3.zero;
	private RectTransform rTransform;
	private Vector3 currentPosition;
	public float smoothStep = 0.2f;
	private Vector3 vel;
	private Vector3 targetPos;
	public float scaleOffset =1;
	private Vector3 previousDirection;
	public Canvas canvas;
	private bool isDragging=false;
	private int currentPointerId=int.MaxValue;
	public float movementRadius =1;
	private float maxMagnitude;
	public Vector2 output;

	void HandleGUIView ()
	{
		GUILayout.Label("drag Dir " + dir + "   magnitude  " + dir.magnitude);
	}

	public GunController gunController;

	public Image speedometer;

	public delegate void OnPointerChange();
	public OnPointerChange PointerUp;
	private RectTransform canvasRecTransform;

	private void OnPointerUpHandler()
	{
		if(PointerUp!=null)
		{
			PointerUp();
		}
	}

	public OnPointerChange PointerDown;
	
	private void OnPointerDownHandler()
	{
		if(PointerDown!=null)
		{
			PointerDown();
		}
	}

	public delegate void OnDragAxisHandler(float swipeAxis);
	public static OnDragAxisHandler DragVerticalDelta;

	private void OnDragVerticalDelta(float v)
	{
		if(DragVerticalDelta!=null)
		{
			DragVerticalDelta(v);
		}
	}

	public OnDragAxisHandler DragHorizontal;

	private void OnDragHorizontal(float v)
	{
		if(DragHorizontal!=null)
		{
			DragHorizontal(v);
		}
	}

	private void Awake()
	{
		GUIView.gui += HandleGUIView;
		rTransform = cursor.GetComponent<RectTransform>();
		canvasRecTransform = canvas.GetComponent<RectTransform>();
		cursorStartPosition = rTransform.anchoredPosition;
		pointerDownCounter = float.MaxValue;
	}

	float pointerDownCounter;
	public float pointerDownThreshold = 0.1f;
	
	public void OnPointerDown (PointerEventData eventData)
	{
		pointerDownCounter = Time.time + pointerDownThreshold;
		
		if(isDragging)
		{
			return;
		}
		isDragging = true;
		currentPointerId = eventData.pointerId;
		startPosition = eventData.position;
		currentPosition = eventData.position;
		targetPos = eventData.position;
		previousY = ((Vector3)eventData.position - startPosition).y/ canvasRecTransform.rect.height;
	}
	
	public void OnPointerUp (PointerEventData eventData)
	{
		if(eventData.pointerId ==currentPointerId)
		{
			dir = Vector2.zero;
			currentPointerId =int.MaxValue;
			isDragging = false;
			OnPointerUpHandler();
			pointerDownCounter = float.MaxValue;
			gunController.ToggleFire(false);
		}
	}

	float previousY;
	public void OnDrag (PointerEventData eventData)
	{

		if(eventData.pointerId ==currentPointerId)
		{
			dir = (Vector3)eventData.position - startPosition;

			if(dir.magnitude>30f && pointerDownCounter>Time.time)
			{
				pointerDownCounter = float.MaxValue;
			}
			float delta = dir.y/ canvasRecTransform.rect.height;
			OnDragVerticalDelta(delta-previousY);
			previousY = delta;
		}
	}
	
	private void Update()
	{
		if(Time.time>pointerDownCounter)
		{
			gunController.ToggleFire(true);
		}

		float cScaledHeight = canvasRecTransform.rect.height;// / canvasRecTransform.localScale.y;
		
		currentPosition = Vector3.SmoothDamp(currentPosition,startPosition + dir,ref vel,smoothStep);
		Vector3 canvasScale = canvas.GetComponent<RectTransform>().localScale * scaleOffset;
		
		Vector3 convertedDir = new Vector3(dir.x/canvasScale.x,dir.y/canvasScale.y,0);
		maxMagnitude = (cScaledHeight/2f)* movementRadius;
		convertedDir = Vector3.ClampMagnitude(convertedDir,maxMagnitude);
		rTransform.anchoredPosition=  cursorStartPosition + convertedDir;
		
		output.x = (rTransform.anchoredPosition.x - cursorStartPosition.x)/maxMagnitude;
		output.y = (rTransform.anchoredPosition.y - cursorStartPosition.y)/maxMagnitude;

	}
}
