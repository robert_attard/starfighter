﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Holoville.HOTween;

[RequireComponent(typeof(Image))]
public class StrafeIndicatorItem : MonoBehaviour {

	private Image image;
	public Image strafeHighlight;
	public float index;

	private float alpha=0;
	public float Alpha
	{
		get{
			return alpha;
		}
		set
		{
			alpha = value;
			float darkAlpha = (1f-alpha) * ((index==0)?1f:0.5f);
			image.color = new Color(1f,1f,1f,darkAlpha);
			strafeHighlight.color = new Color(1f,1f,1f,alpha);
		}
	}

	private Tweener strafeItemTweener;

	public void InitUI(StrafeUIController strafeUIController,float index)
	{
		this.index = index;
		image = GetComponent<Image>();
		Alpha =0;
		strafeUIController.StrafeUpdate += HandleStrafeUpdate;
		strafeItemTweener = HOTween.To(this,0.1f,new TweenParms().Prop("Alpha",1f,false).AutoKill(false).Ease(EaseType.EaseInOutQuad));
		strafeItemTweener.Pause();
	}

	private void OnDestroy()
	{
		//TODO Remove StrafeUpdate Handler ... check for StrafeUIController instance
	}

	public float u;

	void HandleStrafeUpdate (float strafeUIValue)
	{
		u = strafeUIValue;
		if(Mathf.Round(strafeUIValue)==index)
		{
			if(!strafeItemTweener.isComplete)
			{
				strafeItemTweener.PlayForward();
			}
		}
		else if(!strafeItemTweener.isReversed)
		{
			strafeItemTweener.PlayBackwards();
		}
	}
}
