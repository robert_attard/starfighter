﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class DecalUpdateInfo
{
	public float heightOffset = 5f;
	public Transform decalEntity;
	public abstract DecalType decalType{get;}
}


[RequireComponent(typeof(RectTransform))]
public abstract class DecalBase : MonoBehaviour {

	public int poolSize = 20;
	public DecalType decalType;
	private RectTransform rectTransform;
	public DecalUpdateInfo info;

	private void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
	}

	public virtual void UpdateDecal(DecalUpdateInfo info)
	{
		this.info = info;
		rectTransform.transform.position = Camera.main.WorldToScreenPoint(info.decalEntity.position + Vector3.up * info.heightOffset);
	}
}
