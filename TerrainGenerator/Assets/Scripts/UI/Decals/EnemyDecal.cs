﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class EnemyDecalInfo : DecalUpdateInfo
{
	public override DecalType decalType {
		get {
			return DecalType.Enemy;
		}
	}

	public float healthPercentage;
}

public class EnemyDecal : DecalBase {

	public UIBarMask healthBar;
	public override void UpdateDecal (DecalUpdateInfo info)
	{
		base.UpdateDecal(info);
		EnemyDecalInfo enemyDecalInfo = (EnemyDecalInfo)info;
		healthBar.SetBarValue(enemyDecalInfo.healthPercentage);
	}
}
