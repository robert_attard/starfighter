﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum DecalType
{
	Enemy,
	Friendly,
	Objective
}

public class DecalManager : MonoBehaviour {

	public DecalBase[] decals;
	private Dictionary<Transform,DecalBase> activeDecalDictionary = new Dictionary<Transform, DecalBase>();
	private Dictionary<DecalType,List<DecalBase>> inactiveDecalDictionary = new Dictionary<DecalType, List<DecalBase>>();
	private  Transform playerForwardTransform;

	// Use this for initialization
	void Awake () {
		NotificationCenter.AddListener(HandleUIUpdate,NotificationType.UIUpdate);
		playerForwardTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<VehicleController>().pitchPivot;
	}

	void Start()
	{
		CreateDecalPools();
	}

	void OnDestroy()
	{
		NotificationCenter.AddListener(HandleUIUpdate,NotificationType.UIUpdate);
	}

	void HandleUIUpdate (Notification note)
	{
		UIUpdateInfo updateInfo = (UIUpdateInfo)note.data;
		if(updateInfo.uiUpdateType == UIUpdateType.DecalUpdate)
		{
			DecalUpdateInfo decalInfo = (DecalUpdateInfo)updateInfo.data;
			if(!CheckVisibility(decalInfo))
			{
				return;
			}
			if(!activeDecalDictionary.ContainsKey(decalInfo.decalEntity))
			{
				activeDecalDictionary.Add(decalInfo.decalEntity, ActivateDecal(decalInfo));
			}
			else
			{
				activeDecalDictionary[decalInfo.decalEntity].UpdateDecal(decalInfo);
			}
		}
	}

	List<DecalBase> decalsForRemoval = new List<DecalBase>();
	private void Update()
	{
		decalsForRemoval.Clear();
		foreach(KeyValuePair<Transform,DecalBase> d in activeDecalDictionary)
		{
			if(d.Value.info.decalEntity==null)
			{
				decalsForRemoval.Add(d.Value);
			}
			else if(!CheckVisibility(d.Value.info))
			{
				decalsForRemoval.Add(d.Value);
			}
		}
		foreach(DecalBase d in decalsForRemoval)
		{
			activeDecalDictionary.Remove(d.info.decalEntity);
			inactiveDecalDictionary[d.decalType].Add(d);
			d.gameObject.SetActive(false);
		}
	}

	private bool CheckVisibility(DecalUpdateInfo info)
	{
		Vector2 viewportPos = Camera.main.WorldToViewportPoint(info.decalEntity.position);
		bool isInViewPort = (viewportPos.x>0 && viewportPos.x<1f && viewportPos.y>0 && viewportPos.y<1f);
		bool isInfrontOfPlayer = Vector3.Dot(playerForwardTransform.forward, (info.decalEntity.position - playerForwardTransform.position).normalized)>=0;
		return isInViewPort && isInfrontOfPlayer;
	}

	private DecalBase ActivateDecal( DecalUpdateInfo decalUpdateInfo)
	{
		DecalBase newDecal = inactiveDecalDictionary[decalUpdateInfo.decalType][0];
		inactiveDecalDictionary[decalUpdateInfo.decalType].RemoveAt(0);
		newDecal.UpdateDecal(decalUpdateInfo);
		newDecal.gameObject.SetActive(true);
		return newDecal;
	}

	private void CreateDecalPools()
	{
		foreach(DecalBase decal in decals)
		{
			List<DecalBase> decalList = new List<DecalBase>();
			decalList.Add(decal);
			inactiveDecalDictionary.Add(decal.decalType,decalList);
			for(int i=1;i<decal.poolSize;i++)
			{
				DecalBase newDecal = Instantiate<DecalBase>(decal);
				RectTransform decalRectTransform = newDecal.GetComponent<RectTransform>();
				decalRectTransform.SetParent(transform);
				decalRectTransform.anchoredPosition = Vector2.zero;
				inactiveDecalDictionary[decal.decalType].Add(newDecal);
				newDecal.transform.localScale = decal.transform.localScale;
			}
			foreach(DecalBase d in decalList)
			{
				d.gameObject.SetActive(false);
			}
		}
	}
}
