﻿using UnityEngine;
using System.Collections;


public enum UIUpdateType
{
	DecalUpdate,
	PlayerUIUpdate
}

public class UIUpdateInfo {

	public UIUpdateType uiUpdateType;
	public object data;
}


