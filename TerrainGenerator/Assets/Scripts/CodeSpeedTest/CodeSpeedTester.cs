﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using System.Text;


public class StopWatchInfo
{
	public string id;

	public StopWatchInfo(string id)
	{
		this.id = id;
	}

	public Stopwatch stopWatch= new Stopwatch();
	private List<TimeSpan> loggedTimings = new List<TimeSpan>();
	
	public void LogElapsedTime()
	{
		loggedTimings.Add(stopWatch.Elapsed);
		stopWatch.Reset();
	}

	public string GetLogResult()
	{
		StringBuilder sb = new StringBuilder();
		sb.AppendLine(string.Empty);
		sb.AppendLine(string.Format("Timing for {0}",id));
		sb.AppendLine(string.Format("\tTimings {0}",loggedTimings.Count));
		long sumTicks =0;
		
		foreach(TimeSpan t in loggedTimings)
		{
			sumTicks+=t.Ticks;

		}
		sb.AppendLine(string.Format("\tAverage time (ticks) {0}",Mean(loggedTimings).Ticks));
		sb.AppendLine(string.Format("\tAverage time (milliseconds) {0}",Mean(loggedTimings).Milliseconds));
		
		sb.AppendLine(string.Empty);
		sb.AppendLine(string.Format("\tStandard Deviation (ticks) {0}",StandardDeviation(loggedTimings).Ticks));
		sb.AppendLine(string.Format("\tStandard Deviation (milliseconds) {0}",StandardDeviation(loggedTimings).Milliseconds));
		sb.AppendLine("----------------------------");


		return sb.ToString();
	}

	public TimeSpan Mean(List<TimeSpan> values)
	{
		long s = 0;
		
		for (int i = 0; i < values.Count; i++)
		{
			s += values[i].Ticks;
		}
		return new TimeSpan( (values.Count==0)?0:s/values.Count);
	}

	public TimeSpan Variance(List<TimeSpan> values)
	{
		long variance = 0;
		long mean = Mean (values).Ticks;
		
		for (int i = 0; i < values.Count; i++)
		{
			variance += (long)Math.Pow((values[i].Ticks - mean), 2);
		}
		
		return new TimeSpan ( variance / values.Count);
	}

	private TimeSpan StandardDeviation(List<TimeSpan> values)
	{
		return  new TimeSpan((long)Math.Sqrt(Variance(values).Ticks));
	}
}

public class CodeSpeedTester {

	private static CodeSpeedTester instance;
	private static CodeSpeedTester Instance
	{
		get{
			#if UNITY_EDITOR
			if(instance==null)
			{
				instance = new  CodeSpeedTester();
				CodeSpeedTestEditor.PlayModeChanged += instance.HandlePlayModeChanged;
			}
			#endif
			return instance;
		}
	}

	public Dictionary<string,StopWatchInfo> stopWatchDictionary = new Dictionary<string,StopWatchInfo>();

	#if UNITY_EDITOR
	private void HandlePlayModeChanged (PlayModeState arg1, PlayModeState arg2)
	{
		if(arg1 == PlayModeState.Playing && arg2 == PlayModeState.Stopped)
		{
			instance.ShowTimings();
		}
	}
	#endif

	public static void LogTimeStart(string timingId)
	{
		#if UNITY_EDITOR
		if(!Instance.stopWatchDictionary.ContainsKey(timingId))
		{
			StopWatchInfo s = new StopWatchInfo(timingId);
			Instance.stopWatchDictionary.Add(timingId,s);
		}
		instance.stopWatchDictionary[timingId].stopWatch.Start();
		#endif
	}

	public static void LogElapsedTime(string timingId)
	{
		#if UNITY_EDITOR
		StopWatchInfo s = Instance.stopWatchDictionary[timingId];
		s.LogElapsedTime();
		#endif
	}

	private void ShowTimings()
	{
		#if UNITY_EDITOR
		StringBuilder sb = new StringBuilder();
		sb.AppendLine("Timings");
		sb.AppendLine("______________________");
		foreach(KeyValuePair<string,StopWatchInfo> k in stopWatchDictionary)
		{
			sb.Append(k.Value.GetLogResult());
		}
		UnityEngine.Debug.Log(sb.ToString());
		#endif
	}


}
