﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class RectFollower : MonoBehaviour {

	public RectTransform rTransform;
	private RectTransform myRectTransform;

	private void Awake()
	{
		myRectTransform = GetComponent<RectTransform>();
	}

	void Update()
	{
		myRectTransform.position = rTransform.position;
	}

}
