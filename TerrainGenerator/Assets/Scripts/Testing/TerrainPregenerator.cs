﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using ThreadPooling;

public struct TerrainPointInfo
{
	public float height;
	public Color32 color;
}

public class TerrainPregenerationWorker
{
	private Vector3 position;
	public MeshPatchTemplate2 mPatchTemplate;
	public TerrainPregenerationWorker(Vector3 position,int vertexColumns,float CellWidth,MapBitMap mBitMap,int meshColliderDivisor)
	{
		mPatchTemplate = new MeshPatchTemplate2(mBitMap);
		this.position = position;
		ThreadPool.QueueUserWorkItem(ThreadWork,mPatchTemplate);
	}

	private void ThreadWork(object t)
	{
		MeshPatchTemplate2 m = (MeshPatchTemplate2)t;
		m.UpdateMeshProperties(position);
		TerrainPregenerator.finishedJobs++;
	}
}

[RequireComponent(typeof(MapBitMap))]
public class TerrainPregenerator : MonoBehaviour {
	public static float finishedJobs;
	public int vertexColumns = 10;
	public float patchWidth = 100f;
	private float CellWidth;
	private MapBitMap mBitMap;

	public Image progressBar;
	public Text progressText;
	
	private TerrainPointInfo[] pointInfos;
	public static bool mPatchCacheListLocked = false;

	private bool IsEvenNumber(float number)
	{
		return (number % 2 )==0;
	}

	void OnValidate()
	{
	}

	public float estimatedPatchCount;
	public static int max;
	public int maximum;
	public int totalPoints;

	private void Awake()
	{
		ThreadPool.InitInstance(500,4);
		mBitMap = GetComponent<MapBitMap>();

		int patchCount = (int)(mBitMap.worldLength/patchWidth);
		totalPoints = (int)Mathf.Pow( vertexColumns * (patchCount-1),2f);
		pointInfos = new TerrainPointInfo[totalPoints];
		print (pointInfos.Length);
		CellWidth = patchWidth/(float)(vertexColumns-1);
		mBitMap.Init(CellWidth);
		estimatedPatchCount =Mathf.Pow( (mBitMap.worldLength/patchWidth),2f);
		MeshPatchTemplate2.Init(vertexColumns,CellWidth);
	}

	private void Update()
	{
		maximum= max;
		if(Input.GetKeyDown(KeyCode.T))
		{
			Debug.Log("Updating");
			ThreadPool.QueueUserWorkItem(CreateWorld,this);
			Debug.Log("Finished");
		}
		float percentage = Mathf.Round(finishedJobs/estimatedPatchCount * 100f);
		if(percentage%1==0)
		{
			progressBar.fillAmount = percentage/100f;
			progressText.text = percentage.ToString()+"%";
		}
	}

	public void CreateWorld(object g)
	{
		for(int z=0;z<mBitMap.worldLength;z+=(int)patchWidth)
		{
			for(int x=0;x<mBitMap.worldLength;x+=(int)patchWidth)
			{
				new TerrainPregenerationWorker(new Vector3(x,0,z),vertexColumns,CellWidth,mBitMap,3);
			}
		}
	}
}
