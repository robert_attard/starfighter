﻿using UnityEngine;
using System.Collections;

public class GunController : WeaponBase {
	

	public ParticleSystem gunLeft;
	public ParticleSystem gunRight;
	public InputTouchController inputTouchController;
	public Camera cam;

	private void Awake()
	{
		ToggleFire(false);
	}

	bool previousToggle = false;
	public void ToggleFire(bool isEnabled)
	{
		Ray ray = cam.ScreenPointToRay(inputTouchController.cursorScreenPos);
		gunLeft.transform.rotation = Quaternion.LookRotation(ray.direction);
		gunRight.transform.rotation = Quaternion.LookRotation(ray.direction);

		gunLeft.enableEmission = isEnabled;
		gunRight.enableEmission = isEnabled;
//		previousToggle = isEnabled;
	}

	#if UNITY_EDITOR
	void Update()
	{
		ToggleFire(Input.GetKey(KeyCode.L));
	}
	#endif
}
