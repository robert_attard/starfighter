﻿using UnityEngine;
using System.Collections;

public class CrystalGenerator : MonoBehaviour {
	public GameObject crystalPrefab;

	public float radius =100f;
	public float crystalCenterRadius =30f;
	
	public float eclipseXStretch =2f;
	public int crystalCount =100;

	public float crystalMaxHeight = 100f;
	public AnimationCurve positionDistributionCurve;
	public float crystalHeight;
	private System.Random rand;
	public float MaxAngle = 20f;
	public float meanDistanceFromCenter =10f;
	public float stdDev = 1f;

	// Use this for initialization
	void Start () {
		rand = new System.Random();
		crystalHeight = crystalPrefab.GetComponent<MeshFilter>().sharedMesh.bounds.size.y;

		GameObject[] crystals = new GameObject[crystalCount];
		Vector2 cPos = Random.insideUnitCircle * crystalCenterRadius;
		Vector3 center = new Vector3(cPos.x,0,cPos.y) +transform.position;
		
		for(int i=1;i<crystalCount;i++)
		{
			Vector3 pos =  center + ( Quaternion.Euler(0,Random.Range(0,360f),0) * (Vector3.forward * GenerateGuassianRandomNumber(meanDistanceFromCenter,stdDev)));
			crystals[i] = Instantiate(crystalPrefab,pos,Quaternion.identity) as GameObject;
			crystals[i].transform.parent = transform;
			crystals[i].transform.Rotate(0,Random.Range(0,360f),0);
		}

		for(int i=1;i<crystalCount;i++)
		{
			float d = Vector3.Distance(center,crystals[i].transform.position);
			float frac = d/radius;
			float a = positionDistributionCurve.Evaluate(frac);
			SetCrystalHeight(crystals[i],a*crystalMaxHeight * Random.Range(0.8f,1.2f));

			Vector3 dir = center - crystals[i].transform.position;
			crystals[i].transform.rotation = Quaternion.RotateTowards(Quaternion.identity,Quaternion.FromToRotation(Vector3.up,dir),-( (d/meanDistanceFromCenter)*MaxAngle));
		}
	}

	private void SetCrystalHeight(GameObject g, float height)
	{
		if(height>crystalHeight)
		{
			g.transform.localScale = Vector3.one * (height/crystalHeight);
		}else
		{
			g.transform.position-= new Vector3(0, Mathf.Max(height-crystalMaxHeight,+crystalHeight/2f),0);
		}
	}

	float GenerateGuassianRandomNumber(float mean, float stdDev)
	{
		double u1 = rand.NextDouble(); //these are uniform(0,1) random doubles
		double u2 = rand.NextDouble();
		float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log((float)u1)) * Mathf.Sin(2.0f * Mathf.PI * (float)u2); //random normal(0,1)
		return mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
	}

}
