﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//[System.Serializable]
public class PatchVertex :System.ICloneable
{
	public float x;
	public float y;
	public float z;

	public PatchVertex(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public PatchVertex(Vector3 vertex)
	{
		this.x = vertex.x;
		this.y = vertex.y;
		this.z = vertex.z;
	}

	public void Set(float x, float y, float z)
	{
		this.x = x;
		this.y = x;
		this.z = z;

	}

	public Vector3 Vector
	{
		get{
			return new Vector3(x,y,z);
		}
	}

	public override string ToString ()
	{
		return string.Format ("[PatchVertex: Vector={0}]", Vector);
	} 

	public int neighbourCount=0;
	public PatchVertex[] neighbours = new PatchVertex[6];

	public Color color;

	#region ICloneable implementation

	public object Clone ()
	{
		return new PatchVertex(this.x,this.y,this.z);
	}

	#endregion

	public void AddAmbientOcclusion()
	{
		float c = 1f - CalculateOcclusion ();
		color = new Color (c, c, c) * color;
	}

	private float dotsum;
	private Vector3 averageNormal = Vector3.zero;
	private Vector3 sum = Vector3.zero;

	public float CalculateOcclusion()
	{
		dotsum =0;
		sum = Vector3.zero;
		
		for(int i=0;i<neighbourCount-1;i+=2)
		{
			sum += Vector3.Cross(neighbours[i+1].Vector - Vector,neighbours[i].Vector - Vector);
		}
		
		averageNormal = (sum/neighbourCount).normalized;

		for(int i=0;i<neighbourCount;i++)
		{
			Vector3 cv = neighbours[i].Vector;
			Vector3 nv = Vector + averageNormal;
			float d = Vector3.Dot((cv-Vector).normalized,(nv-Vector).normalized);
			dotsum+=d;
		}
		return  Mathf.Max(0,dotsum/neighbourCount) *1f;
	}
}

//[System.Serializable]
public class MeshPatchTemplate {

	public static int triangleCount;
	public static int vertexCount;
	public static float cellWidth;
	public static float patchWidth;
	public static float maxQuadrantDistance;

	public int vertexColumns=0;
	public PatchVertex[] pVertices;


	public int[] triangleIndices;
	public Vector3[] hVertices;
	public Vector2[] uvs;
	public Color[] colors;
	public Vector3[] normals;
	public MapBitMap mBitMap;
	public string patchName;

	public int meshColliderDivisor;
	public Vector3[] meshColliderMeshVertices;
	public int[] meshColliderTriangleIndices;
	public PatchVertex[] pMeshColliderVertices;
	private int meshColliderVertexCount;
	private int mcVertexColumns;

	private bool isThreadLocked=false;
	public bool IsThreadLocked
	{
		get{
			return isThreadLocked;
		}
	}

	public MeshPatchTemplate(int vColumns, float cwidth,MapBitMap mBitMap,int meshColliderDivisor)
	{
		this.meshColliderDivisor = meshColliderDivisor;
		this.mBitMap = mBitMap;
		vertexColumns = vColumns;
		cellWidth = cwidth;
		patchWidth = cellWidth * (vertexColumns-1);
		triangleCount =  (int)Mathf.Pow(vertexColumns-1,2)*2;
		vertexCount = (int)Mathf.Pow(vertexColumns,2);
		maxQuadrantDistance = Mathf.Sqrt(2* Mathf.Pow(patchWidth,2f));
		hVertices = new Vector3[triangleCount * 3];
		pVertices = new PatchVertex[vertexCount];
		uvs = new Vector2[vertexCount];
		colors = new Color[hVertices.Length];
		normals = new Vector3[hVertices.Length];
		triangleIndices = new int[hVertices.Length];

		InitPoints();
		BuildTriangles();

//		InitMeshColliderPoints();
//		BuildMeshColliderTriangles();

	}

	private void InitPoints()
	{
		//Set Up base positions;
		for(int i=0;i<vertexCount;i++)
		{
			float x = ((i%vertexColumns) * cellWidth);
			float z = ((Mathf.Floor(i/vertexColumns)) * cellWidth);
			PatchVertex  pv = new PatchVertex(x,0,z);
			pVertices[i] = pv;
		}

		//setNeighbours
		int[] neighbourIndexOffsets = new int[]{-vertexColumns-1, -vertexColumns,+1,vertexColumns+1,vertexColumns,-1};
		for(int i=0;i<vertexCount;i++)
		{
			int childCount =0;
			for(int p=0;p<neighbourIndexOffsets.Length;p++)
			{
				int targetIndex = GetNeighbour(i,neighbourIndexOffsets[p]);
				if(targetIndex!=-1)
				{
					pVertices[i].neighbours[childCount] = pVertices[targetIndex];
					childCount++;
				}
				pVertices[i].neighbourCount = childCount;
			}
			if(i>(vertexColumns-1)*vertexColumns)
			{
				SetLastNeighbourToFirst(pVertices[i]);
			}
		}
	}

	private void BuildTriangles()
	{
		for(int i=0;i<triangleIndices.Length;i+=6)
		{
			triangleIndices[i] =i;
			triangleIndices[i+1] =i+1;
			triangleIndices[i+2] =i+2;
			
			triangleIndices[i+3] =i+3;
			triangleIndices[i+4] =i+4;
			triangleIndices[i+5] =i+5;
		}
	}


	private void InitMeshColliderPoints()
	{
		//MeshCollider Data
		mcVertexColumns = ((vertexColumns-1)/meshColliderDivisor)+1;
		meshColliderVertexCount = (int)Mathf.Pow(mcVertexColumns,2);
		meshColliderMeshVertices = new Vector3[meshColliderVertexCount];
		pMeshColliderVertices = new PatchVertex[meshColliderVertexCount];

		int vIndex=0;
		int increment = meshColliderDivisor;
		for(int i=0;i<vertexCount;i+=increment)
		{
			increment = meshColliderDivisor;
			pMeshColliderVertices[vIndex] = pVertices[i];
//			Debug.Log (string.Format ("{0}  Vector:  {1}", vIndex, pMeshColliderVertices [vIndex].Vector));
			vIndex++;
			if((i+1)%vertexColumns==0)
			{
				increment = (vertexColumns*(increment-1))+1;
			}
		}
	}

	private void BuildMeshColliderTriangles()
	{
		int triangleIndexCount = 0;
		int numberOfTriangles = (int)Mathf.Pow(mcVertexColumns-1,2f) * 2;
		meshColliderTriangleIndices = new int[numberOfTriangles*3];

		int v =0;
		for(int i=0;i<meshColliderTriangleIndices.Length;i+=6)
		{
			meshColliderTriangleIndices[i] =v;
			meshColliderTriangleIndices[i+1] =v+mcVertexColumns;
			meshColliderTriangleIndices[i+2] =v+mcVertexColumns+1;


			meshColliderTriangleIndices[i+3] =v;
			meshColliderTriangleIndices[i+4] =v+mcVertexColumns+1;
			meshColliderTriangleIndices[i+5] =v+1;
			
			v++;
			
			if((v+1)%(mcVertexColumns)==0)
			{
				v++;
			}
		}
	}

	private void SetLastNeighbourToFirst(PatchVertex vertex)
	{
		PatchVertex[] n = new PatchVertex[6];
		PatchVertex lastN = vertex.neighbours[vertex.neighbourCount-1];
		for(int i=0;i<vertex.neighbourCount-1;i++)
		{
			n[i+1] = vertex.neighbours[i];
		}
		n[0]=lastN;
		vertex.neighbours = n;
	}

	private int GetNeighbour(int startIndex, int offset)
	{
		if((startIndex + offset)<0 || (startIndex + offset) >= vertexCount)
		{
			return -1;
		}

		int mod = startIndex%vertexColumns;

		if(mod == 0)
		{
			if(offset == -1  || offset == -vertexColumns-1 || offset == vertexColumns-1)
			{
				return -1;
			}
		}

		if(mod == (vertexColumns-1))
		{
			if(offset == -vertexColumns+1  || offset == 1 || offset == vertexColumns+1)
			{
				return -1;
			}
		}
		return startIndex + offset;
	}
	


	private class TerrainQuadrant
	{
		public TerrainCharacteristic tc;
		public float distance;
		public static float patchWidth;
		public static MapBitMap mBitMap;
		public static float squaredRadius;
		public float _x;
		public float _y;
		public VTColorInfo vtInfo;
		private float maxDistance;

		public TerrainQuadrant(float x, float y)
		{
			this._x = x + (patchWidth/2f);
			this._y = y + (patchWidth/2f);
			tc = mBitMap.GetHeightMapInfo(x,y);
			maxDistance = patchWidth*2f;
		}

		public bool ContainsPoint(Vector2 point)
		{
			return Mathf.Pow(point.x - _x,2) + Mathf.Pow(point.y - _y,2f) < squaredRadius;
		}
	}

	private static float SDistance(Vector2 a,Vector2 b)
	{
		return maxQuadrantDistance - Vector3.Distance(a,b);
	}

	public void UpdateMeshProperties(Vector3 offset)
	{
		isThreadLocked = true;
		TerrainQuadrant.patchWidth = patchWidth;
		TerrainQuadrant.mBitMap = mBitMap;
		TerrainQuadrant.squaredRadius = Mathf.Pow(patchWidth,2f);
		TerrainQuadrant[] quadrants = new TerrainQuadrant[9];

		bool isSimilar= false;

		quadrants[0] = new TerrainQuadrant(offset.x-patchWidth ,offset.z-patchWidth);
		quadrants[1] = new TerrainQuadrant(offset.x ,offset.z-patchWidth);
		quadrants[2] = new TerrainQuadrant(offset.x+patchWidth ,offset.z-patchWidth);
		quadrants[3] = new TerrainQuadrant(offset.x-patchWidth,offset.z);
		quadrants[4] = new TerrainQuadrant(offset.x ,offset.z);
		quadrants[5] = new TerrainQuadrant(offset.x+patchWidth,offset.z);
		quadrants[6] = new TerrainQuadrant(offset.x-patchWidth,offset.z+patchWidth);
		quadrants[7] = new TerrainQuadrant(offset.x,offset.z+patchWidth);
		quadrants[8] = new TerrainQuadrant(offset.x+patchWidth,offset.z+patchWidth);

		if(quadrants[0].tc == quadrants[1].tc)
			if(quadrants[1].tc == quadrants[2].tc)
				if(quadrants[2].tc == quadrants[3].tc)
					if(quadrants[3].tc == quadrants[4].tc)
						if(quadrants[4].tc == quadrants[5].tc)
							if(quadrants[5].tc == quadrants[6].tc)
								if(quadrants[6].tc == quadrants[7].tc)
									if(quadrants[7].tc == quadrants[8].tc)
									{
										isSimilar = true;
									}



		float totalDistance =0;
		if(!isSimilar)
		{
			for(int i=0;i<pVertices.Length;i++)
			{
				List<VTColorInfo> allVtInfoList= new List<VTColorInfo>();
				TerrainQuadrant[] tQuadrantArray = new TerrainQuadrant[System.Enum.GetValues(typeof( TerrainColorMatch)).Length];
				PatchVertex p = pVertices[i];

				for(int q=0;q<quadrants.Length;q++)
				{
					Vector2 ptPos = new Vector2(offset.x+p.x,offset.z+p.z);
					if(quadrants[q].ContainsPoint(ptPos))
					{
						TerrainQuadrant tq = quadrants[q];

						float fDistance = Mathf.Pow( SDistance(new Vector2(tq._x,tq._y),ptPos),3f);
						allVtInfoList.Add(new VTColorInfo(){ color = tq.tc.color, distanceFromVertex = fDistance,weighting =-1,vertexPosition = ptPos,quadrantCenter = new Vector2(tq._x,tq._y)});
						if(tQuadrantArray[(int)tq.tc.color]==null)
						{
							tQuadrantArray[(int)tq.tc.color] = tq;
							tq.distance =fDistance;
						}
						else
						{
							tQuadrantArray[(int)tq.tc.color].distance += fDistance;
						}
						totalDistance += fDistance;
					}
				}

				totalDistance =0;
				List<TerrainQuadrant> tQuadrants = new List<TerrainQuadrant>();
				foreach(TerrainQuadrant kv in tQuadrantArray)
				{
					if(kv!=null)
					{
						totalDistance +=kv.distance;
						tQuadrants.Add(kv);
					}
				}

				p.y =0;
				if(tQuadrants.Count==1)
				{
					p.y += tQuadrants[0].tc.Generate(WorldWrap( offset.x+p.x), WorldWrap(offset.z + p.z));
					float f = (p.y +15f)/30f;
					p.color = quadrants[4].tc.gradient.Evaluate(f);
				}
				else
				{
					float gX1 = WorldWrap(offset.x+p.x);
					float gZ1 = WorldWrap(offset.z + p.z);
				

					p.y = Mathf.Lerp(tQuadrants[0].tc.Generate(gX1,gZ1 ),tQuadrants[1].tc.Generate(gX1,gZ1),tQuadrants[1].distance/totalDistance);
					float f0 = (p.y)/tQuadrants[0].tc.heightScale;
					float f1 = (p.y)/tQuadrants[1].tc.heightScale;
					p.color = Color.Lerp(tQuadrants[0].tc.gradient.Evaluate(f0),tQuadrants[1].tc.gradient.Evaluate(f1),tQuadrants[1].distance/totalDistance);
				}
			}
		}
		else
		{
			TerrainQuadrant tq = quadrants[4];
			for(int i=0;i<pVertices.Length;i++)
			{
				PatchVertex p = pVertices[i];
				p.y = tq.tc.Generate(WorldWrap( offset.x+p.x), WorldWrap(offset.z + p.z));
				float f = (p.y +15f)/30f;
				p.color = tq.tc.gradient.Evaluate(f);
			}
		}



		UpdateNewMesh();
	}

	private float WorldWrap(float v)
	{
		return v % mBitMap.worldLength;
	}

	private void UpdateNewMesh()
	{
		for(int i=0;i<pVertices.Length;i++)
		{
			pVertices[i].AddAmbientOcclusion();
		}
		
		int v =0;
		for(int i=0;i<triangleIndices.Length;i+=6)
		{
			int v2 = v+vertexColumns+1;
			int v3 = v+vertexColumns;
			int v4 = v+1;
			
			Vector3 vertex1 = pVertices[v].Vector;
			Vector3 vertex2 = pVertices[v2].Vector;
			Vector3 vertex3 = pVertices[v3].Vector;
			Vector3 vertex4 = pVertices[v4].Vector;
			
			hVertices[i] = vertex1;
			hVertices[i+1] =vertex2;
			hVertices[i+2] =vertex4;
			
			hVertices[i+3] =vertex1;
			hVertices[i+4] =vertex3;
			hVertices[i+5] =vertex2;
			
			Vector3 normal = GetNormal(vertex1,vertex2,vertex4);
			normals[i] =normal;
			normals[i+1] =normal;
			normals[i+2] =normal;
			
			normal = GetNormal(vertex1,vertex3,vertex2);
			normals[i+3] =normal;
			normals[i+4] =normal;
			normals[i+5] =normal;
			
			colors[i] = pVertices[v].color;
			colors[i+1] = pVertices[v2].color;
			colors[i+2] = pVertices[v4].color;
			
			colors[i+3] = pVertices[v].color;
			colors[i+4] = pVertices[v3].color;
			colors[i+5] = pVertices[v2].color;
			
			v++;
			
			if((v+1)%(vertexColumns)==0)
			{
				v++;
			}
		}

//		Debug.Assert(meshColliderMeshVertices.Length == pMeshColliderVertices.Length,"meshColliderMeshVertices and pMeshColliderVertices must have equal number of vertices");
//		Debug.Assert(meshColliderTriangleIndices.Length%3 == 0,"meshColliderTriangleIndices length mus be in multiples of 3");
//		for(int i=0;i<pMeshColliderVertices.Length;i++)
//		{
//			meshColliderMeshVertices[i] = pMeshColliderVertices[i].Vector;
//		}

		isThreadLocked = false;
	}

	public static Vector3 GetNormal(Vector3 a, Vector3 b, Vector3 c)
	{
		return Vector3.Cross(b-a,c-a).normalized;
	}
	
	public static Vector3 MakePlanar ( Vector3 v0, Vector3 v2, Vector3 v3 , Vector3 v1 )
	{
		Vector3 normal = GetNormal(v0,v2,v3);
		float d = -(normal.x * v0.x)-(normal.y * v0.y)-(normal.z * v0.z);
		float y = ( - d - (normal.x*v1.x) - (normal.z*v1.z))/ normal.y;
		return new Vector3(v1.x,y,v1.z);
	}
	
}



