﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(VehicleController))]
public class Player : MonoBehaviour {

	[HideInInspector]
	public VehicleController vController;
	public InputScroller StrafeInputController;
	private float throttle;
	public float maxHealth = 200f;
	private float health;
	private PlayerUIUpdateInfo playerUIInfo;
	private UIUpdateInfo uiUpdateInfo;

	void Awake()
	{
		health = maxHealth;
		vController = GetComponent<VehicleController>();
		playerUIInfo = new PlayerUIUpdateInfo();
		playerUIInfo.playerForwardTransform = vController.pitchPivot;
		uiUpdateInfo = new UIUpdateInfo();
		uiUpdateInfo.uiUpdateType = UIUpdateType.PlayerUIUpdate;
		uiUpdateInfo.data = playerUIInfo;
	}

	void Start()
	{
		ScrollableThrottleUIController.Throttle += HandleThrottleUpdate;
		vController.SetThrottle(1f);
	}
	
	void OnDestroy()
	{
		ScrollableThrottleUIController.Throttle -= HandleThrottleUpdate;
	}

	Vector3 prevPos;
	// Update is called once per frame
	void Update () {
		vController.SetThrottle(throttle);
		vController.SetStrafeDir(StrafeInputController.output.x);

	}

	void FixedUpdate()
	{
		if(Input.GetKeyDown(KeyCode.O))
		{
			float f = Vector3.Distance(prevPos,transform.position);
			Debug.Log( f/Time.fixedDeltaTime);
		}
		prevPos =  transform.position;
	}


	
	void HandleThrottleUpdate (float throttle)
	{
		this.throttle = throttle;
	}

	public void ApplyDamage(DamageInfo damageInfo)
	{
		health -=damageInfo.damage;
		playerUIInfo.HealthPercentage = health/maxHealth;
		playerUIInfo.inflictor = damageInfo.inflictor.transform;
		NotificationCenter.Post(NotificationType.UIUpdate,uiUpdateInfo);
		NotificationCenter.Post(NotificationType.CameraShake);
	}
}
