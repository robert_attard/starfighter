using UnityEngine;
using System.Collections;

public enum InputProcessType
{
	KeyboardMouseInput,
	Accelerometer,
	MobileJoystick,
	AI
}

public abstract class InputProcessor : MonoBehaviour {

	public abstract Vector2 ProcessInput();
	public abstract InputProcessType ProcessType { get;}
	public abstract void OnSelected();
	public abstract void OnDeselected();
}
