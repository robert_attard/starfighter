﻿using UnityEngine;
using System.Collections;

public class AccelerometerInputProcessor : InputProcessor {
	public float AccelSmoothing = 0.5f;
	public float AccelLowPass = 0.1f;
	private Vector3 _accelerationPrevious;
	private Vector3 _acceleration;
	private float zOffset = 0;
	private Vector3 _accelerationSmooth;

	#region implemented abstract members of InputProcessor
		public override Vector2 ProcessInput ()
		{
			Vector3 _acc = Input.acceleration;
			_acceleration.x = (_acc.x * AccelLowPass) + (_accelerationPrevious.x * (1.0f - AccelLowPass));
			_acceleration.z = (_acc.z * AccelLowPass) + (_accelerationPrevious.z * (1.0f - AccelLowPass));
			
			_accelerationSmooth = Vector3.Lerp (_acceleration, _accelerationPrevious, AccelSmoothing);
			_accelerationPrevious = _acceleration;
			return new Vector2 (_accelerationSmooth.x + 0.5f, _accelerationSmooth.z + 0.5f - zOffset);
		}

		public void Calibrate ()
		{
			Debug.Log ("Calibrating ");
			zOffset = _accelerationSmooth.z;
		}

		public override void OnSelected ()
		{
		}

		public override void OnDeselected ()
		{
		}

		public override InputProcessType ProcessType {
			get {
				return InputProcessType.Accelerometer;
			}
		}
	#endregion
	
}
