﻿using UnityEngine;
using System.Collections;

public class JoystickInputProcessor : InputProcessor {
	#region implemented abstract members of InputProcessor

	public InputTouchController joystickUI;

	public override Vector2 ProcessInput ()
	{
		return joystickUI.output;
	}

	public override void OnSelected ()
	{
	}

	public override void OnDeselected ()
	{
	}

	public override InputProcessType ProcessType {
		get {
			return InputProcessType.MobileJoystick;
		}
	}
	#endregion
}
