﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MonoExtensions;

public class EnemyAI : InputProcessor {
	#region implemented abstract members of InputProcessor

	public Image targetIcon;
	public Transform target;

	public Vector3 screenPos;
	public Vector2 output;
	private Animator animator;

	public Vector3 primaryTargetPosition;
	public Vector3 currentTargetPosition;

	private void Awake()
	{
		animator = GetComponent<Animator>();
	}

	//Called by the VehicleController
	public override Vector2 ProcessInput ()
	{
		return output;
	}

	public void AimVehicle(Vector3 target)
	{
		float dot = Vector3.Dot(transform.forward,(target-transform.position).normalized);
		
		screenPos = Camera.main.WorldToScreenPoint(target);
		screenPos.z = 0;
		Vector3 center = new Vector3(Screen.width/2f,Screen.height/2f,0);
		
		Vector3 dir = screenPos - center;
		dir = Vector3.ClampMagnitude(dir,Screen.height/2f);
		
		output.x = dir.x/(Screen.height/2f);
		output.y = dir.y/(Screen.height/2f);
	}


	public override void OnSelected ()
	{
	}

	public override void OnDeselected ()
	{
	}

	public override InputProcessType ProcessType {
		get {
			return InputProcessType.AI;
		}
	}
	#endregion
}
