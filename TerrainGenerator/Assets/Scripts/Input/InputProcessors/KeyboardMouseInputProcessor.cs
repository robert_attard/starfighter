﻿using UnityEngine;
using System.Collections;

public class KeyboardMouseInputProcessor : InputProcessor {
	#region implemented abstract members of InputProcessor

	public override Vector2 ProcessInput ()
	{
		return Vector2.zero;
	}

	public override void OnSelected ()
	{
	}

	public override void OnDeselected ()
	{
	}

	public override InputProcessType ProcessType {
		get {
			return InputProcessType.KeyboardMouseInput;
		}
	}

	#endregion



}
