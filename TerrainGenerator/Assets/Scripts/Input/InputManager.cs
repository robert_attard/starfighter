using UnityEngine;
using System.Collections;

[RequireComponent(typeof(VehicleController))]
public class InputManager : MonoBehaviour {
	public InputProcessType selectedType;
	// Use this for initialization
	void Awake () {
		VehicleController v = GetComponent<VehicleController>();
		InputProcessor[] processors = GetComponentsInChildren<InputProcessor>();
		foreach (var inputProcessor in processors) {
			if(inputProcessor.ProcessType == selectedType)
			{
				v.inputProcessor = inputProcessor;
			}
		}
	}
}
