﻿using UnityEngine;
using System.Collections;
using MonoExtensions;
using UnityEngine.UI;
using Holoville.HOTween;

[RequireComponent(typeof(CharacterController))]
public class VehicleController : MonoBehaviour {

	public AnimationCurve controlCurve;
	public Transform pitchPivot;
	public Transform yawPivot;
	public Transform rollPivot;
	[HideInInspector]  //Assigned by InputManager ///TODO Confirm whether this should be the right implementation
	public InputProcessor inputProcessor;
	public float rollSmoothTime =0.4f;
	public float yawSpeed =80f;
	public float pitchSpeed = 70f;
	public float maxRollAngle = 60f;
	public float maxPitchAngle = 40f;
	public float maxSpeed = 900f;
	public float maxStraffeSpeed = 100f;
	public float throttleDrag = 1;

	[HideInInspector]
	public float throttle;

	private Vector3 straffeDir;
	private Vector2 viewportPos;
	public float currentSpeed = 0;
	private float rollVelocity =0;
	private bool invertPitchAxis = false;
	private bool isStabilising = false;
	private Tweener stabilisingTween;
	private CharacterController characterController;
	private const float pitchFreqScale =1f;
	private const float pitchAmpScale = 1f;
	private const float freqScale =1f;
	private const float ampScale = 1.8f;

	public void InvertPitchAxis()
	{
		invertPitchAxis=!invertPitchAxis;
	}

	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		characterController = GetComponent<CharacterController>();
	}

	// Update is called once per frame
	void Update () {
		Cursor.lockState = CursorLockMode.None;
		viewportPos = inputProcessor.ProcessInput();
		float targetRollAngle = controlCurve.Evaluate(Mathf.Abs(viewportPos.x)) * maxRollAngle * -Mathf.Sign(viewportPos.x);
		float netRollAngle = Mathf.SmoothDampAngle(rollPivot.localRotation.eulerAngles.z,targetRollAngle,ref rollVelocity,rollSmoothTime);
		rollPivot.localRotation = Quaternion.Euler(0,0,netRollAngle  + (Time.deltaTime * 10f * GetScaledSinPerlin(1f,1f)));
		float rollAnglePercentage = yawPivot.up.AngleDir(rollPivot.up,-yawPivot.forward)/maxRollAngle;
		yawPivot.Rotate(new Vector3(0,(rollAnglePercentage * yawSpeed * Time.deltaTime),0),Space.Self);

		float s = controlCurve.Evaluate(Mathf.Abs(viewportPos.y)) * pitchSpeed * -Mathf.Sign(viewportPos.y)  * ((invertPitchAxis)?-1f:1f);
		float currentPitchAngle = Angle ( pitchPivot.localRotation.eulerAngles.x);
		float newPitchAngle = Mathf.Abs( currentPitchAngle + s * Time.deltaTime);

		if(viewportPos.y==0)
		{
			if(!isStabilising && currentPitchAngle<0)
			{
				isStabilising = true;
				if(stabilisingTween!=null)
				{
					stabilisingTween.Kill();
				}
				stabilisingTween = HOTween.To(pitchPivot,3f,new TweenParms().Prop("localRotation",Vector3.zero,false).Ease(EaseType.EaseOutElastic,1f,1.5f).AutoKill(true).OnComplete(()=>{isStabilising=false;}));
			}
		}
		else
		{
			if(stabilisingTween!=null)
			{
				stabilisingTween.Kill();
			}
			isStabilising=false;
		}

		if(newPitchAngle<maxPitchAngle)
		{
			pitchPivot.Rotate(new Vector3(s * Time.deltaTime,0,0),Space.Self);
		}
		else
		{
			pitchPivot.localRotation = Quaternion.Euler(pitchPivot.localRotation.eulerAngles.x + (Time.deltaTime * 10f * GetScaledSinPerlin(pitchFreqScale,pitchAmpScale)),0,0);
		}

		Vector3 upNoise = Vector3.up * (GetScaledSinPerlin(freqScale,ampScale) * Time.deltaTime);
		characterController.Move((pitchPivot.forward * currentSpeed * Time.deltaTime)  +  upNoise + (straffeDir * maxStraffeSpeed * Time.deltaTime) );
		currentSpeed = Mathf.Lerp(currentSpeed,maxSpeed * throttle,  Time.deltaTime/throttleDrag);
	}

	//throttle is generally 0 to 1. negative values denote reverse
	public void SetThrottle(float throttle)
	{
		this.throttle = throttle;
	}

	//amount is generally between -1 to 1. 
	public void SetStrafeDir(float amount)
	{
		straffeDir = pitchPivot.right * amount; 
	}

	private float GetScaledSinPerlin(float freqScale, float ampScale)
	{
		return ((Mathf.PerlinNoise(Time.time * freqScale,50f)*2f)-1) * ampScale;
	}

	private float Angle(float angle)
	{
		if(angle>180f)
		{
			return angle-360f;
		}
		return angle;
	}
}
