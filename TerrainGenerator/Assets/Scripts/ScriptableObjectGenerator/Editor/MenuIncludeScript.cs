using UnityEngine;
using UnityEditor;
using System.Collections;

public class MenuIncludeScript : MonoBehaviour {

	[MenuItem("Assets/Create/TerrainResource")]
	public static void CreateTerrainAsset ()
	{
		ScriptableObjectUtility.CreateAsset<GradientContainer> ();
	}
}
