﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

public class PlayModeObjectPersistenceInfo
{
	public string assetPath;
	public Vector3 position;
	public Vector3 scale;
	public Quaternion rotation;
}

[ExecuteInEditMode]
public class PlayModeObjectPersistence : MonoBehaviour {

	public TextAsset dataFile;
	// Use this for initialization

	//Prioritise this script in the editor execution order list
	public void TakeSnapShot()
	{
		#if UNITY_EDITOR
			PlayModePlacement[] pList = FindObjectsOfType<PlayModePlacement>();
			List<PlayModeObjectPersistenceInfo> pInfoList = new List<PlayModeObjectPersistenceInfo>();
			foreach(PlayModePlacement p in pList)
			{
				if(p.placementType == PlayModePlacementType.PlayMode)
				{
					Object parentObject = UnityEditor.EditorUtility.GetPrefabParent(p.gameObject); 
					if(parentObject!=null)
					{
						string path = UnityEditor.AssetDatabase.GetAssetPath(parentObject);
						PlayModeObjectPersistenceInfo pInfo = new PlayModeObjectPersistenceInfo();
						pInfo.position = p.transform.position;
						pInfo.scale = p.transform.localScale;
						pInfo.rotation = p.transform.rotation;
						pInfo.assetPath = path;
						pInfoList.Add(pInfo);
					}
				}

			}
			string s = JsonConvert.SerializeObject(pInfoList,Formatting.None);
			string[] guid =UnityEditor.AssetDatabase.FindAssets("ObjectPlacementDataFile");
			if(guid.Length==0)
			{
				Debug.Log("ObjectPlacementDataFile not found");
				return;
			}
			string dataFilePath = Application.dataPath.Replace("Assets","") + UnityEditor.AssetDatabase.GUIDToAssetPath(guid[0]);
			TextWriter t = new StreamWriter(dataFilePath);
			t.Write(s);
			t.Flush();
			t.Close();
		#endif
	}

	public void ApplySnapShot()
	{
		#if UNITY_EDITOR
		string[] guid =UnityEditor.AssetDatabase.FindAssets("ObjectPlacementDataFile");
		if(guid.Length==0)
		{
			Debug.Log("ObjectPlacementDataFile not found");
			return;
		}
		string dataFilePath = Application.dataPath.Replace("Assets","") + UnityEditor.AssetDatabase.GUIDToAssetPath(guid[0]);
		TextReader t = new StreamReader(dataFilePath);
		List<PlayModeObjectPersistenceInfo> pInfoList =  JsonConvert.DeserializeObject<List<PlayModeObjectPersistenceInfo>>( t.ReadToEnd());
		t.Close();
		print(pInfoList.Count);
		foreach(PlayModeObjectPersistenceInfo p in pInfoList)
		{
			PlayModePlacement g  = UnityEditor.PrefabUtility.InstantiatePrefab(UnityEditor.AssetDatabase.LoadAssetAtPath<PlayModePlacement>(p.assetPath)) as PlayModePlacement;
			g.transform.position = p.position;
			g.transform.rotation = p.rotation;
			g.transform.localScale = p.scale;
			g.placementType = PlayModePlacementType.EditorMode;
		}
		#endif
	}
}
