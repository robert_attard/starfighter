﻿using UnityEngine;
using System.Collections;

public enum PlayModePlacementType
{
	PlayMode,
	EditorMode
}

public class PlayModePlacement : MonoBehaviour {

	public PlayModePlacementType placementType;
	public float yOffset;

	void Awake()
	{
		if(placementType== PlayModePlacementType.EditorMode)
		{
			enabled = false;
		}
	}

	void OnDrawGizmos()
	{
		if(!Application.isPlaying)
		{
			return;
		}

		RaycastHit hit;
		if(Physics.Raycast(transform.position + Vector3.up * 200f,Vector3.down,out hit,Mathf.Infinity,1<<9))
		{
			transform.position = hit.point + Vector3.up * yOffset;
		}
	}
}
