﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class PlayModeObjectPlacementEditorWindow : EditorWindow {

	[MenuItem ("Window/PlayMode Object Placement")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		PlayModeObjectPlacementEditorWindow window = (PlayModeObjectPlacementEditorWindow)EditorWindow.GetWindow (typeof (PlayModeObjectPlacementEditorWindow));
		window.Show();
	}

	void OnGUI()
	{
		if(GUILayout.Button("Load Data"))
		{

		}
	}

	private string GetDataPath()
	{
		string[] guid =AssetDatabase.FindAssets("ObjectPlacementDataFile");
		if(guid.Length==0)
		{
			Debug.Log("ObjectPlacementDataFile not found");
			return null;
		}
		return Application.dataPath.Replace("Assets","") + AssetDatabase.GUIDToAssetPath(guid[0]);
	}
}
