﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PlayModeObjectPersistence))]
public class PlayModeObjectPersistenceEditor : Editor {

	public override void OnInspectorGUI ()
	{
		GUI.enabled = true;
		base.OnInspectorGUI ();
		PlayModeObjectPersistence p = (PlayModeObjectPersistence)target;

		GUI.enabled = (Application.isPlaying);
		if(GUILayout.Button("Take SnapShot"))
		{
			p.TakeSnapShot();
		}

		GUI.enabled = !(Application.isPlaying);
		if(GUILayout.Button("Apply Snapshot"))
		{
			p.ApplySnapShot();
		}

		if(GUI.changed)
		{
			EditorUtility.SetDirty(target);
		}
	}
}
