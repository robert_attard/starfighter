﻿using UnityEngine;
using System.Collections;

public class OutlineTriangleRasterization : MonoBehaviour {

	public Vector3[] trianglePts;
	private Rasterizer rasterizer;
	public float fovScale = 1.3f;

	// Use this for initialization
	void Start () {
		rasterizer = GetComponent<Rasterizer>();
//		g = rasterizer.DrawTriangle(trianglePts[0].position.x,trianglePts[0].position.z
//		                            ,trianglePts[1].position.x,trianglePts[1].position.z
//		                            ,trianglePts[2].position.x,trianglePts[2].position.z);
	}

	GameObject g;
	
	// Update is called once per frame
	void Update () {

		trianglePts = CreateCameraRectangle(Camera.main,fovScale);

		if(g!=null)
		{
			Destroy(g);
		}

		g = rasterizer.DrawTriangle(trianglePts[0].x,trianglePts[0].z
		                        ,trianglePts[1].x,trianglePts[1].z
		                        ,trianglePts[2].x,trianglePts[2].z);
	}


	public Vector3[] CreateCameraRectangle(Camera camera, float scaleOffset)
	{
		float diff = (camera.farClipPlane*scaleOffset -camera.farClipPlane)/2f;

		Vector3[] pts = new Vector3[3];
		pts[0] = camera.transform.position - (camera.transform.forward * diff);

		float farClipLength = camera.farClipPlane*scaleOffset;
		float vFOVrad = Camera.main.fieldOfView * Mathf.Deg2Rad;
		float cameraHeightAt1 = Mathf.Tan(vFOVrad *0.5f);
		float hFOVrad = Mathf.Atan(cameraHeightAt1 * Camera.main.aspect) * 2f;
		float hFOV = (hFOVrad * Mathf.Rad2Deg)/2f;
		float otherAngle = 180f-hFOV-90f;

		float a  = (farClipLength/Mathf.Sin(Mathf.Deg2Rad*otherAngle))* Mathf.Sin(Mathf.Deg2Rad*hFOV);

		
		pts[1] = pts[0]+ (camera.transform.forward*farClipLength) + (-camera.transform.right * a);
		pts[2] = pts[0]+ (camera.transform.forward*farClipLength) + (camera.transform.right * a);


		return pts;
	}


	void OnDrawGizmos()
	{
		if(!Application.isPlaying)
		{
			trianglePts = CreateCameraRectangle(Camera.main,fovScale);
			if(trianglePts!=null)
			{
				Gizmos.color = new Color(1f,0,0,0.4f);
				
				for (int i = 1; i < trianglePts.Length; i++) {
					Gizmos.DrawLine(trianglePts [i-1],trianglePts [i]);
					Gizmos.DrawSphere(trianglePts [i-1],5f);
				}
				Gizmos.DrawLine(trianglePts [trianglePts.Length-1],trianglePts [0]);
				Gizmos.DrawSphere(trianglePts [trianglePts.Length-1],5f);
			}
		}
	}
}
