﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Edge
{
	public float X1;
	public float Y1;
	public float X2;
	public float Y2;
	
	private float gradient;
	public float Gradient{
		get{
			return gradient;
		}
		private set{
			gradient=value;
		}
	}
	
	private float intercept;
	public float Intercept{
		get{
			return intercept;
		}
		private set{
			intercept=value;
		}
	}
	
	public Edge(Vector2 v1, Vector2 v2)
	{
		X1 = v1.x;
		Y1 = v1.y;
		X2 = v2.x;
		Y2 = v2.y;
		
		Gradient =     ((X2-X1)==0)?0:(Y2-Y1)/(X2-X1);
		Intercept = Y1 - (Gradient*X1);
	}
}

public class Span
{
	public float X1;
	public float X2;
	public Span(float x1, float x2)
	{
		this.X1 = x1;
		this.X2 = x2;
	}
}



public class Rasterizer : MonoBehaviour {
	
	public Transform pixel;
	public float pixelWidth =1f;
	
	GameObject SetPixel(float x, float y)
	{
		GameObject g = Instantiate(pixel.gameObject,new Vector3(x,0,y),Quaternion.identity) as GameObject;
		g.transform.localScale = Vector3.one * pixelWidth;
		return g;
	}
	
	public GameObject DrawLine(Vector3 a,Vector3 b)
	{
		return DrawLine(a.x,a.z,b.x,b.z);
	}
	
	public GameObject DrawLine(float x1, float y1,float x2, float y2)
	{
		GameObject g = new GameObject("Edge");
		x1 = Mathf.Floor(x1/pixelWidth) * pixelWidth;
		y1 = Mathf.Floor(y1/pixelWidth) * pixelWidth;
		x2 = Mathf.Floor(x2/pixelWidth) * pixelWidth;
		y2 = Mathf.Floor(y2/pixelWidth) * pixelWidth;
		
		float xdiff = (x2 - x1);
		float ydiff = (y2 - y1);
		
		if(xdiff == 0.0f && ydiff == 0.0f) {
			SetPixel(x1, y1);
			return null;
		}
		
		if(Mathf.Abs(xdiff) > Mathf.Abs(ydiff)) {
			float xmin, xmax;
			
			// set xmin to the lower x value given
			// and xmax to the higher value
			if(x1 < x2) {
				xmin = x1;
				xmax = x2;
			}  else {
				xmin = x2;
				xmax = x1;
			}
			float slope = ydiff / xdiff;
			for(float x = xmin; x <= xmax; x += pixelWidth) {
				float y = ConvertToPixelGrid( y1 + ((x - x1) * slope));
				GameObject ptObj = SetPixel(x, y);
				ptObj.transform.parent = g.transform;
			}
		}  else {
			float ymin, ymax;
			if(y1 < y2) {
				ymin = y1;
				ymax = y2;
			}  else {
				ymin = y2;
				ymax = y1;
			}
			
			// draw line in terms of x slope
			float slope = xdiff / ydiff;
			
			for(float y = ymin; y <= ymax; y += pixelWidth) {
				float x =   ConvertToPixelGrid( x1 + ((y - y1) * slope));
				GameObject ptObj = SetPixel(x, y);
				ptObj.transform.parent = g.transform;
			}
		}
		return g;
	}
	public Edge[] edges;
	public GameObject triangle;
	public GameObject DrawTriangle(float x1, float y1,float x2, float y2,float x3, float y3)
	{
		triangle = new GameObject("Triangle");
		//		x1= ConvertToPixelGrid(x1);
		y1= ConvertToPixelGrid(y1);
		//		x2= ConvertToPixelGrid(x2);
		y2= ConvertToPixelGrid(y2);
		//		x3= ConvertToPixelGrid(x3);
		y3= ConvertToPixelGrid(y3);
		
		
		List<Vector2> vertices = new List<Vector2>();
		vertices.Add(new Vector2(x1,y1));
		vertices.Add(new Vector2(x2,y2));
		vertices.Add(new Vector2(x3,y3));
		
		vertices.Sort((a,b)=>{return a.y.CompareTo(b.y);});
		
		
		// create edges for the triangle
		edges = new Edge[]{ new Edge(new Vector2(x1,y1),new Vector2(x2,y2)),
			new Edge(new Vector2(x2,y2),new Vector2(x3,y3)),
			new Edge(new Vector2(x3,y3),new Vector2(x1,y1))};
		
		Vector2 middlePt = vertices[1];
		Vector2 slicePt = Vector2.zero;
		
		foreach(Edge e in edges)
		{
			float x =middlePt.x;
			float yResult = e.Intercept;
			
			if(e.Gradient !=0)
			{
				print ("gradient is not 0");
				x = (middlePt.y - e.Intercept)/e.Gradient;
				yResult = e.Gradient*x + e.Intercept;
			}
			
			if( !(Mathf.Round(x) == Mathf.Round(middlePt.x)) ||  !(Mathf.Round(yResult) == Mathf.Round(middlePt.y)))
			{
				slicePt = new Vector2(x,ConvertToPixelGrid( middlePt.y));
				break;
			}
		}
		
		if(Mathf.Approximately(middlePt.y,vertices[0].y))
		{
			print ("top pyramid");
			if(vertices[0].x>=middlePt.x)
			{
				print("a");
				DrawSpansBetweenEdges(new Edge(middlePt,vertices[2]), new Edge(vertices[0],vertices[2]));
				return triangle;
			}
			else
			{
				print("b");
				DrawSpansBetweenEdges(new Edge(vertices[0],vertices[2]), new Edge(middlePt,vertices[2]));
				return triangle;
			}
		}

		if(slicePt.x>=middlePt.x)
		{
			print ("c");
			DrawSpansBetweenEdges(new Edge(vertices[0],middlePt), new Edge(vertices[0],slicePt));
			DrawSpansBetweenEdges(new Edge(middlePt,vertices[2]), new Edge(slicePt,vertices[2]));
		}
		else
		{
			print ("d");
			DrawSpansBetweenEdges(new Edge(vertices[0],slicePt), new Edge(vertices[0],middlePt));
			DrawSpansBetweenEdges(new Edge(slicePt,vertices[2]), new Edge(middlePt,vertices[2]));
		}
		
		return triangle;
	}
	
	void DrawSpansBetweenEdges(Edge e1, Edge e2)
	{
		for(float y =  e2.Y1; y <   e2.Y2; y+=pixelWidth) {
			float X1 = (float.IsInfinity((y-e1.Intercept)/e1.Gradient)?e1.X1:(y-e1.Intercept)/e1.Gradient);
			float X2 = (float.IsInfinity((y-e2.Intercept)/e2.Gradient)?e2.X1:(y-e2.Intercept)/e2.Gradient);
			
			Span span= new Span( X1 , X2);
			DrawSpan(span, y);
		}
	}
	
	void DrawSpan(Span span, float y)
	{
		float xdiff = span.X2 - span.X1;
		
		for(float x =ConvertToPixelGrid( span.X1); x <= ConvertToPixelGrid( span.X2); x+=pixelWidth) {
			GameObject g = SetPixel(x,y);
			g.transform.parent = triangle.transform;
		}
	}
	
	private void CreateMarker(Vector3 pos, string name)
	{
		GameObject s = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		s.transform.localScale = Vector3.one*5f;
		s.name = name;
		s.transform.position = pos;
	}
	
	private Vector3 ToZ(Vector2 a)
	{
		return new Vector3(a.x,0,a.y);
	}
	
	public float ConvertToPixelGrid(float coordinate)
	{
		return Mathf.Floor(coordinate/pixelWidth)*pixelWidth;
	}
	
}

