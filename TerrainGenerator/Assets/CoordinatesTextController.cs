﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoordinatesTextController : MonoBehaviour {

	public VehicleController playerVehicle;
	public Text coordinatesTextX;
	public Text coordinatesTextZ;
	
	// Use this for initialization

	// Update is called once per frame
	void Update () {
		float x = Mathf.Round(playerVehicle.transform.position.x*100f)/100f;
		float z = Mathf.Round(playerVehicle.transform.position.z*100f)/100f;
		coordinatesTextX.text = x.ToString();
		coordinatesTextZ.text = z.ToString();
		
	}
}
