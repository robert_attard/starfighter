﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class CameraShake : MonoBehaviour {

	private Tweener shakeTween;

	void Awake()
	{
		NotificationCenter.AddListener(HandleCameraShake,NotificationType.CameraShake);
		shakeTween = HOTween.Shake(transform,1f,new TweenParms().Prop("localPosition",Vector3.one * 2f,false).AutoKill(false).OnComplete(()=>{shakeTween.Rewind();}),1f,0.4f);
		shakeTween.Pause();
	}

	void OnDestroy()
	{
		NotificationCenter.RemoveListener(HandleCameraShake,NotificationType.CameraShake);
	}

	void HandleCameraShake (Notification note)
	{
		Debug.Log("Shake");
		if(shakeTween.isPaused)
		{
			shakeTween.Play();
		}
	}
}
