﻿using UnityEngine;
using System.Collections;

public class ParticleCollisionDetector : MonoBehaviour {

	public WeaponBase gunController;
	public void OnParticleCollision(GameObject obj)
	{
		obj.SendMessageUpwards("ApplyDamage",new DamageInfo(gunController.damage,gunController.gameObject),SendMessageOptions.DontRequireReceiver);
	}
}
