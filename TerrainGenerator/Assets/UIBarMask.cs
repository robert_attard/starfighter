﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class UIBarMask : MonoBehaviour {

	private RectTransform rectTransform;
	private float initialWidth;
	private Vector2 anchoredPos;
	// Use this for initialization

	void Awake () {
		rectTransform = GetComponent<RectTransform>();
		initialWidth = rectTransform.rect.width;
		anchoredPos = rectTransform.anchoredPosition;
	}

	public void SetBarValue(float percentage)
	{
		float currentWidth = Mathf.Clamp(initialWidth*percentage,0,initialWidth);
		rectTransform.sizeDelta = new Vector2(currentWidth,rectTransform.rect.height);
		rectTransform.anchoredPosition = new Vector2 (anchoredPos.x - ((initialWidth-currentWidth)),anchoredPos.y);
	}
}
