﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RadarMarker : MonoBehaviour {
	
	private RectTransform rectTransform;
	[HideInInspector]
	public RadarTarget radarTarget;
	//TODO this must be assigned differently in network 
	private Transform playerTransform;

	public RectTransform horizonIndicator;
	public RectTransform markerRTransform;

	public Image blackDot;
	public Image highLight;

	// Use this for initialization

	void Awake()
	{
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		rectTransform = GetComponent<RectTransform>();
	}

	private float dot;
	private float dotLeftRight;
	private float theta;
	public float distance;

	void Update()
	{
		VehicleController v =playerTransform.GetComponent<VehicleController>();
		Vector3 playerFlat = new Vector3(playerTransform.position.x,radarTarget.transform.position.y,playerTransform.position.z);
		Vector3 enemyDir = (radarTarget.transform.position -playerFlat).normalized;
		dot = Vector3.Dot(v.yawPivot.forward,enemyDir);

		if(dot>0)
		{
			distance = Vector3.Distance(playerTransform.position, radarTarget.transform.position);
			if(IsInView())
			{
				blackDot.gameObject.SetActive((distance>700f));
			}
			else
			{
				blackDot.gameObject.SetActive(true);
			}
		}
		else
		{
			blackDot.gameObject.SetActive(true);	
		}

		dotLeftRight = Vector3.Dot(v.yawPivot.right,enemyDir);
		theta = Mathf.Rad2Deg * Mathf.Acos(dot) * Mathf.Sign(-dotLeftRight);
		theta = float.IsNaN(theta)?0:theta;
		rectTransform.transform.rotation = Quaternion.Euler(0,0,theta);

		Vector3 horizonDir = (markerRTransform.transform.position - horizonIndicator.transform.position).normalized;
		float horizonDot = Vector3.Dot(horizonIndicator.transform.up,horizonDir);

		blackDot.enabled = (horizonDot<0);
		highLight.enabled = (horizonDot>=0);
	}

	private bool IsInView()
	{
		Vector2 v = Camera.main.WorldToViewportPoint(radarTarget.transform.position);
		return (v.x>0 && v.x<1f && v.y>0 && v.y<1f);
	}
}
