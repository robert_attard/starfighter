﻿using UnityEngine;
using System.Collections;

public class PlayerFacingDecal : MonoBehaviour {

	private Transform player;
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}


	// Update is called once per frame
	void LateUpdate () {
	
		Vector3 lookPoint = new Vector3(player.position.x,transform.position.y,player.position.z);
		Vector3 lookDir = lookPoint - transform.position;
		transform.rotation = Quaternion.LookRotation(-lookDir);
	}
}
