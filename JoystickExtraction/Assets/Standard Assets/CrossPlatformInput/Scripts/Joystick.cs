using System;
using UnityEngine;
using UnityEngine.EventSystems;


public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public int MovementRange = 100;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use

	Vector3 m_StartPos;
	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	public Vector3 viewportDelta;


	void OnEnable()
	{
		CreateVirtualAxes();
	}

    void Start()
    {
        m_StartPos = transform.position;
    }

	void UpdateVirtualAxes(Vector3 value)
	{
		viewportDelta = value - m_StartPos;
		viewportDelta /= MovementRange;
		viewportDelta.x = (viewportDelta.x/2f) + 0.5f;
		viewportDelta.y = (viewportDelta.y/2f) + 0.5f;
	}

	void CreateVirtualAxes()
	{
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);
	}

	void Update()
	{
		Debug.Log(viewportDelta);
	}


	public void OnDrag(PointerEventData data)
	{
		Vector3 newPos = Vector3.zero;

		if (m_UseX)
		{
			int delta = (int)(data.position.x - m_StartPos.x);
			delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
			newPos.x = delta;
		}

		if (m_UseY)
		{
			int delta = (int)(data.position.y - m_StartPos.y);
			delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
			newPos.y = delta;
		}
		transform.position = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);
		UpdateVirtualAxes(transform.position);
	}


	public void OnPointerUp(PointerEventData data)
	{
		transform.position = m_StartPos;
		UpdateVirtualAxes(m_StartPos);
	}

	public void OnPointerDown(PointerEventData data) { }
}
