﻿using UnityEngine;
using System.Collections;

public class TriTreeNode 
{
	public TriTreeNode LeftChild;
	public TriTreeNode RightChild;
	public TriTreeNode BaseNeighbor;
	public TriTreeNode LeftNeighbor;
	public TriTreeNode RightNeighbor;
}
