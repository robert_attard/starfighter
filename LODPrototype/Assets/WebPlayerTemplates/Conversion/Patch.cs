﻿using UnityEngine;
using System.Collections;

public class Patch : MonoBehaviour {

	public const int VARIANCE_DEPTH =9;
	private TriTreeNode m_BaseLeft;
	private TriTreeNode m_BaseRight;

	private int m_WorldX;
	private int m_WorldY;
	private bool m_VarianceDirty;
	private bool m_isVisible;
	private int[] m_VarianceLeft;			// Left variance tree
	private int[] m_VarianceRight;
	private int[] m_CurrentVariance;
	

	private Landscape landscape;

	private TerrainCharacteristic m_HeightMap;

	public Patch(Landscape landscape)
	{
		this.landscape = landscape;
		m_VarianceLeft = new int[ 1<<(VARIANCE_DEPTH)];
		m_VarianceRight = new int[ 1<<(VARIANCE_DEPTH)];
	}

	void Split(TriTreeNode tri)
	{
		// We are already split, no need to do it again.
		if (tri.LeftChild!=null)
			return;
		
		// If this triangle is not in a proper diamond, force split our base neighbor
		if ( tri.BaseNeighbor!=null && (tri.BaseNeighbor.BaseNeighbor != tri) )
		{
			Split(tri.BaseNeighbor);
		}

		// Create children and link into mesh
		tri.LeftChild  = new TriTreeNode();
		tri.RightChild = new TriTreeNode();
		
		// If creation failed, just exit.
		if ( tri.LeftChild==null )
		{
			return;
		}
		
		// Fill in the information we can get from the parent (neighbor pointers)
		tri.LeftChild.BaseNeighbor  = tri.LeftNeighbor;
		tri.LeftChild.LeftNeighbor  = tri.RightChild;

		tri.RightChild.BaseNeighbor  = tri.RightNeighbor;
		tri.RightChild.RightNeighbor = tri.LeftChild;

		// Link our Left Neighbor to the new children
		if (tri.LeftNeighbor != null)
		{
			if (tri.LeftNeighbor.BaseNeighbor == tri)
				tri.LeftNeighbor.BaseNeighbor = tri.LeftChild;
			else if (tri.LeftNeighbor.LeftNeighbor == tri)
				tri.LeftNeighbor.LeftNeighbor = tri.LeftChild;
			else if (tri.LeftNeighbor.RightNeighbor == tri)
				tri.LeftNeighbor.RightNeighbor = tri.LeftChild;
			else
				Debug.LogError("Illegal Left Neighbor!");
		}
		
		// Link our Right Neighbor to the new children
		if (tri.RightNeighbor != null)
		{
			if (tri.RightNeighbor.BaseNeighbor == tri)
				tri.RightNeighbor.BaseNeighbor = tri.RightChild;
			else if (tri.RightNeighbor.RightNeighbor == tri)
				tri.RightNeighbor.RightNeighbor = tri.RightChild;
			else if (tri.RightNeighbor.LeftNeighbor == tri)
				tri.RightNeighbor.LeftNeighbor = tri.RightChild;
			else
				Debug.LogError("Illegal Right Neighbor!");
		}
		
		// Link our Base Neighbor to the new children
		if (tri.BaseNeighbor != null)
		{
			if ( tri.BaseNeighbor.LeftChild!=null)
			{
				tri.BaseNeighbor.LeftChild.RightNeighbor = tri.RightChild;
				tri.BaseNeighbor.RightChild.LeftNeighbor = tri.LeftChild;
				tri.LeftChild.RightNeighbor = tri.BaseNeighbor.RightChild;
				tri.RightChild.LeftNeighbor = tri.BaseNeighbor.LeftChild;
			}
			else
				Split( tri.BaseNeighbor);  // Base Neighbor (in a diamond with us) was not split yet, so do that now.
		}
		else
		{
			// An edge triangle, trivial case.
			tri.LeftChild.RightNeighbor = null;
			tri.RightChild.LeftNeighbor = null;
		}
	}

	public TriTreeNode GetBaseLeft()  
	{ 
		return m_BaseLeft; 
	}

	public TriTreeNode GetBaseRight() 
	{ 
		return m_BaseRight; 
	}

	public bool isDirty()     { return m_VarianceDirty; }
	public bool  isVisibile( ) { return m_isVisible; }

	public void Init(int heightX, int heightY, int worldX, int worldY, TerrainCharacteristic tc )
	{
		m_BaseLeft = new TriTreeNode();
		m_BaseRight = new TriTreeNode();

		m_BaseLeft.RightNeighbor = m_BaseLeft.LeftNeighbor = m_BaseRight.RightNeighbor = m_BaseRight.LeftNeighbor =
			m_BaseLeft.LeftChild = m_BaseLeft.RightChild = m_BaseRight.LeftChild = m_BaseLeft.LeftChild = null;
		
		// Attach the two m_Base triangles together
		m_BaseLeft.BaseNeighbor = m_BaseRight;
		m_BaseRight.BaseNeighbor = m_BaseLeft;
		
		// Store Patch offsets for the world and heightmap.
		m_WorldX = worldX;
		m_WorldY = worldY;
		
		// Store pointer to first byte of the height data for this patch.
		m_HeightMap = tc;
		
		// Initialize flags
		m_VarianceDirty = true;
		m_isVisible = false;
	}

	public void ComputeVariance()
	{
		// Compute variance on each of the base triangles...
		
		m_CurrentVariance = m_VarianceLeft;
		RecursComputeVariance(	0,Landscape.PATCH_SIZE, (int)m_HeightMap.Generate(0,Landscape.PATCH_SIZE)
		                      ,Landscape.PATCH_SIZE, 0,(int)m_HeightMap.Generate(Landscape.PATCH_SIZE,0)
		                      ,0,0, (int)m_HeightMap.Generate(0,0)
		                      ,1);

		m_CurrentVariance = m_VarianceRight;
		RecursComputeVariance(	Landscape.PATCH_SIZE, 0,(int)m_HeightMap.Generate(Landscape.PATCH_SIZE,0)
		                      ,0,Landscape.PATCH_SIZE, (int)m_HeightMap.Generate(0,Landscape.PATCH_SIZE)
		                      ,Landscape.PATCH_SIZE, Landscape.PATCH_SIZE, (int)m_HeightMap.Generate(Landscape.PATCH_SIZE,Landscape.PATCH_SIZE)
		                      ,1);
		
		// Clear the dirty flag for this patch
		m_VarianceDirty = false;
	}

	private int RecursComputeVariance( int leftX,  int leftY,  int leftZ,int rightX, int rightY, int rightZ,int apexX,  int apexY,  int apexZ,int node)
	{

		int centerX = (leftX + rightX) >>1;		// Compute X coordinate of center of Hypotenuse
		int centerY = (leftY + rightY) >>1;		// Compute Y coord...
		int myVariance =0;
		
		// Get the height value at the middle of the Hypotenuse
		int centerZ  = (int)m_HeightMap.Generate((float)centerX,(float)centerY);//[(centerY * MAP_SIZE) + centerX];
		
		// Variance of this triangle is the actual height at it's hypotenuse midpoint minus the interpolated height.
		// Use values passed on the stack instead of re-accessing the Height Field.
		myVariance = Mathf.Abs((int)centerZ - (((int)leftZ + (int)rightZ)>>1));
		
		// Since we're after speed and not perfect representations,
		//    only calculate variance down to an 8x8 block
		if ( (Mathf.Abs(leftX - rightX) >= 8) ||(Mathf.Abs(leftY - rightY) >= 8) )
		{
			// Final Variance for this node is the max of it's own variance and that of it's children.
			myVariance = Mathf.Max( myVariance, RecursComputeVariance( apexX,   apexY,  apexZ, leftX, leftY, leftZ, centerX, centerY, centerZ,    node<<1 ) );
			myVariance = Mathf.Max( myVariance, RecursComputeVariance( rightX, rightY, rightZ, apexX, apexY, apexZ, centerX, centerY, centerZ, 1+(node<<1)) );
		}
		
		// Store the final variance for this node.  Note Variance is never zero.
		if (node < (1<<VARIANCE_DEPTH))
		{
			m_CurrentVariance[node] = 1 + myVariance;
		}
		
		return myVariance;
	}

	void RecursTessellate( TriTreeNode tri,int leftX,  int leftY,int rightX, int rightY,int apexX,  int apexY,int node )
	{
		float TriVariance =0;
		int centerX = (leftX + rightX)>>1; // Compute X coordinate of center of Hypotenuse
		int centerY = (leftY + rightY)>>1; // Compute Y coord...
		
		if ( node < (1<<VARIANCE_DEPTH) )
		{
			// Extremely slow distance metric (sqrt is used).
			// Replace this with a faster one!
			float distance = 1.0f + Mathf.Sqrt( SQR((float)centerX - landscape.gViewPosition.x) +
			                              SQR((float)centerY - landscape.gViewPosition.y) );
			
			// Egads!  A division too?  What's this world coming to!
			// This should also be replaced with a faster operation.
			TriVariance = ((float)m_CurrentVariance[node] *  Landscape.MAP_SIZE * 2)/distance;	// Take both distance and variance into consideration
		}
		
		if ( (node >= (1<<VARIANCE_DEPTH)) ||	(TriVariance > Landscape.gFrameVariance))
		{
			Split (tri);														// Split this triangle.
			
			if (tri.LeftChild!=null && ((Mathf.Abs(leftX - rightX) >= 3) || (Mathf.Abs(leftY - rightY) >= 3)))	// Tessellate all the way down to one vertex per height field entry
			{
				RecursTessellate( tri.LeftChild,   apexX,  apexY, leftX, leftY, centerX, centerY,    node<<1  );
				RecursTessellate( tri.RightChild, rightX, rightY, apexX, apexY, centerX, centerY, 1+(node<<1) );
			}
		}
	}

	public static float SQR(float a)
	{
		return Mathf.Pow(a,2f);
	}

	void RecursRender( TriTreeNode tri, int leftX, int leftY, int rightX, int rightY, int apexX, int apexY )
	{
		if ( tri.LeftChild!=null )					// All non-leaf nodes have both children, so just check for one
		{
			int centerX = (leftX + rightX)>>1;	// Compute X coordinate of center of Hypotenuse
			int centerY = (leftY + rightY)>>1;	// Compute Y coord...
			
			RecursRender( tri.LeftChild,  apexX,   apexY, leftX, leftY, centerX, centerY );
			RecursRender( tri.RightChild, rightX, rightY, apexX, apexY, centerX, centerY );
		}
		else									// A leaf node!  Output a triangle to be rendered.
		{
			// Actual number of rendered triangles...
			Landscape.gNumTrisRendered++;
			
			float leftZ  = m_HeightMap.Generate(leftX,leftY);
			float rightZ = m_HeightMap.Generate(rightX,rightY);
			float apexZ  = m_HeightMap.Generate(apexX,apexY);

//			Debug.Log("apex " + new Vector3(apexX,apexZ,apexY));
			Debug.DrawLine(new Vector3(apexX,apexZ,apexY),new Vector3(leftX,leftZ,leftY));
			Debug.DrawLine(new Vector3(apexX,apexZ,apexY),new Vector3(rightX,rightZ,rightY));
			Debug.DrawLine(new Vector3(leftX,leftZ,leftY),new Vector3(rightX,rightZ,rightY));
		}
	}

	public void Reset()
	{
		// Assume patch is not visible.
		m_isVisible = false;
		
		// Reset the important relationships
		m_BaseLeft.LeftChild = m_BaseLeft.RightChild = m_BaseRight.LeftChild = m_BaseLeft.LeftChild = null;
		
		// Attach the two m_Base triangles together
		m_BaseLeft.BaseNeighbor = m_BaseRight;
		m_BaseRight.BaseNeighbor = m_BaseLeft;
		
		// Clear the other relationships.
		m_BaseLeft.RightNeighbor = m_BaseLeft.LeftNeighbor = m_BaseRight.RightNeighbor = m_BaseRight.LeftNeighbor = null;
	}

	public static int orientation( int pX, int pY, int qX, int qY, int rX, int rY )
	{
		int aX, aY, bX, bY;
		float d;
		
		aX = qX - pX;
		aY = qY - pY;
		
		bX = rX - pX;
		bY = rY - pY;
		
		d = (float)aX * (float)bY - (float)aY * (float)bX;
		int a =(d < 0) ? (-1) : (d > 0)?1:0;
		return a;
	}

	public void SetVisibility( int eyeX, int eyeY, int leftX, int leftY, int rightX, int rightY )
	{
		// Get patch's center point
		int patchCenterX = m_WorldX + Landscape.PATCH_SIZE / 2;
		int patchCenterY = m_WorldY + Landscape.PATCH_SIZE / 2;
		
		// Set visibility flag (orientation of both triangles must be counter clockwise)
//		m_isVisible = (orientation * ( eyeX,  eyeY,  rightX, rightY, patchCenterX, patchCenterY ) < 0) &&
//			(orientation * ( leftX, leftY, eyeX,   eyeY,   patchCenterX, patchCenterY ) < 0);

		m_isVisible = true;
	}

	public void Tessellate()
	{
		// Split each of the base triangles
		m_CurrentVariance = m_VarianceLeft;
		RecursTessellate (	m_BaseLeft,
		                  m_WorldX,m_WorldY+Landscape.PATCH_SIZE,
		                  m_WorldX+ Landscape.PATCH_SIZE,	m_WorldY,
		                  m_WorldX,				m_WorldY,
		                  1 );
		
		m_CurrentVariance = m_VarianceRight;
		RecursTessellate(	m_BaseRight,
		                 m_WorldX+ Landscape.PATCH_SIZE,	m_WorldY,
		                 m_WorldX,m_WorldY+Landscape.PATCH_SIZE,
		                 m_WorldX+Landscape.PATCH_SIZE,	m_WorldY+Landscape.PATCH_SIZE,
		                 1 );
	}

	public void Render()
	{
		RecursRender (m_BaseLeft,0,Landscape.PATCH_SIZE,Landscape.PATCH_SIZE,0,0,0);
		RecursRender(m_BaseRight,Landscape.PATCH_SIZE,0,0,Landscape.PATCH_SIZE,Landscape.PATCH_SIZE,Landscape.PATCH_SIZE);
	}

}
