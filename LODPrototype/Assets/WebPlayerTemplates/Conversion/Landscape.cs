﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class Landscape : MonoBehaviour {

	public const int MAP_SIZE = 256;
	public const int NUM_PATCHES_PER_SIDE =8;
	public const int PATCH_SIZE  = (MAP_SIZE/NUM_PATCHES_PER_SIDE);
	public const float MULT_SCALE = 0.5f;
	public Patch[,] m_Patches;	
	public TerrainCharacteristic tCharacteristic;
	public static float gFrameVariance = 50;
	public static int gNumTrisRendered;
	public Triangle2D viewTriangle;
	public float farClip = 1000f;

	public int gDesiredTris = 100;
	static int	m_NextTriNode;	
	public Vector2 gViewPosition
	{
		get{
			return new Vector2(Camera.main.transform.position.x,Camera.main.transform.position.z);
		}
	}

	// Use this for initialization
	void Start () {
	
		Init();
	}
	
	// Update is called once per frame
	void Update () {
		viewTriangle = new Triangle2D(CreateCameraRectangle(Camera.main,1f));
		Reset();
		Tessellate();
		Render();
	}

	private void Init()
	{
		tCharacteristic.Init(1f);
		m_Patches = new Patch[NUM_PATCHES_PER_SIDE,NUM_PATCHES_PER_SIDE];
		for ( int Y=0; Y < NUM_PATCHES_PER_SIDE; Y++)
		{
			for ( int X=0; X < NUM_PATCHES_PER_SIDE; X++ )
			{
				Patch p = new Patch(this);
				m_Patches[X,Y] = p;
				p.Init( X*PATCH_SIZE, Y*PATCH_SIZE, X*PATCH_SIZE, Y*PATCH_SIZE, tCharacteristic );
				p.ComputeVariance();
			}
		}
	}

	public Vector2[] CreateCameraRectangle(Camera camera, float scaleOffset)
	{
		float diff = (farClip * scaleOffset - farClip)/2f;
		
		Vector3[] Pts = new Vector3[3];
		Pts[0] = camera.transform.position - (camera.transform.forward * diff);
		
		float farClipLength = farClip * scaleOffset;
		float vFOVrad = Camera.main.fieldOfView * Mathf.Deg2Rad;
		float cameraHeightAt1 = Mathf.Tan(vFOVrad *0.5f);
		float hFOVrad = Mathf.Atan(cameraHeightAt1 * Camera.main.aspect) * 2f;
		float hFOV = (hFOVrad * Mathf.Rad2Deg)/2f;
		float otherAngle = 180f-hFOV-90f;
		
		float a  = (farClipLength/Mathf.Sin(Mathf.Deg2Rad*otherAngle))* Mathf.Sin(Mathf.Deg2Rad*hFOV);
		
		Pts[1] = Pts[0]+ (camera.transform.forward*farClipLength) + (-camera.transform.right * a);
		Pts[2] = Pts[0]+ (camera.transform.forward*farClipLength) + (camera.transform.right * a);
		
		Vector2[] pts2 = new Vector2[3];
		pts2[0] = Pts[0].ConvertZtoY();
		pts2[1] = Pts[1].ConvertZtoY();
		pts2[2] = Pts[2].ConvertZtoY();
		return pts2;
	}

	
	static int GetNextTriNode() { return m_NextTriNode; }
	static void SetNextTriNode( int nNextNode ) { m_NextTriNode = nNextNode; }

	public void Reset()
	{
		//
		// Perform simple visibility culling on entire patches.
		//   - Define a triangle set back from the camera by one patch size, following
		//     the angle of the frustum.
		//   - A patch is visible if it's center point is included in the angle: Left,Eye,Right
		//   - This visibility test is only accurate if the camera cannot look up or down significantly.
		//
		const float PI_DIV_180 = Mathf.PI / 180.0f;
		const float FOV_DIV_2 = 60/2;
		
		int eyeX = (int)viewTriangle.Apex.x;
		int eyeY = (int)viewTriangle.Apex.y;

		int leftX  = (int)viewTriangle.pts[1].x;
		int leftY  = (int)viewTriangle.pts[1].y;
		
		int rightX = (int)viewTriangle.pts[2].x;
		int rightY = (int)viewTriangle.pts[2].y;
		
		Patch patch = null;
		
		// Set the next free triangle pointer back to the beginning
		SetNextTriNode(0);
		
		// Reset rendered triangle count.
		gNumTrisRendered = 0;
		
		// Go through the patches performing resets, compute variances, and linking.
		for( int Y=0; Y < NUM_PATCHES_PER_SIDE; Y++ )
		{
			for ( int X=0; X < NUM_PATCHES_PER_SIDE; X++)
			{
				patch = (m_Patches[Y,X]);
				
				// Reset the patch
				patch.Reset();
				patch.SetVisibility( eyeX, eyeY, leftX, leftY, rightX, rightY );
				
				// Check to see if this patch has been deformed since last frame.
				// If so, recompute the varience tree for it.
				if ( patch.isDirty())
				{
					patch.ComputeVariance();
				}
				
				if ( patch.isVisibile())
				{
					// Link all the patches together.
					if ( X > 0 )
						patch.GetBaseLeft().LeftNeighbor = m_Patches[Y,X-1].GetBaseRight();
					else
						patch.GetBaseLeft().LeftNeighbor = null;		// Link to bordering Landscape here..
					
					if ( X < (NUM_PATCHES_PER_SIDE-1) )
						patch.GetBaseRight().LeftNeighbor = m_Patches[Y,X+1].GetBaseLeft();
					else
						patch.GetBaseRight().LeftNeighbor = null;		// Link to bordering Landscape here..
					
					if ( Y > 0 )
						patch.GetBaseLeft().RightNeighbor = m_Patches[Y-1,X].GetBaseRight();
					else
						patch.GetBaseLeft().RightNeighbor = null;		// Link to bordering Landscape here..
					
					if ( Y < (NUM_PATCHES_PER_SIDE-1) )
						patch.GetBaseRight().RightNeighbor = m_Patches[Y+1,X].GetBaseLeft();
					else
						patch.GetBaseRight().RightNeighbor = null;	// Link to bordering Landscape here..
				}
			}
		}
	}

	private void Tessellate()
	{
		for ( int Y=0; Y < NUM_PATCHES_PER_SIDE; Y++)
		{
			for ( int X=0; X < NUM_PATCHES_PER_SIDE; X++ )
			{

//				if(m_Patches[X,Y].isVisibile())
				{
					m_Patches[X,Y].Tessellate();
				}
			}
		}
	}
	
	// ---------------------------------------------------------------------
	// Render each patch of the landscape & adjust the frame variance.
	//
	private void Render()
	{
		for ( int Y=0; Y < NUM_PATCHES_PER_SIDE; Y++)
		{
			for ( int X=0; X < NUM_PATCHES_PER_SIDE; X++ )
			{
//				if(m_Patches[X,Y].isVisibile())
				{
					m_Patches[X,Y].Render();
				}
			}
		}
		
		// Check to see if we got close to the desired number of triangles.
		// Adjust the frame variance to a better value.
		if ( GetNextTriNode() != gDesiredTris )
			gFrameVariance += ((float)GetNextTriNode() - (float)gDesiredTris) / (float)gDesiredTris;
		
		// Bounds checking.
		if ( gFrameVariance < 0 )
			gFrameVariance = 0;
	}

}
