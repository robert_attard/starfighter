﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class TriangleRasterization : MonoBehaviour {

	private int pixelSize;
	public Transform p0;
	public Transform p1;
	public Transform p2;
	

	public void DrawTriangle(Vector2 pt1,Vector2 pt2, Vector2 pt3, int pixelSize)
	{
		this.pixelSize = pixelSize;
		ClampToGrid(ref pt1);
		ClampToGrid(ref pt2);
		ClampToGrid(ref pt3);

		Edge[] edges =  new Edge[]{ new Edge((int)pt1.x,(int)pt1.y,(int)pt2.x,(int)pt2.y),
									new Edge((int)pt2.x,(int)pt2.y,(int)pt3.x,(int)pt3.y),
									new Edge((int)pt3.x,(int)pt3.y,(int)pt1.x,(int)pt1.y)};
		int maxLength = 0;
		int longEdge = 0;
		
		// find edge with the greatest length in the y axis
		for(int i = 0; i < 3; i++) {
			int length = edges[i].Y2 - edges[i].Y1;
			if(length > maxLength) {
				maxLength = length;
				longEdge = i;
			}
		}

		int shortEdge1 = (longEdge + 1) % 3;
		int shortEdge2 = (longEdge + 2) % 3;

//		Debug.DrawLine(new Vector2((float)edges[longEdge].X1,(float)edges[longEdge].Y1).ConvertYtoZ(),
//		               new Vector2((float)edges[longEdge].X2,(float)edges[longEdge].Y2).ConvertYtoZ(),Color.green);

		DrawSpansBetweenEdges(edges[longEdge], edges[shortEdge1]);
//		DrawSpansBetweenEdges(edges[longEdge], edges[shortEdge2]);
	}

	private void DrawSpansBetweenEdges(Edge e1, Edge e2)
	{

		Debug.DrawLine(new Vector2((float)e1.X1,(float)e1.Y1).ConvertYtoZ(),
		               new Vector2((float)e1.X2,(float)e1.Y2).ConvertYtoZ(),Color.green);

		Debug.DrawLine(new Vector2((float)e2.X1,(float)e2.Y1).ConvertYtoZ(),
		               new Vector2((float)e2.X2,(float)e2.Y2).ConvertYtoZ(),Color.blue);

		// calculate difference between the y coordinates
		// of the first edge and return if 0
		float e1ydiff = (float)(e1.Y2 - e1.Y1);
		if(e1ydiff < pixelSize)
		{
			return;
		}
		
		// calculate difference between the y coordinates
		// of the second edge and return if 0
		float e2ydiff = (float)(e2.Y2 - e2.Y1);
		if(e2ydiff < pixelSize)
		{
			return;
		}

		// calculate differences between the x coordinates
		float e1xdiff = (float)(e1.X2 - e1.X1);
		float e2xdiff = (float)(e2.X2 - e2.X1);


		// calculate factors to use for interpolation
		// with the edges and the step values to increase
		// them by after drawing each span
		float factor1 = (float)(e2.Y1 - e1.Y1) / e1ydiff;
		float factorStep1 = 1.0f / e1ydiff;
		float factor2 = 0.0f;
		float factorStep2 = 1.0f / e2ydiff;

		// loop through the lines between the edges and draw spans
		for(int y = e2.Y1; y < e2.Y2; y+=pixelSize) {
			// create and draw span
			Span span = new Span(e1.X1 + (int)(e1xdiff * factor1),e2.X1 + (int)(e2xdiff * factor2));
			DrawSpan(span, y);
			
			// increase factors
			factor1 += factorStep1;
			factor2 += factorStep2;
		}
	}


	private void Update()
	{
		DrawTriangle(p0.position.ConvertZtoY(),p1.position.ConvertZtoY(),p2.position.ConvertZtoY(),20);
	}

	void DrawSpan(Span span, int y)
	{
		float xdiff = span.X2 - span.X1;
		if(xdiff == 0)
		{
			return;
		}
		    // draw each pixel in the span
		for(int x = span.X1; x < span.X2; x+=pixelSize) {
			DebugDrawPixel(new Vector2((float)x,(float)y));
		}
	}

	private Vector2 ClampToGrid(ref Vector2 v)
	{
		return new Vector2(ClampToGrid(v.x),ClampToGrid(v.y));
	}

	private float ClampToGrid(float a)
	{
		float result =  a - (a%pixelSize);
		return result;
	}

	public void DebugDrawPixel(Vector2 lowerLeftAnchor)
	{
		Vector3 topRight = lowerLeftAnchor.ConvertYtoZ().Extend(pixelSize,0,pixelSize);

		Debug.DrawLine(lowerLeftAnchor.ConvertYtoZ()
		               ,lowerLeftAnchor.ConvertYtoZ().Extend(pixelSize,0,0),Color.magenta);
		Debug.DrawLine(lowerLeftAnchor.ConvertYtoZ()
		               ,lowerLeftAnchor.ConvertYtoZ().Extend(0,0, pixelSize),Color.magenta);


		Debug.DrawLine(topRight
		               ,topRight.Extend(-pixelSize,0,0),Color.magenta);
		Debug.DrawLine(topRight
		               ,topRight.Extend(0,0,-pixelSize),Color.magenta);
	}
}

public class Span
{
	public int X1;
	public int X2;

	public Span(int x1, int x2)
	{
		if(x1 < x2) {
			X1 = x1;
			X2 = x2;
		} else {
			X1 = x2;
			X2 = x1;
		}
	}
}

public class Edge
{
	public int X1, Y1, X2, Y2;
	public Edge( int x1, int y1, int x2, int y2)
	{
		if(y1 < y2) {
			X1 = x1;
			Y1 = y1;
			X2 = x2;
			Y2 = y2;
		} else {
			X1 = x2;
			Y1 = y2;
			X2 = x1;
			Y2 = y1;
		}
	}
}