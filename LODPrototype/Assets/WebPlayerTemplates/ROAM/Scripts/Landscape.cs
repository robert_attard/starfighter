﻿using UnityEngine;
using System.Collections;

class Landscape : MonoBehaviour{

//TODO should contain parameter for heightmap


	// ------- 2048x2048 MAP -------
	public const int MAP_SIZE = 512;
	public const int NUM_PATCHES_PER_SIDE = 8;

	
	// ---------------------------------------------------------------------
	// Scale of the terrain ie: 1 unit of the height map == how many world units (meters)?
	// 1.0f == 1 meter resolution
	// 0.5f == 1/2 meter resolution
	// 0.25f == 1/4 meter resolution
	// etc..
	public const float MULT_SCALE = 0.5f;
	
	// How many TriTreeNodes should be allocated?
	public const int POOL_SIZE  =25000;
	
	// Some more definitions
	public const int PATCH_SIZE = MAP_SIZE/NUM_PATCHES_PER_SIDE;
//	#define TEXTURE_SIZE (128)
//	
//	// Drawing Modes
//	#define DRAW_USE_TEXTURE   (0)
//	#define DRAW_USE_LIGHTING  (1)
//	#define DRAW_USE_FILL_ONLY (2)
//	#define DRAW_USE_WIREFRAME (3)
//	
//	// Rotation Indexes
//	#define ROTATE_PITCH (0)
//	#define ROTATE_YAW   (1)
//	#define ROTATE_ROLL	 (2)
//	
//	
//	#define SQR(x) ((x) * (x))
//	#define MAX(a,b) ((a < b) ? (b) : (a))
//	#define DEG2RAD(a) (((a) * M_PI) / 180.0f)
//	#define M_PI (3.14159265358979323846f)

	protected static int m_NextTriNode; 
	// Index to the next free TriTreeNode 
	static TriTreeNode[] m_TriPool; 
	// Pool of nodes for tessellation 
	Patch[][] m_aPatches; 
	// Initialize the whole process 

	public const float PlayerX =0;
	public const float PlayerY =0;

	public static Vector2 playerPos;

	public void Init()
	{
	}

	void Reset()
	{
	}
	// Reset for a new frame 
	void Tessellate(){
	}
	// Create mesh approximation 
	void Render()
	{
	}
	// Render current mesh static 
	public static TriTreeNode AllocateTri()
	{
		//TODO Create TriTreeNode pool
		return null;
	}
}