﻿using UnityEngine;
using System.Collections;

public class TriTreeNode {

	// Our Left child
	public TriTreeNode LeftChild; 
	// Our Right child 
	public TriTreeNode RightChild; 
	// Adjacent node, below us 
	public TriTreeNode BaseNeighbor; 
	// Adjacent node, to our left 
	public TriTreeNode LeftNeighbor; 
	// Adjacent node, to our right 
	public TriTreeNode RightNeighbor; 
}


