﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class Circle2D
{
	public float radius;
	public Vector2 position;
	public Circle2D(float radius, Vector2 position)
	{
		this.radius = radius;
		this.position = position;
	}
}

public class Circle : MonoBehaviour {

	public float radius = 10f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		VectorExtension.DrawCircle(transform.position.ConvertZtoY(),radius,Color.red,10f);
	}
}
