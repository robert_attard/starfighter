﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class EdgeGenerator : MonoBehaviour {

	public Transform pt00;
	public Transform pt01;
	public SquareGenerator square;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		Color c = (Overlap(pt00.position.ConvertZtoY(),pt01.position.ConvertZtoY(),square.rect))?Color.red:Color.green;
		Debug.DrawLine(pt00.position,pt01.position,c);
	}

	public float gradient;
	public float intercept;

	private bool Overlap(Vector2 p0,Vector2 p1, Rect rect)
	{
		gradient = (p1.y-p0.y)/(p1.x-p0.x);
		intercept = p1.y - (gradient*p1.x);

		float yMinBoxX  = rect.xMin * gradient + intercept;
		if(IsInRange(yMinBoxX,p0.y,p1.y) && IsInRange(yMinBoxX,rect.yMin,rect.yMax))
		{
			return true;
		}

		float yMaxBoxX  = rect.xMax * gradient + intercept ;
		if(IsInRange(yMaxBoxX,p0.y,p1.y) && IsInRange(yMaxBoxX,rect.yMin,rect.yMax))
		{
			return true;
		}

		float xMinBoxY  =  (rect.yMin - intercept) / gradient;
		if(IsInRange(xMinBoxY,p0.x,p1.x) && IsInRange(xMinBoxY,rect.xMin,rect.xMax))
		{
			return true;
		}

		float xMaxBoxY  =  (rect.yMax - intercept) / gradient;
		if(IsInRange(xMaxBoxY,p0.x,p1.x) && IsInRange(xMaxBoxY,rect.xMin,rect.xMax))
		{
			return true;
		}

		return false;
	}

	private bool IsInRange(float v,float a,float b)
	{
		if(a>b)
		{
			float temp=a;
			a=b;
			b=temp;
		}
		return (v>=a && v<=b);
	}
}
