﻿using UnityEngine;
using System.Collections;

public class SquareGenerator : MonoBehaviour {

	public float width = 10f;
	public Rect rect;
	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		rect = new Rect(new Vector2(transform.position.x - width/2f,transform.position.z- width/2f) , new Vector2(width,width));
		RectDraw.Draw(rect,Color.red);
	}
}
