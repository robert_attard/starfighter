﻿using UnityEngine;
using System.Collections;

public class RectDraw : MonoBehaviour {

	public static void Draw(Rect rect,Color color)
	{
		Vector3 widthVector = new Vector3(rect.width,0,0);
		Vector3 heightVector = new Vector3(0,0,rect.height);

		Debug.DrawLine(VectorConv(rect.min),VectorConv(rect.min) + widthVector,color);
		Debug.DrawLine(VectorConv(rect.min),VectorConv(rect.min) + heightVector,color);
		Debug.DrawLine(VectorConv(rect.max),VectorConv(rect.max) - widthVector,color);
		Debug.DrawLine(VectorConv(rect.max),VectorConv(rect.max) - heightVector,color);
	}

	private static Vector3 VectorConv(Vector2 v)
	{
		return new Vector3(v.x,0,v.y);
	}
}
