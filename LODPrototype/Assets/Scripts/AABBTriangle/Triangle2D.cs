﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class Triangle2D {

	public Vector2[] pts;

	/// <summary>
	/// Triangle 2D creates a triangle on the XY plane
	/// </summary>
	/// <param name="pts">Pts.</param>
	public Triangle2D(Vector2[] pts)
	{
		Debug.Assert(pts.Length==3,"Triangle must have 3 points!!");
		UpdateGeometry(pts);
	}

	private void GetTriangleRect()
	{

	}

	public Vector2 Apex
	{
		get{
			return pts[0];
		}
	}
	
	public void UpdateGeometry(Vector2[] pts)
	{
		this.pts = pts;
	}

	public bool ContainsPoint(Vector2 p)
	{
		// Compute vectors        
		Vector3 v0 = pts[1] - pts[0];
		Vector3 v1 = pts[2] - pts[0];
		Vector3 v2 = p - pts[0];

		// Compute dot products
		float dot00 = Vector3.Dot(v0, v0);
		float dot01 = Vector3.Dot(v0, v1);
		float dot02 = Vector3.Dot(v0, v2);
		float dot11 = Vector3.Dot(v1, v1);
		float dot12 = Vector3.Dot(v1, v2);
				
		// Compute barycentric coordinates
		float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
		float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
		float v = (dot00 * dot12 - dot01 * dot02) * invDenom;
				
		return (u >= 0) && (v >= 0) && (u + v < 1);
	}

	public bool Overlaps(Rect rect)
	{
		bool result =  AABBTest(rect) || TriangleBoxVertexTest(rect) || EdgeIntersectionTest(rect);
		return result;
	}

	//Fastest
	private bool AABBTest(Rect rect)
	{
		for(int i=0;i<pts.Length;i++)
		{
			if(rect.Contains(pts[i]))
			{
				return true;
			}
		}
		return false;
	}

	private bool TriangleBoxVertexTest(Rect rect)
	{
		return			(ContainsPoint(rect.min)) 
					||	(ContainsPoint(rect.max)) 
					||	(ContainsPoint(rect.min + new Vector2(rect.width,0))) 
					||	(ContainsPoint(rect.max - new Vector2(rect.width,0)));
	}

	//Slowest
	private bool EdgeIntersectionTest(Rect rect)
	{
		Edge2D edge0 = new Edge2D(pts[0],pts[1]);
		Edge2D edge1 = new Edge2D(pts[1],pts[2]);
		Edge2D edge2 = new Edge2D(pts[0],pts[2]);
		
		return edge0.Overlap(rect) || edge1.Overlap(rect) || edge2.Overlap(rect);
//		return edge0.Overlap(rect);
	}

	public bool Overlaps(Circle2D circle)
	{
		float v1x = pts[0].x;
		float v1y = pts[0].y;

		float v2x = pts[1].x;
		float v2y = pts[1].y;

		float v3x = pts[2].x;
		float v3y = pts[2].y;

			
			
		float centrex = circle.position.x;
		float centrey = circle.position.y;

		//TEST 1: Vertex within circle
		float c1x = centrex - v1x;
		float c1y = centrey - v1y;
		float radiusSqr = circle.radius * circle.radius;
		float c1sqr = c1x*c1x + c1y*c1y - radiusSqr;
		
		if (c1sqr <= 0)
		{
			return true;
		}
				
		float c2x = centrex - v2x;
		float c2y = centrey - v2y;
		float c2sqr = c2x*c2x + c2y*c2y - radiusSqr;
				
		if( c2sqr <= 0)
		{
			return true;
		}
						
		float c3x = centrex - v3x;
		float c3y = centrey - v3y;
						
		float c3sqr = c3x*c3x + c3y*c3y - radiusSqr;
			
		if (c3sqr <= 0)
		{
			return true;
		}

		float e1x = v2x - v1x;
		float e1y = v2y - v1y;

		float e2x = v3x - v2x;
		float e2y = v3y - v2y;
				
		float e3x = v1x - v3x;
		float e3y = v1y - v3y;


		float k = c1x*e1x + c1y*e1y;
			
		if( k > 0)
		{
			float len = e1x*e1x + e1y*e1y;
			
			if( k < len)
			{
				if( c1sqr * len <= k*k)
				{
					return true;
				}
			}
		}

		k = c2x*e2x + c2y*e2y;
				
		if( k > 0)
		{
			float len = e2x*e2x + e2y*e2y;
			if( k < len)
			{
				if (c2sqr * len <= k*k)
				{
					return true;
				}
			}
		}

		k = c3x*e3x + c3y*e3y;
				
			if( k > 0)
			{
				float len = e3x*e3x + e3y*e3y;
					
				if( k < len)
				{
					if(c3sqr * len <= k*k)
					{
						return true;
					}
				}
			}
		return false;
	}
}


public class Edge2D {
	public Vector2 p0;
	public Vector2 p1;
	public float gradient;
	public float intercept;

	public Edge2D(Vector2 p0, Vector2 p1)
	{
		this.p0 = p0;
		this.p1 = p1;

		gradient = (p1.y-p0.y)/(p1.x-p0.x);
		intercept =  p0.y - gradient * p0.x;

	}
	
	public bool Overlap(Rect rect)
	{
		if(p1.y-p0.y ==0)
		{
			if(  IsInRange(p1.y,rect.yMax,rect.yMin ))
			{
				return (IsInRange(rect.xMin,p0.x,p1.x) || IsInRange(rect.xMax,p0.x,p1.x));
			}
		}
		
		float yMinBoxX  = rect.xMin * gradient + intercept;
		if(IsInRange(yMinBoxX,p0.y,p1.y) && IsInRange(yMinBoxX,rect.yMin,rect.yMax))
		{
			return true;
		}
		
		float yMaxBoxX  = rect.xMax * gradient + intercept ;
		if(IsInRange(yMaxBoxX,p0.y,p1.y) && IsInRange(yMaxBoxX,rect.yMin,rect.yMax))
		{
			return true;
		}
		
		float xMinBoxY  =  (rect.yMin - intercept) / gradient;
		if(IsInRange(xMinBoxY,p0.x,p1.x) && IsInRange(xMinBoxY,rect.xMin,rect.xMax))
		{
			return true;
		}
		
		float xMaxBoxY  =  (rect.yMax - intercept) / gradient;
		if(IsInRange(xMaxBoxY,p0.x,p1.x) && IsInRange(xMaxBoxY,rect.xMin,rect.xMax))
		{
			return true;
		}

		return false;
	}



	
	private bool IsInRange(float v,float a,float b)
	{
		if(a>b)
		{
			float temp=a;
			a=b;
			b=temp;
		}
		return (v>=a && v<=b);
	}
}
