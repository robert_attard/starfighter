﻿using UnityEngine;
using System.Collections;

namespace VectorExtensions
{
	public static class VectorExtension  {

		public static Vector2 ConvertZtoY(this Vector3 v)
		{
			return new Vector2(v.x,v.z);
		}

		public static Vector3 ConvertYtoZ(this Vector2 v)
		{
			return new Vector3(v.x,0,v.y);
		}

		public static Vector3 Extend(this Vector3 v,float x,float y, float z)
		{
			return new Vector3(v.x +x,v.y + y,v.z +z);
		}

		public static Vector2 Extend(this Vector2 v,float x,float y)
		{
			return new Vector2(v.x +x,v.y + y);
		}

		public static void  DrawTriangle(this Vector2[] v, Color c)
		{
			Debug.Assert(v.Length==3,"Triangle must have three vertices");
			Debug.DrawLine(v[0].ConvertYtoZ(),v[1].ConvertYtoZ(),c);
			Debug.DrawLine(v[0].ConvertYtoZ(),v[2].ConvertYtoZ(),c);
			Debug.DrawLine(v[1].ConvertYtoZ(),v[2].ConvertYtoZ(),c);
		}

		public static void  DrawTriangle(this Vector3[] v, Color c)
		{
			Debug.Assert(v.Length==3,"Triangle must have three vertices");
			Debug.DrawLine(v[0],v[1],c);
			Debug.DrawLine(v[0],v[2],c);
			Debug.DrawLine(v[1],v[2],c);
		}

		public static void DrawSquare(Rect r,Color col)
		{
			Debug.DrawLine(new Vector3(r.x,0,r.y),new Vector3(r.x,0,r.y+r.height),col);
			Debug.DrawLine(new Vector3(r.x,0,r.y),new Vector3(r.x+r.width,0,r.y),col);
			Debug.DrawLine(new Vector3(r.x+r.width,0,r.y),new Vector3(r.x+r.width,0,r.y + r.height),col);
			Debug.DrawLine(new Vector3(r.x,0,r.y + r.height),new Vector3(r.x+r.width,0,r.y + r.height),col);
		}

		public static void DrawCross(Vector2 v, float size, Color col)
		{
			size /=2;
			Debug.DrawLine(new Vector3(v.x-size,0,v.y),new Vector3(v.x+size,0,v.y),col);
			Debug.DrawLine(new Vector3(v.x,0,v.y-size),new Vector3(v.x,0,v.y+size),col);
		}

		public static void DrawCircle(Vector2 v, float radius, Color col,float steps)
		{
			steps = steps - (360f % steps);
			Vector2 nextPt = Vector2.zero;
			Vector2 prevPt = Vector2.zero;
			prevPt = GetPointOnCircle(radius,v,0);
			for(int i=(int)steps;i<=360;i+=(int)steps)
			{
				nextPt = GetPointOnCircle(radius,v,i);
				Debug.DrawLine(prevPt.ConvertYtoZ(),nextPt.ConvertYtoZ(),col);
				prevPt = nextPt;
			}
		}

		private static Vector2 GetPointOnCircle(float r, Vector2 center, float theta)
		{
			float x  =  center.x + r * Mathf.Cos(theta*Mathf.Deg2Rad);
			float y  =  center.y + r * Mathf.Sin(theta*Mathf.Deg2Rad);
			return new Vector2(x,y);
		}
	}
}
