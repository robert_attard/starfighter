using UnityEngine;
using System.Collections;
using VectorExtensions;

public class TriangleGenerator : MonoBehaviour {

	public float angle = 30f;
	public float perpendicularLength = 5f;
	public Vector2[] pts;
	public SquareGenerator square;
	public Circle circle;

	// Use this for initialization
	void Start () {
		pts= new Vector2[3];
	}
	
	// Update is called once per frame
	void Update () {
		CreateTriangle();


	}

	public Transform point;

	private bool ptIntersected = false;

	Vector3 perp;
	private void CreateTriangle()
	{
		pts[0] = transform.position.ConvertZtoY();
		perp = transform.position + (transform.forward*perpendicularLength);

		float angleB = 90f-(angle/2f);

		float a = Mathf.Sin(Mathf.Deg2Rad * angle/2f) *  (perpendicularLength/Mathf.Sin(Mathf.Deg2Rad * angleB));

		Vector3 r = Vector3.Cross(transform.forward,transform.up);
		pts[1] = (perp + r * a).ConvertZtoY();
		pts[2] = (perp - r * a).ConvertZtoY();

		Color c = (ptIntersected)?Color.red:Color.green;

		pts.DrawTriangle(c);

		Triangle2D t = new Triangle2D(pts);
//		ptIntersected = t.Overlaps(square.rect);
		ptIntersected = t.Overlaps(new Circle2D(circle.radius,circle.transform.position.ConvertZtoY()));
	}
	
}
