﻿using UnityEngine; 
using System.Collections; 
using System.Collections.Generic; 

public class QuadTree 
{ 
	private int MAX_OBJECTS = 1; 
	private int MAX_LEVELS = 3; 
	public int level =0;
	public Rect rect;
	public QuadTree parentQuad;
	public QuadTree[] children;
	private QuadTreeLODTerrain manager;
	private LODInfo lodInfo;
	private GameObject lodObject;
	private Vector3 position;

	public QuadTree(int level, QuadTree parentQuad, QuadTreeLODTerrain manager, Vector3 position)
	{
		this.position = position;
		this.level = level;
		this.parentQuad = parentQuad;
		this.manager = manager;
		lodInfo = manager.GetLODInfo(level);

		lodObject = GameObject.Instantiate(lodInfo.lodPrefab,position,Quaternion.identity) as GameObject;
		if(parentQuad!=null)
		{
			lodObject.transform.parent = parentQuad.lodObject.transform;
		}
		MeshRenderer mr = lodObject.GetComponent<MeshRenderer>();
		float colorLevel = ((float)(QuadTreeLODTerrain.lodLevels-level)/(float)QuadTreeLODTerrain.lodLevels);
		mr.material.color = Color.white * colorLevel;
		lodObject.name = string.Format("lod-{3}  ({0},{1} w:{2})",position.x,position.z,lodInfo.width,level);
		
		lodObject.transform.position = position;
		rect = new Rect(position.x,position.z,lodInfo.width,lodInfo.height);
	}

	public void Split(LODView lodView)
	{
		if(level ==QuadTreeLODTerrain.maxLODLevel)
		{
			return;
		}
		int nextLevel;
		if(lodView.CanSplit(rect, out nextLevel))
		{
			if(level >=  nextLevel)
			{
				RemoveChildren();
				return;
			}
			if(children==null)
			{
				lodObject.GetComponent<MeshRenderer>().enabled=false;
				Vector3[] anchors = new Vector3[]{Vector3.zero,new Vector3(0,0,1),new Vector3(1,0,1),new Vector3(1,0,0)};
				int childCount = (lodInfo.split)?4:1;
				children = new QuadTree[childCount];
				for(int i=0;i<childCount;i++)
				{
					Vector3 childPos = position + (anchors[i] * lodInfo.width/2f);
					children[i] = new QuadTree(level + 1,this,manager,childPos);
				}
			}
			for(int i=0;i<children.Length;i++)
			{
				children[i].Split(lodView);
			}
		}
		else
		{
			RemoveChildren();
		}
	}

	private void RemoveChildren()
	{
		if(children!=null)
		{
			lodObject.GetComponent<MeshRenderer>().enabled=true;
			foreach(QuadTree child in children)
			{
				GameObject.Destroy(child.lodObject.gameObject);
			}
			children = null;
		}
	}

}