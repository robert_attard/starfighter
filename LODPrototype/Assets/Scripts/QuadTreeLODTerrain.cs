﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VectorExtensions;

[System.Serializable]
public class LODInfo
{
	public GameObject lodPrefab;
	public int level;
	public bool split;
	public float width;
	public float height;
}

public class LODView
{
	public TriangleView tView;
	public AnimationCurve fallOffCurve;

	public LODView(TriangleView triangleView, AnimationCurve fallOffCurve)
	{
		this.tView = triangleView;
		this.fallOffCurve = fallOffCurve;
	}

	public int GetCurrentLevel(Vector2 position)
	{
		float distance = Vector3.Distance(position,tView.triangle.Apex);
		float perc = distance/tView.Radius;
		float p = fallOffCurve.Evaluate(perc);
//		Debug.Log(string.Format("distance: {0} perc: {1}  p: {2}  tView.Radius: {3} ",distance,perc,p,tView.Radius));

		return Mathf.RoundToInt(Mathf.Lerp(QuadTreeLODTerrain.maxLODLevel,3,p));
	}

	public bool CanSplit(Rect rect, out int lodLevel)
	{
		lodLevel =0;
		bool overlaps = tView.triangle.Overlaps(rect);
		if(overlaps)
		{
			lodLevel = GetCurrentLevel(rect.center);
		}
		return overlaps;
	}
}

public class QuadTreeLODTerrain : MonoBehaviour {

	public const int maxLODLevel = 11;
	public LODInfo[] lods;
	public Dictionary<int,LODInfo> lodDictionary = new Dictionary<int, LODInfo>();
	public QuadTree quadTree;
	public static int lodLevels;

	public AnimationCurve fallOffLODCurve;
	public TriangleView triangleView;
	public Rect selectionRect;

	private LODView lodView;

	void Start()
	{
		lodLevels = lods.Length;
		foreach(LODInfo lInfo in lods)
		{
			MeshFilter mf = lInfo.lodPrefab.GetComponent<MeshFilter>();
			Bounds b = mf.sharedMesh.bounds;
			lInfo.width = b.size.x;
			lInfo.height = b.size.z;
			lodDictionary.Add(lInfo.level,lInfo);
		}
		quadTree = new QuadTree(0,null,this,Vector3.zero);
		lodView = new LODView(triangleView,fallOffLODCurve);
		Vector3 r = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		triangleView.UpdateView();
		quadTree.Split(lodView);
	}
	
	private void Update()
	{
		triangleView.UpdateView();
		quadTree.Split(lodView);
	}

	Vector3 ConvertToZ(Vector2 a)
	{
		return new Vector3(a.x,0,a.y);
	}

	void OnDrawGizmos()
	{
		Vector3 r = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		selectionRect.center = new Vector2(r.x,r.z);
		Gizmos.color = Color.magenta;
		Gizmos.DrawWireCube(ConvertToZ( selectionRect.center),new Vector3(selectionRect.width,2,selectionRect.height));
	}

	public LODInfo GetLODInfo(int level)
	{
		return lodDictionary[level];
	}
}
