﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class TriangleView : MonoBehaviour {

	public float angle = 30f;
	public float perpendicularLength = 5f;
	public Vector2[] pts;
	[HideInInspector]
	public Triangle2D triangle;

	public float Radius
	{
		get{
			return Mathf.Sqrt(  Mathf.Pow(perpendicularLength,2f) + Mathf.Pow(halfOppositeSide,2f));
		}
	}

	// Use this for initialization
	void Awake () {
		pts= new Vector2[3];
		triangle = new Triangle2D(pts);
		CreateTriangle();
		Debug.Log(Radius);
	}

	// Update is called once per frame
	public void UpdateView () {
		CreateTriangle();
	}

	float halfOppositeSide;
	private void CreateTriangle()
	{
		pts[0] = transform.position.ConvertZtoY();
		Vector3 perp = transform.position + (transform.forward*perpendicularLength);
		
		float angleB = 90f-(angle/2f);
		
		halfOppositeSide = Mathf.Sin(Mathf.Deg2Rad * angle/2f) *  (perpendicularLength/Mathf.Sin(Mathf.Deg2Rad * angleB));
		
		Vector3 r = Vector3.Cross(transform.forward,transform.up);
		pts[1] = (perp + r * halfOppositeSide).ConvertZtoY();
		pts[2] = (perp - r * halfOppositeSide).ConvertZtoY();

		triangle.UpdateGeometry(pts);
		pts.DrawTriangle(Color.green);
	}

	void OnDrawGizmos()
	{
		if(pts!=null && pts.Length==3)
		{
			Gizmos.color = new Color(1f,0,0,0.3f);
			Gizmos.DrawSphere(pts[0].ConvertYtoZ(),0.3f);
		}
	}
}
