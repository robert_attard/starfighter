﻿using UnityEngine;
using System.Collections;

public class RectOverlapTest : MonoBehaviour {

	public Rect selectionRect;
	public Rect testRect;
	private bool createdRect = false;

	public Vector2 sCenter;
	public Vector2 tCenter;
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0) )
		{
			if(!createdRect)
			{
				testRect = selectionRect;
				createdRect = true;
			}
			else
			{
				Debug.Log(selectionRect.Overlaps(testRect));
			}
		}

		sCenter = selectionRect.position;
		tCenter = testRect.position;
	}

	public Vector3 r;
	void OnDrawGizmos()
	{
		r = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		//r.y =0;
		selectionRect.center = new Vector2(r.x,r.z);
		Gizmos.color = Color.magenta;
		Gizmos.DrawWireCube(ConvertToZ( selectionRect.center),new Vector3(selectionRect.width,2,selectionRect.height));


		if(createdRect)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireCube(ConvertToZ(testRect.center),new Vector3(testRect.width,2,testRect.height));
		}
	}

	Vector3 ConvertToZ(Vector2 a)
	{
		return new Vector3(a.x,0,a.y);
	}
}
