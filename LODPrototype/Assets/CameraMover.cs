﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

	private Vector3 targetDir;

	// Use this for initialization
	void Start () {
		GetNewTarget();
		StartCoroutine(MotionCoroutine());
	}
	
	// Update is called once per frame
	void Update () {
	
		Quaternion rot = Quaternion.LookRotation(targetDir);
		transform.rotation = Quaternion.Slerp(transform.rotation,rot,0.5f * Time.deltaTime);
		transform.position += transform.forward * 100f * Time.deltaTime;
	}

	IEnumerator MotionCoroutine()
	{
		while(true)
		{
			yield return new WaitForSeconds(Random.Range(3f,4f));
			GetNewTarget();
		}
	}

	void GetNewTarget()
	{
		targetDir = Quaternion.Euler(0,Random.Range(-180f,180f),0) * transform.forward;
	}

}
