﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using VectorExtensions;

public class RoamDiamond
{
	public TriTree top;
	public TriTree bottom;
	public float lodLevel;
	public RoamDiamond parent;

	public bool isMergeable = true;
	public Vector2 centroid;

	
	public RoamDiamond(TriTree top, TriTree bottom, float lodLevel)
	{
		this.top = top;
		this.bottom = bottom;
		this.lodLevel = lodLevel;
		int x = (top.leftVertex.x + top.rightVertex.x)/2;
		int y = (top.leftVertex.y + top.rightVertex.y)/2;
		
		centroid = new Vector2((float)x,(float)y);
	}

	public void Draw(Color color)
	{
		Debug.DrawLine(top.apexVertex.vector3,top.leftVertex.vector3,color);
		Debug.DrawLine(top.apexVertex.vector3,top.rightVertex.vector3,color);

		Debug.DrawLine(bottom.apexVertex.vector3,bottom.leftVertex.vector3,color);
		Debug.DrawLine(bottom.apexVertex.vector3,bottom.rightVertex.vector3,color);
	}

}


public class RoamDiamondManager
{
	public int diamondCount;
	public List<RoamDiamond>[] diamondLODList ;
	public int maxLOD;
	public bool block = false;

	public static RoamDiamondManager instance;

	public RoamDiamondManager(int maxLOD)
	{
		instance = this;
		diamondCount=0;
		this.maxLOD = maxLOD;
		diamondLODList = new List<RoamDiamond>[maxLOD];
		for(int i=0;i<maxLOD;i++)
		{
			diamondLODList[i] = new List<RoamDiamond>();
		}
	}

	public void RegisterDiamond(RoamDiamond diamond)
	{
		ROAMRender.diamondCount++;
		diamondLODList[(int)diamond.lodLevel].Add(diamond);
	}
	public void UnRegisterDiamond(RoamDiamond diamond)
	{
		ROAMRender.diamondCount--;
		diamondLODList[(int)diamond.lodLevel].Remove(diamond);
	}
	
	public void RemoveOutOfViewDiamonds(Triangle2D viewTriangle)
	{
		List<RoamDiamond> trashedDiamonds = new List<RoamDiamond>();
		for(int i = maxLOD-1;i>=0;i--)
		{
			foreach(RoamDiamond d in diamondLODList[i])
			{	
				if(!viewTriangle.ContainsPoint(d.centroid))
				{
					d.top.Merge();
					d.bottom.Merge();
					trashedDiamonds.Add(d);
				}
			}
		}
		
		foreach(RoamDiamond d in trashedDiamonds)
		{
			UnRegisterDiamond(d);
		}
	}


	public IEnumerator RemoveOutOfViewDiamondsCoroutine(Triangle2D viewTriangle, Action update )
	{
		List<RoamDiamond> trashedDiamonds = new List<RoamDiamond>();
		for(int i = maxLOD-1;i>=0;i--)
		{
			foreach(RoamDiamond d in diamondLODList[i])
			{

				float k = Time.time +0.2f;
				while(Time.time<k)
				{
					d.Draw(Color.green);
					VectorExtension.DrawCross(d.centroid,100f,Color.green);
					yield return null;
				}
				
				if(!viewTriangle.ContainsPoint(d.centroid))
				{
					d.top.Merge();
					d.bottom.Merge();
					trashedDiamonds.Add(d);
				}

				yield return new WaitForSeconds(0.1f);
				if(update!=null)
				{
					update();
				}
			}
		}
		
		foreach(RoamDiamond d in trashedDiamonds)
		{
			UnRegisterDiamond(d);
		}
	}
}


