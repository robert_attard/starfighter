﻿using UnityEngine;
using System.Collections;
using VectorExtensions;
using System.Collections.Generic;

[SerializeField]
public class TriTree 
{
	public ROAMPatch patch;
	public TriTree LeftChild;
	public TriTree RightChild;
	public RoamDiamond diamond;


	public TriTree BaseNeighbor;
	public TriTree LeftNeighbor;
	public TriTree RightNeighbor;
	public TriTree parent;

	public RoamVertex apexVertex;
	public RoamVertex leftVertex;
	public RoamVertex rightVertex;


	public RoamVertex centerVertex; //splitting vertex
	public RoamVertex BarycentricPoint; //center of triangle
	public int lod;
	public Vector3 normal;
	public Rect rect;
	public bool isSplit = false;
	private float error;


	public TriTree(ROAMPatch patch, RoamVertex apexVertex,RoamVertex leftVertex, RoamVertex rightVertex,int lod)
	{
		this.lod = lod;
		this.patch = patch;
		this.apexVertex = apexVertex;
		this.leftVertex = leftVertex;
		this.rightVertex = rightVertex;
		BarycentricPoint = new RoamVertex( (apexVertex.x + leftVertex.x + rightVertex.x)/3
		                                  ,(apexVertex.y + leftVertex.y + rightVertex.y)/3,0);

		normal = Vector3.Cross((rightVertex.vector3-apexVertex.vector3).normalized,(leftVertex.vector3-apexVertex.vector3).normalized);

		float xMax = Mathf.Max(Mathf.Max(apexVertex.x,leftVertex.x), rightVertex.x);
		float xMin = Mathf.Min(Mathf.Min(apexVertex.x,leftVertex.x), rightVertex.x);
		
		float yMax = Mathf.Max(Mathf.Max(apexVertex.y,leftVertex.y), rightVertex.y);
		float yMin = Mathf.Min(Mathf.Min(apexVertex.y,leftVertex.y), rightVertex.y);
		
		rect = new Rect(xMin,yMin,xMax-xMin,yMax-yMin);

		Vector2 cV = new Vector2((leftVertex.x + rightVertex.x)/2f,(leftVertex.y + rightVertex.y)/2f);
		float avgHeight = (leftVertex.height+rightVertex.height)/2f;
		error = Mathf.Abs(patch.terrainCharacteristic.Generate(cV.x,cV.y)-avgHeight);
	}
	

	public void SubmitForRendering()
	{
		if(!isSplit)
		{
			patch.AddTriangleForRendering(this);
		}
		else
		{
			LeftChild.SubmitForRendering();
			RightChild.SubmitForRendering();
		}
	}

	public void Split()
	{
		Split(null);
	}

	public void Reset()
	{
		LeftChild = null;
		RightChild = null;
		LeftNeighbor = null;
		RightNeighbor = null;
		BaseNeighbor = null;
		isSplit = false;
	}


	public void Split(RoamVertex centerVertex)
	{
		if(lod>=patch.maxLOD)
		{
			return;
		}

		if(isSplit)
		{
			return;
		}

		centerVertex = patch.GetCenterVertex(this);
		
		if ( (BaseNeighbor!=null) && (BaseNeighbor.BaseNeighbor != this) )
		{
			BaseNeighbor.Split(centerVertex);
		}

		if(diamond == null && BaseNeighbor!=null)
		{
				diamond = new RoamDiamond(this,BaseNeighbor,lod);
				BaseNeighbor.diamond = diamond;
//				RoamDiamondManager.instance.RegisterDiamond(diamond);
		}

		// Create children and link into mesh
		LeftChild  = new TriTree(patch,centerVertex,apexVertex,leftVertex,lod+1);
		RightChild = new TriTree(patch,centerVertex,rightVertex,apexVertex,lod+1);
		LeftChild.parent = this;
		RightChild.parent = this;

		isSplit = true;

		// Fill in the information we can get from the parent (neighbor pointers)
		LeftChild.BaseNeighbor  = LeftNeighbor;
		LeftChild.LeftNeighbor  = RightChild;
		
		RightChild.BaseNeighbor  = RightNeighbor;
		RightChild.RightNeighbor = LeftChild;


		// Link our Left Neighbor to the new children
		if (LeftNeighbor != null)
		{
			if (LeftNeighbor.BaseNeighbor == this)
			{
				LeftNeighbor.BaseNeighbor = LeftChild;
			}
			else if (LeftNeighbor.LeftNeighbor == this)
			{
				LeftNeighbor.LeftNeighbor = LeftChild;
			}
			else if (LeftNeighbor.RightNeighbor == this)
			{
				LeftNeighbor.RightNeighbor = LeftChild;
			}
			else
			{

				Debug.LogError("Illegal Left Neighbor!");
			}
		}
		
		// Link our Right Neighbor to the new children
		if (RightNeighbor != null)
		{
			if (RightNeighbor.BaseNeighbor == this)
			{
				RightNeighbor.BaseNeighbor = RightChild;
			}
			else if (RightNeighbor.RightNeighbor == this)
			{
				RightNeighbor.RightNeighbor = RightChild;
			}
			else if (RightNeighbor.LeftNeighbor == this)
			{
				RightNeighbor.LeftNeighbor = RightChild;
			}
			else
			{
				Debug.LogError("Illegal Right Neighbor!");
			}
		}
		
		if (BaseNeighbor != null)
		{
			if ( BaseNeighbor.LeftChild!=null)
			{
				BaseNeighbor.LeftChild.RightNeighbor = RightChild;
				BaseNeighbor.RightChild.LeftNeighbor = LeftChild;
				LeftChild.RightNeighbor = BaseNeighbor.RightChild;
				RightChild.LeftNeighbor = BaseNeighbor.LeftChild;
			}
			else
			{
				BaseNeighbor.Split(centerVertex);// Base Neighbor (in a diamond with us) was not split yet, so do that now.
			}
		}
		else
		{
			LeftChild.RightNeighbor = null;
			RightChild.LeftNeighbor = null;
		}
	}

	private bool isHypotenusePatchEdge
	{
		get{
			return (centerVertex.x == 0) || (centerVertex.x == patch.mapSize) || (centerVertex.y == 0) || (centerVertex.y == patch.mapSize);
		}
	}

	public void DebugDraw()
	{
		if(!isSplit)
		{
			DrawTri(this,new Color(1f,1f,1f,0.2f));
		}
		else
		{
			LeftChild.DebugDraw();
			RightChild.DebugDraw();
		}
	}

	public void HighLightTri()
	{
		if(patch.viewTriangle.Overlaps(rect))
		{
			if(!isSplit)
			{
				patch.lodTarget = GetTargetLOD();
				patch.currentLOD = lod;

				VectorExtension.DrawSquare(rect,Color.blue);
				DrawTri(this,Color.cyan);
			}
			else
			{
				LeftChild.HighLightTri();
				RightChild.HighLightTri();
			}
		}
	}
	

	public void RecursiveSplit()
	{
		float targetLOD = GetTargetLOD();
		if (lod<targetLOD)
		{
			if(!isSplit)
			{
				Split();
			}
			LeftChild.RecursiveSplit();
			RightChild.RecursiveSplit();
		}
	}

	public int GetTargetLOD()
	{
		if(lod>=patch.maxLOD)
		{
			return (int)Mathf.Clamp((error * patch.errorTolerance) * patch.maxLOD,0,patch.maxLOD);
		}

		if(patch.viewTriangle.Overlaps(rect))
		{
			float distance = Vector2.Distance(BarycentricPoint.vector2,patch.viewTriangle.Apex);
			for (int i = 0; i < patch.lodRules.Length; i++) {
				LODRule r = patch.lodRules [i];
				if (distance < r.minDistance) {
					if(r.lod>=patch.maxLOD)
					{
						return  (int)Mathf.Clamp((error * patch.errorTolerance) * r.lod,0,patch.maxLOD);
					}
					return r.lod;
				}
			}
			return 0;
		}
		else
		{

			return 0;
		}
	}

	public static void DrawTri(TriTree t,Color color)
	{
		Debug.DrawLine(t.apexVertex.vector3,t.rightVertex.vector3,color);
		Debug.DrawLine(t.apexVertex.vector3,t.leftVertex.vector3,color);
		Debug.DrawLine(t.leftVertex.vector3,t.rightVertex.vector3,color);
	}


	public void Merge()
	{
		if(isSplit)
		{
			LeftNeighbor = LeftChild.BaseNeighbor;
			RightNeighbor = RightChild.BaseNeighbor;
			
			if(LeftNeighbor!=null && LeftNeighbor.LeftChild!=null)
			{
				LeftNeighbor.Merge();
			}
			
			if(RightNeighbor!=null && RightNeighbor.LeftChild!=null)
			{
				RightNeighbor.Merge();
			}
			
			if(LeftNeighbor!=null) 
			{
				if(LeftNeighbor.RightNeighbor==LeftChild)
				{
					LeftNeighbor.RightNeighbor = this;
				}
				
				if(LeftNeighbor.BaseNeighbor ==LeftChild)
				{
					LeftNeighbor.BaseNeighbor = this;
				}
			}
			
			if(RightNeighbor!=null)  
			{
				if(RightNeighbor.LeftNeighbor==RightChild)
				{
					RightNeighbor.LeftNeighbor = this;
				}
				if(RightNeighbor.BaseNeighbor ==RightChild)
				{
					RightNeighbor.BaseNeighbor = this;
				}
			}
			
			LeftChild = null;
			RightChild = null;
			isSplit = false;
		}
	}

	public bool ContainsPoint(Vector2 p)
	{
		// Compute vectors        
		Vector2 v0 = leftVertex.vector2 - apexVertex.vector2;
		Vector2 v1 = rightVertex.vector2 - apexVertex.vector2;
		Vector2 v2 = p - apexVertex.vector2;
		
		// Compute dot products
		float dot00 = Vector3.Dot(v0, v0);
		float dot01 = Vector3.Dot(v0, v1);
		float dot02 = Vector3.Dot(v0, v2);
		float dot11 = Vector3.Dot(v1, v1);
		float dot12 = Vector3.Dot(v1, v2);
		
		// Compute barycentric coordinates
		float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
		float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
		float v = (dot00 * dot12 - dot01 * dot02) * invDenom;
		
		// Check if point is in triangle
		return (u >= 0) && (v >= 0) && (u + v < 1);
	}
	
	
	public void HighLightTri(Vector2 p)
	{
		if(ContainsPoint(p))
		{
			if(!isSplit)
			{
				patch.lodTarget = GetTargetLOD();
				patch.currentLOD = lod;
				patch.currentError = error;
				
				DrawTri(this,Color.cyan);
				
				if(LeftNeighbor!=null)
				{
					DrawTri(LeftNeighbor,Color.red);
				}
				if(RightNeighbor!=null)
				{
					DrawTri(RightNeighbor,Color.green);
				}
				if(BaseNeighbor!=null)
				{
					DrawTri(BaseNeighbor,Color.blue);
				}
			}
			else
			{
				LeftChild.HighLightTri(p);
				RightChild.HighLightTri(p);
			}
		}
	}
}




public class RoamVertex
{
	public int x;
	public int y;
	public float height;

	public RoamVertex(int x,int y,float height)
	{
		this.x= x;
		this.y =y;
		this.height = height;
	}

	public Vector2 vector2
	{
		get{
			return new Vector2((float)x,(float)y);
		}
	}

	public Vector3 vector3
	{
		get{
			return new Vector3((float)x,height,(float)y);
		}
	}

	public override string ToString ()
	{
		return string.Format ("[{0}:{1}]",x,y);
	}
}






