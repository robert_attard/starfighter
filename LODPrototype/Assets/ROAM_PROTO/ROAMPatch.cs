using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VectorExtensions;
using System;
using ThreadPooling;


public class ROAMPatch {

	private Dictionary<Vector2,RoamVertex> cachedVertices = new Dictionary<Vector2, RoamVertex>();

	public int maxLOD;
	public int mapSize;
	public float errorTolerance = 1f;
	public float maxLODDistanceCheck = 15;

	private TriTree leftTri;
	private TriTree rightTri;


	public float lodTarget =0;
	public float currentLOD =0;
	public float currentError =0;

	RoamVertex bottomLeft;
	RoamVertex topLeft;
	RoamVertex bottomRight;
	RoamVertex topRight;

	public List<Vector3> meshVertices = new List<Vector3>();
	public List<int> meshTriangles = new List<int>();
	public List<Vector3> meshNormals = new List<Vector3>();

	public Triangle2D viewTriangle;
	public Triangle2D renderTriangle;
	public TerrainCharacteristic terrainCharacteristic;
	public RoamDiamondManager diamondManager;
	public LODRule[] lodRules;


	public ROAMPatch(int maxLOD, int mapSize,float errorTolerance,float maxLODDistanceCheck,  TerrainCharacteristic terrainCharacteristic, LODRule[] lodRules)
	{
		this.lodRules = lodRules;
		this.errorTolerance = errorTolerance;
		this.maxLODDistanceCheck = maxLODDistanceCheck;
		this.terrainCharacteristic = terrainCharacteristic;
		this.maxLOD = maxLOD;
		this.mapSize = mapSize;

		this.terrainCharacteristic.Init(1);
		diamondManager = new RoamDiamondManager(maxLOD);
	}

	public void CreateInitialTriangles()
	{
		bottomLeft = new RoamVertex(0,0,0);
		topLeft = new RoamVertex(0,mapSize,0);
		bottomRight = new RoamVertex(mapSize,0,0);
		topRight = new RoamVertex(mapSize,mapSize,0);
		
		cachedVertices.Add(bottomLeft.vector2, bottomLeft);
		cachedVertices.Add(topLeft.vector2, topLeft);
		cachedVertices.Add(bottomRight.vector2, bottomRight);
		cachedVertices.Add(topRight.vector2, topRight);
		
		
		TriTree t = new TriTree(this,bottomLeft,bottomLeft,bottomLeft,0);
		
		leftTri = new TriTree(this,topLeft,topRight,bottomLeft,0);
		rightTri = new TriTree(this,bottomRight,bottomLeft,topRight,0);
		leftTri.BaseNeighbor = rightTri;
		rightTri.BaseNeighbor = leftTri;
		
		RoamDiamond diamond = new RoamDiamond(leftTri,rightTri,0);
		leftTri.diamond = diamond;
		rightTri.diamond = diamond;
		diamondManager.RegisterDiamond(diamond);
	}


	public void Process(Triangle2D viewTriangle, Triangle2D renderTriangle)
	{

		this.viewTriangle = viewTriangle;
		this.renderTriangle = renderTriangle;

		leftTri.Reset();
		rightTri.Reset();
		leftTri.BaseNeighbor = rightTri;
		rightTri.BaseNeighbor = leftTri;

		Tesselate();
		CreateMesh();
	}

	public void Tesselate()
	{
		leftTri.RecursiveSplit();
		rightTri.RecursiveSplit();
	}

	public void CreateMesh()
	{
		meshVertices.Clear();
		meshTriangles.Clear();
		meshNormals.Clear();
		leftTri.SubmitForRendering();
		rightTri.SubmitForRendering();
	}

	public void AddTriangleForRendering(TriTree tri)
	{
		meshVertices.Add(tri.apexVertex.vector3);
		meshVertices.Add(tri.leftVertex.vector3);
		meshVertices.Add(tri.rightVertex.vector3);

		meshNormals.Add(tri.normal);
		meshNormals.Add(tri.normal);
		meshNormals.Add(tri.normal);

		meshTriangles.Add(meshVertices.Count-3);
		meshTriangles.Add(meshVertices.Count-2);
		meshTriangles.Add(meshVertices.Count-1);
	}

	public RoamVertex GetCenterVertex(TriTree tri)
	{
		int x = (tri.leftVertex.x + tri.rightVertex.x)/2;
		int y = (tri.leftVertex.y + tri.rightVertex.y)/2;
		
		Vector2 cV = new Vector2((float)x,(float)y);
		RoamVertex centerVertex = null;


//		if(cachedVertices.ContainsKey(cV))
//		{
//			centerVertex = cachedVertices[cV];
//		}
//		else
//		{
//			centerVertex = new RoamVertex(x,y,terrainCharacteristic.Generate(cV.x,cV.y));
//			cachedVertices.Add(centerVertex.vector2,centerVertex);
//		}

		centerVertex = new RoamVertex(x,y,terrainCharacteristic.Generate(cV.x,cV.y));
		
		return centerVertex;
	}

	public override string ToString ()
	{
		return string.Format ("[ROAMPatch] vertices:{0}  triangles:{1}  normals:{2}",meshVertices.Count,(meshTriangles.Count/3f).ToString(),meshNormals.Count);
	}

}

//		leftTri.HighLightTri();
//		rightTri.HighLightTri();



//
//		Rect r = new Rect(Vector2.zero,Vector2.one * 50f);
//		r.center = c;
//		if(viewTriangle.Overlaps(r))
//		{
//			DrawSquare(r,Color.red);
//		}
//		else
//		{
//			DrawSquare(r,Color.black);
//		}
