﻿using UnityEngine;
using System.Collections;

public class TreeNode {
	public TreeNode LeftChild;
	public TreeNode RightChild;
	public TreeDiamond diamond;
	
	public TreeNode BaseNeighbor;
	public TreeNode LeftNeighbor;
	public TreeNode RightNeighbor;
	
	public PVertex apexVertex;
	public PVertex leftVertex;
	public PVertex rightVertex;
}

public class TreeDiamond
{
	public TreeNode TopChild;
	public TreeNode BottomChild;
}
