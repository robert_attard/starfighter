﻿using UnityEngine;
using System.Collections;

public class PVertex {

	public int x;
	public int y;
	public int height;
	
	public PVertex(int x,int y,int height)
	{
		this.x= x;
		this.y =y;
		this.height = height;
	}
	
	public Vector2 vector2
	{
		get{
			return new Vector2((float)x,(float)y);
		}
	}
	
	public Vector3 vector3
	{
		get{
			return new Vector3((float)x,(float)height,(float)y);
		}
	}
	
	public override string ToString ()
	{
		return string.Format ("[{0}:{1}]",x,y);
	}

	public static PVertex operator +(PVertex a, PVertex b)
	{
		return new PVertex(a.x+b.x,a.y + b.y,a.height+b.height);
	}

	public static PVertex operator -(PVertex a, PVertex b)
	{
		return new PVertex(a.x-b.x,a.y - b.y,a.height-b.height);
	}



//	public static ushort Add(ushort x, ushort y)
//	{
//		while (x != 0)
//		{
//			ushort c = y & x;
//			y = y ^ x; 
//			x = c << 1;
//		}
//		return y;
//	}
}
