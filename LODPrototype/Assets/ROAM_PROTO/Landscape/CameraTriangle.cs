﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class CameraTriangle : MonoBehaviour {

	public float farClip = 100f;
	public Camera camera;
	public float scaleOffset =1f;
	

	public void UpdateCameraTriangle(out Vector2 apex,out Vector2 left, out Vector2 right)
	{
		apex = Vector2.zero;
		left = Vector2.zero;
		right = Vector2.zero;

		float diff = (farClip * scaleOffset - farClip)/2f;
		
		Vector3[] Pts = new Vector3[3];
		Pts[0] = camera.transform.position - (camera.transform.forward * diff);
		
		float farClipLength = farClip * scaleOffset;
		float vFOVrad = Camera.main.fieldOfView * Mathf.Deg2Rad;
		float cameraHeightAt1 = Mathf.Tan(vFOVrad *0.5f);
		float hFOVrad = Mathf.Atan(cameraHeightAt1 * Camera.main.aspect) * 2f;
		float hFOV = (hFOVrad * Mathf.Rad2Deg)/2f;
		float otherAngle = 180f-hFOV-90f;
		
		float a  = (farClipLength/Mathf.Sin(Mathf.Deg2Rad*otherAngle))* Mathf.Sin(Mathf.Deg2Rad*hFOV);
		
		Pts[1] = Pts[0]+ (camera.transform.forward*farClipLength) + (-camera.transform.right * a);
		Pts[2] = Pts[0]+ (camera.transform.forward*farClipLength) + (camera.transform.right * a);
		
		Vector2[] pts2 = new Vector2[3];
		apex = Pts[0].ConvertZtoY();
		left = Pts[1].ConvertZtoY();
		right = Pts[2].ConvertZtoY();
	}

	private void OnDrawGizmos()
	{
//		Debug.DrawLine(apex.ConvertYtoZ(),left.ConvertYtoZ(),Color.red);
//		Debug.DrawLine(apex.ConvertYtoZ(),right.ConvertYtoZ(),Color.blue);
//		Debug.DrawLine(left.ConvertYtoZ(),right.ConvertYtoZ(),Color.white);
	}
}
