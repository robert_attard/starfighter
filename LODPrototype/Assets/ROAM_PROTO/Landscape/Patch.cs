﻿using UnityEngine;
using System.Collections;

public class Patch {


	public Vector2 worldOffset;
	public TriTree leftTree;
	public TriTree rightTree;
	public int lodLevel;


	public Patch(Vector2 worldOffset)
	{
		this.worldOffset = worldOffset;
	}

	public void Tesselate()
	{
	}

	public void BuildMesh()
	{
	}

	public void Render()
	{
	}
}
