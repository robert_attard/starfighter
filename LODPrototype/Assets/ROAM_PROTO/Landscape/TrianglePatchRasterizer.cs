﻿using UnityEngine;
using System.Collections;
using VectorExtensions;

public class TrianglePatchRasterizer : MonoBehaviour {

	public CameraTriangle cameraTriangle;
	public int pixelSize =1;
	public Vector2 p0;
	public Vector2 p1;
	public Vector2 p2;

	public int pCount =0;

	// Update is called once per frame
	void LateUpdate () {
		cameraTriangle.UpdateCameraTriangle(out p0,out p1,out p2);
		DrawTriangle(p0,p1,p2);
	}

	public void DrawPoint(Vector2 lowerLeftAnchor)
	{
		lowerLeftAnchor = new Vector2(lowerLeftAnchor.x - (pixelSize/2f),lowerLeftAnchor.y - (pixelSize/2f));

		Vector3 topRight = lowerLeftAnchor.ConvertYtoZ().Extend(pixelSize,0,pixelSize);
		
		Debug.DrawLine(lowerLeftAnchor.ConvertYtoZ()
		               ,lowerLeftAnchor.ConvertYtoZ().Extend(pixelSize,0,0),Color.magenta);
		Debug.DrawLine(lowerLeftAnchor.ConvertYtoZ()
		               ,lowerLeftAnchor.ConvertYtoZ().Extend(0,0, pixelSize),Color.magenta);
		
		
		Debug.DrawLine(topRight
		               ,topRight.Extend(-pixelSize,0,0),Color.magenta);
		Debug.DrawLine(topRight
		               ,topRight.Extend(0,0,-pixelSize),Color.magenta);

		pCount++;
	}

	float Clamp(float value, float min = 0, float max = 1)
	{
		return Mathf.Max(min, Mathf.Min(value, max));
	}

	float Interpolate(float min, float max, float gradient)
	{
		return min + (max - min) * Clamp(gradient);
	}



	public float dP1P2, dP1P3;

	public void DrawTriangle(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		pCount =0;
		p1 = ClampToGrid(p1);
		p2 = ClampToGrid(p2);
		p3 = ClampToGrid(p3);

		if (p1.y > p2.y)
		{
			var temp = p2;
			p2 = p1;
			p1 = temp;
		}
		
		if (p2.y > p3.y)
		{
			var temp = p2;
			p2 = p3;
			p3 = temp;
		}
		
		if (p1.y > p2.y)
		{
			var temp = p2;
			p2 = p1;
			p1 = temp;
		}

		DrawCross(p1,2,Color.red);
		DrawCross(p2,2,Color.green);
		DrawCross(p3,2,Color.blue);
		

		if (p2.y - p1.y > 0)
		{
			dP1P2 = (p2.x - p1.x) / (p2.y - p1.y);
		}
		else
		{
			dP1P2 = 0;
		}
		
		if (p3.y - p1.y > 0)
		{
			dP1P3 = (p3.x - p1.x) / (p3.y - p1.y);
		}
		else
		{
			dP1P3 = 0;
		}

		int pt1Y = (int)p1.y;
		int pt2Y = (int)p2.y;
		int pt3Y = (int)p3.y;
		

		if (dP1P2 > dP1P3)
		{
			for (var y = pt1Y; y <= pt3Y; y+=pixelSize)
			{
				if (y <= p2.y)
				{
					ProcessScanLine( y, p1, p3, p1, p2);
				}
				else
				{
					ProcessScanLine(y, p1, p3, p2, p3);
				}
			}
		}
		else
		{
			for (var y = pt1Y; y <= pt3Y; y+=pixelSize)
			{
				if (y <= p2.y)
				{
					ProcessScanLine(y, p1, p2, p1, p3);
				}
				else
				{
					ProcessScanLine(y, p2, p3, p1, p3);
				}
			}
		}

	}

	void ProcessScanLine(int y, Vector3 pa, Vector3 pb, Vector3 pc, Vector3 pd)
	{
		var gradient1 = (pa.y != pb.y) ? (y - pa.y) / (pb.y - pa.y) : 1f;
		var gradient2 = (pc.y != pd.y) ? (y - pc.y) / (pd.y - pc.y) : 1f;

		float sxf = Interpolate(pa.x, pb.x, gradient1);
		float exf = Interpolate(pc.x, pd.x, gradient2);
		int sx = (int)  ClampToGrid( sxf);
		int ex = (int)  ClampToGrid(exf );

		


		if(sx>ex)
		{
			int temp = ex;
			ex = sx;
			sx = temp;
		}

		DrawCross(new Vector2((float)sx-5f,(float)y),10,Color.red);
		DrawCross(new Vector2((float)ex,(float)y),10,Color.black);
		
		// drawing a line from left (sx) to right (ex) 
		for (var x = sx; x <= ex; x+=pixelSize)
		{
			DrawPoint(new Vector2(x, y));
		}
	}

	private Vector2 ClampToGrid(Vector2 v)
	{
		return new Vector2(ClampToGrid(v.x),ClampToGrid(v.y));
	}

	private float ClampToGrid(float a)
	{
		float ratio = a/(float)pixelSize;
		float round =  Mathf.Round(ratio);
		float result =  (round * pixelSize);
		
		return result;
	}

	private void DrawCross(Vector2 v,float length,Color color)
	{
		Vector3 p = v.ConvertYtoZ();
		Debug.DrawLine(p.Extend(-length,0,0),p.Extend(length,0,0),color);
		Debug.DrawLine(p.Extend(0,0,-length),p.Extend(0,0,length),color);
	}
}
