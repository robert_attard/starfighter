﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VectorExtensions;


[System.Serializable]
public class Edge
{
	public float X1;
	public float Y1;
	public float X2;
	public float Y2;
	
	private float gradient;
	public float Gradient{
		get{
			return gradient;
		}
		private set{
			gradient=value;
		}
	}
	
	private float intercept;
	public float Intercept{
		get{
			return intercept;
		}
		private set{
			intercept=value;
		}
	}
	
	public Edge(Vector2 v1, Vector2 v2)
	{
		X1 = v1.x;
		Y1 = v1.y;
		X2 = v2.x;
		Y2 = v2.y;
		
		Gradient =     ((X2-X1)==0)?0:(Y2-Y1)/(X2-X1);
		Intercept = Y1 - (Gradient*X1);
	}
}


public class Rasterizer : MonoBehaviour {
	
	public int pixelSize = 20;
	private Edge[] edges;
	public Transform p0;
	public Transform p1;
	public Transform p2;

	private void SetPixel(float x, float y)
	{
		Vector2 lowerLeftAnchor = new Vector2(x-(pixelSize/2f),y-(pixelSize/2f));

//		Vector2 lowerLeftAnchor = new Vector2(x,y);
		
		Vector3 topRight = lowerLeftAnchor.ConvertYtoZ().Extend(pixelSize,0,pixelSize);
		
		Debug.DrawLine(lowerLeftAnchor.ConvertYtoZ()
		               ,lowerLeftAnchor.ConvertYtoZ().Extend(pixelSize,0,0),Color.magenta);
		Debug.DrawLine(lowerLeftAnchor.ConvertYtoZ()
		               ,lowerLeftAnchor.ConvertYtoZ().Extend(0,0, pixelSize),Color.magenta);
		
		
		Debug.DrawLine(topRight
		               ,topRight.Extend(-pixelSize,0,0),Color.magenta);
		Debug.DrawLine(topRight
		               ,topRight.Extend(0,0,-pixelSize),Color.magenta);

	}

	private void Update()
	{
		DrawTriangle(p0.position.x,p0.position.z,
		             p1.position.x,p1.position.z,
		             p2.position.x,p2.position.z);
	}


	public void DrawTriangle(float x1, float y1,float x2, float y2,float x3, float y3)
	{
		y1= ConvertToPixelGrid(y1);
		y2= ConvertToPixelGrid(y2);
		y3= ConvertToPixelGrid(y3);
		
		
		List<Vector2> vertices = new List<Vector2>();
		vertices.Add(new Vector2(x1,y1));
		vertices.Add(new Vector2(x2,y2));
		vertices.Add(new Vector2(x3,y3));
		
		vertices.Sort((a,b)=>{return a.y.CompareTo(b.y);});
		
		
		// create edges for the triangle
		edges = new Edge[]{ new Edge(new Vector2(x1,y1),new Vector2(x2,y2)),
			new Edge(new Vector2(x2,y2),new Vector2(x3,y3)),
			new Edge(new Vector2(x3,y3),new Vector2(x1,y1))};
		
		Vector2 middlePt = vertices[1];
		Vector2 slicePt = Vector2.zero;
		
		foreach(Edge e in edges)
		{
			float x =middlePt.x;
			float yResult = e.Intercept;
			
			if(e.Gradient !=0)
			{
				x = (middlePt.y - e.Intercept)/e.Gradient;
				yResult = e.Gradient*x + e.Intercept;
			}
			
			if( !(Mathf.Round(x) == Mathf.Round(middlePt.x)) ||  !(Mathf.Round(yResult) == Mathf.Round(middlePt.y)))
			{
				slicePt = new Vector2(x, middlePt.y);
				break;
			}
		}
		
		if(Mathf.Approximately(middlePt.y,vertices[0].y))
		{
			if(vertices[0].x>=middlePt.x)
			{
				DrawSpansBetweenEdges(new Edge(middlePt,vertices[2]), new Edge(vertices[0],vertices[2]));
			}
			else
			{
				DrawSpansBetweenEdges(new Edge(vertices[0],vertices[2]), new Edge(middlePt,vertices[2]));
			}
		}

		if(slicePt.x>=middlePt.x)
		{
			DrawSpansBetweenEdges(new Edge(vertices[0],middlePt), new Edge(vertices[0],slicePt));
			DrawSpansBetweenEdges(new Edge(middlePt,vertices[2]), new Edge(slicePt,vertices[2]));
		}
		else
		{
			DrawSpansBetweenEdges(new Edge(vertices[0],slicePt), new Edge(vertices[0],middlePt));
			DrawSpansBetweenEdges(new Edge(slicePt,vertices[2]), new Edge(middlePt,vertices[2]));
		}
	}
	
	private void DrawSpansBetweenEdges(Edge e1, Edge e2)
	{
		Debug.DrawLine(new Vector3(e1.X1,0,e1.Y1),new Vector3(e1.X2,0,e1.Y2),Color.cyan);
		Debug.DrawLine(new Vector3(e2.X1,0,e2.Y1),new Vector3(e2.X2,0,e2.Y2),Color.cyan);
		

		for(float y =  e2.Y1; y <   e2.Y2; y+=pixelSize) {
			float X1 = (float.IsInfinity((y-e1.Intercept)/e1.Gradient)?e1.X1:(y-e1.Intercept)/e1.Gradient);
			float X2 = (float.IsInfinity((y-e2.Intercept)/e2.Gradient)?e2.X1:(y-e2.Intercept)/e2.Gradient);

			X1 = ConvertToPixelGrid(X1);
			X2 = ConvertToPixelGrid(X2);

			float xdiff = X2 - X1;
			
			for(float x =X1; x <= X2; x+=pixelSize) {
				SetPixel(x,y);
			}
		}
	}
	
	private float ConvertToPixelGrid(float coordinate)
	{
		return Mathf.Floor(coordinate/pixelSize)*pixelSize;
//		return  coordinate - (coordinate % pixelSize);
	}
}

