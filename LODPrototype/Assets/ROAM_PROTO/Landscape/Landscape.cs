﻿using UnityEngine;
using System.Collections;

public class Landscape : MonoBehaviour {

	public float maxLOD=12;
	public int vertexSide;
	public int patchSize=100;
	public int worldSize = 900;
	public int patches;
	public CameraTriangle ctriangle;

	private void OnValidate()
	{
		maxLOD = (int)((float)System.Math.Round(maxLOD/2f,System.MidpointRounding.AwayFromZero) * 2f) -1;

		vertexSide = (int)(Mathf.Pow(2,Mathf.Floor((float)(maxLOD)/2f)) + 1);

		// ((ceil( (log(16385)/log(2)))-1)*2  )+1

		worldSize = Mathf.Max(worldSize,patchSize);
		worldSize =  worldSize - (int)(worldSize % (float)patchSize);
		patches = (int)Mathf.Pow((worldSize/patchSize),2);
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	private void CreatePatches()
	{
	}
}
