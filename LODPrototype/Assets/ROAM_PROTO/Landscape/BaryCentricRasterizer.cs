﻿using UnityEngine;
using System.Collections;

public class BaryCentricRasterizer : MonoBehaviour {

	public CameraTriangle cameraTriangle;
	public int pixelSize = 1000;




	private Vector2 ClampToGrid(Vector2 v)
	{
		return new Vector2(ClampToGrid(v.x),ClampToGrid(v.y));
	}
	
	private float ClampToGrid(float a)
	{
		float ratio = a/(float)pixelSize;
		float round =  Mathf.Round(ratio);
		float result =  (round * pixelSize);
		
		return result;
	}
}
