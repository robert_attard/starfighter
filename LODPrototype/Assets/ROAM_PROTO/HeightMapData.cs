﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class HeightMapInfo
{
	public Texture2D texture;
	private Color[] textureColors;
	private float[] map2DValues;
	private int width;
	public Gradient gradient;
	private float cellWidth;

	public HeightMapInfo(Texture2D texture,Gradient gradient, float cellWidth )
	{
		this.cellWidth = cellWidth;
		float minV = float.MaxValue;
		float maxV = float.MinValue;
		this.texture = texture;
		this.gradient = gradient;
		ValidateTexture();
		textureColors = texture.GetPixels ();
		width = texture.width;
		map2DValues = new float[width*width];
		for (int i=0;i<textureColors.Length;i++)
		{
			Color c = textureColors[i];
			float average = (c.r + c.g + c.b)/3f;
			map2DValues[i] = average;
			minV = Mathf.Min(minV,map2DValues[i]);
			maxV = Mathf.Max(maxV,map2DValues[i]);
		}

		for (int i=0;i<textureColors.Length;i++)
		{
			map2DValues[i] /=maxV;
		}
	}
	
	float[] sqrMagnitudes;
	float[] neighbourValues;
	float nx;
	float ny;
	float _x;
	float _y;
	int xMin;
	int xMax;
	int yMin;
	int yMax;	
	int pixels;

	public float Generate(float x,float y, float scale, float heightScale)
	{
		float currentX = x/((float)cellWidth * scale);
		float currentY = y/((float)cellWidth * scale);

		float modx = Mathf.Abs(currentX % width);
		float mody = Mathf.Abs(currentY % width);
		
		int xIndex =  (int)modx;
		int yIncrement = (int)mody;

		int index = xIndex + ((int)width * yIncrement);
		float heightValue = Mathf.Max(map2DValues[index] * heightScale,1f);
		return heightValue;
	}
	
	public void ValidateTexture()
	{
		if(texture.width!=texture.height && texture.mipmapCount>0)
		{
			Debug.LogError("Invalid Texture");
		}
	}
	
}




