﻿using UnityEngine;
using System.Collections;
using VectorExtensions;
using ThreadPooling;

[System.Serializable]
public class LODRule
{
	public int minDistance;
	public int lod;

	public LODRule(int minDistance, int lod)
	{
		this.minDistance = minDistance;
		this.lod =  lod;
	}
}

public enum RenderState
{
	Idle,
	Computing,
	Rendering
}

public class ROAMJob
{
	public Triangle2D viewTriangle;
	public Triangle2D renderTriangle;
	public ROAMPatch patch;

	public ROAMJob(Triangle2D viewTriangle,Triangle2D renderTriangle, ROAMPatch patch)
	{
		this.viewTriangle = viewTriangle;
		this.renderTriangle = renderTriangle;
		this.patch = patch;
	}
}

public class ROAMRender : MonoBehaviour {

	public TerrainCharacteristic terrainCharacteristic;
	public int maxLOD;
	public int mapSize;
	public float errorTolerance = 1f;
	public float maxLODDistanceCheck = 15;
	public float renderFarClip =100f;
	public float farClip = 200f;
	public Material mat;
	public Camera orthographicCamera;
	private Mesh terrainMesh;
	private System.Object thisLock = new System.Object();

	private Vector3[] meshVertices;
	private int[] meshTriangles;
	private Vector3[] normals;
	private ROAMPatch patch;
	public static RenderState renderState;

	public int dCount;
	public static int diamondCount;

	[TextArea]
	public string LODRulesText;

	public LODRule[] lodRules;


	private void CreateLODRules()
	{
		LODRulesText.Trim();
		string[] s = LODRulesText.Split(',');
		lodRules = new LODRule[s.Length];
		for (int i = 0; i < s.Length; i++) {
			string[] fields= s [i].Split(':');
			LODRule r = new LODRule(int.Parse(fields[0]),int.Parse(fields[1]));
			lodRules[i] =r;
		}
	}

	void OnValidate()
	{
		maxLOD = Mathf.Max(maxLOD,1);
	}

	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 60;
		terrainMesh = new Mesh();
		CreateLODRules();
		ThreadPool.InitInstance(10,4);
		patch = new ROAMPatch(maxLOD,mapSize,errorTolerance,maxLODDistanceCheck,terrainCharacteristic,lodRules);
		patch.CreateInitialTriangles();
		StartCoroutine(RenderCoroutine());
	}

	void Update()
	{
		Render();
	}

	IEnumerator RenderCoroutine()
	{
		while(true)
		{
			switch (renderState) {
				case RenderState.Idle:
					renderState = RenderState.Computing;
					Triangle2D v = new Triangle2D(CreateCameraRectangle(Camera.main,1f,farClip));
					Triangle2D r = new Triangle2D(CreateCameraRectangle(Camera.main,1.5f,renderFarClip));
					ROAMJob job = new ROAMJob(v,r,patch);
					ThreadPool.QueueUserWorkItem(DoThreadedWork,job);
					break;
				case RenderState.Computing:
					break;
				case RenderState.Rendering:
					terrainMesh.Clear();
					terrainMesh.vertices = patch.meshVertices.ToArray();
					terrainMesh.triangles = patch.meshTriangles.ToArray();
					terrainMesh.normals = patch.meshNormals.ToArray();
					renderState = RenderState.Idle;
					break;
			}
			yield return null;
		}
	}



	private void DoThreadedWork(object state)
	{	
		ROAMJob p = (ROAMJob)state;
		p.patch.Process(p.viewTriangle,p.renderTriangle);
		ROAMRender.renderState = RenderState.Rendering;
	}

	private void Render()
	{
		if(terrainMesh!=null)
		{
			Graphics.DrawMesh(terrainMesh,Vector3.zero,Quaternion.identity,mat,1);
		}
	}

	public Vector2[] CreateCameraRectangle(Camera camera, float scaleOffset,float distance)
	{
		float diff = (distance * scaleOffset - distance)/2f;
		
		Vector3[] Pts = new Vector3[3];
		Pts[0] = camera.transform.position - (camera.transform.forward * diff);
		
		float farClipLength = distance * scaleOffset;
		float vFOVrad = Camera.main.fieldOfView * Mathf.Deg2Rad;
		float cameraHeightAt1 = Mathf.Tan(vFOVrad *0.5f);
		float hFOVrad = Mathf.Atan(cameraHeightAt1 * Camera.main.aspect) * 2f;
		float hFOV = (hFOVrad * Mathf.Rad2Deg)/2f;
		float otherAngle = 180f-hFOV-90f;
		
		float a  = (farClipLength/Mathf.Sin(Mathf.Deg2Rad*otherAngle))* Mathf.Sin(Mathf.Deg2Rad*hFOV);
		
		Pts[1] = Pts[0]+ (camera.transform.forward*farClipLength) + (-camera.transform.right * a);
		Pts[2] = Pts[0]+ (camera.transform.forward*farClipLength) + (camera.transform.right * a);
		
		Vector2[] pts2 = new Vector2[3];
		pts2[0] = Pts[0].ConvertZtoY();
		pts2[1] = Pts[1].ConvertZtoY();
		pts2[2] = Pts[2].ConvertZtoY();
		
		return pts2;
	}

//	void OnDrawGizmos()
//	{
//		Gizmos.color = Color.cyan;
//		Gizmos.DrawWireCube(transform.position,new Vector3(mapSize,0,mapSize));
//		
//		if(viewTriangle!=null)
//		{
//			foreach(Vector2 v in viewTriangle.pts)
//			{
//				Gizmos.color = Color.blue;
//				Gizmos.DrawSphere(v.ConvertYtoZ(),1f);
//			}
//		}
//	}
//	
//	private void DebugTriangles()
//	{
//		Vector3 c = orthographicCamera.ScreenToWorldPoint(Input.mousePosition);
//		Vector2 c2 = c.ConvertZtoY();
//		leftTri.HighLightTri(c2);
//		rightTri.HighLightTri(c2);
//	}
}
