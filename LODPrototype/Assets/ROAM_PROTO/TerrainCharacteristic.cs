﻿using UnityEngine;
using System.Collections;


public enum TerrainColorMatch
{
	White,
	Black,
	Yellow,
	Green,
	Magenta,
	Cyan,
	Red,
	Blue
}

public class TerrainCharacteristic : MonoBehaviour {
	
	public TerrainColorMatch color;
	public Gradient gradient;
	public float textureWorldScale = 1f;
	public float heightScale= 40f;
	private HeightMapInfo hInfo;
	public Texture2D heightMap;
	private float cellWidth;

	public void Init(float cellWidth)
	{
		this.cellWidth = cellWidth;
		hInfo = new HeightMapInfo(heightMap,gradient,cellWidth);
	}

	public float Generate(float x, float y)
	{
		return hInfo.Generate(x,y,textureWorldScale,heightScale);
	}
}
